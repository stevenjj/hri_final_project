#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include "inetclientstream.hpp"
#include <std_msgs/Int8.h>

double x = 0.0;
double y = 0.0;
double th = 0.0;

double vx = 0.0;
double vy = 0.0;
double vth = 0.0;

double dx_step = 0.3; //base_frame step
double world_step = dx_step;
double dtheta_step = 3.14159/6.5;        

class Odom_Obj{
public:
  ros::NodeHandle n;
  ros::Publisher odom_pub;
  ros::Subscriber odom_sub;
  tf::TransformBroadcaster odom_broadcaster;
  ros::Time current_time;
  nav_msgs::Odometry odom;


void callback(const std_msgs::Int8::ConstPtr &msg);
void publish();
  Odom_Obj();
  ~Odom_Obj();
};

Odom_Obj::Odom_Obj(){}
Odom_Obj::~Odom_Obj(){}


void Odom_Obj::callback(const std_msgs::Int8::ConstPtr &msg){
    int command = (int) msg->data;

    ros::Rate wait(0.5);
    std::cout << "I heard your message " << std::endl;

    if (command == 1){ // Turn Left;
      std::cout << "Command to Turn Left. Theta Changes by " << dtheta_step << std::endl;
      th += dtheta_step ;
    }else if (command == 2){ //MoveForward
      std::cout << "Command to Move Forward. X changes by  " << dtheta_step << std::endl;
      x += world_step*cos(th); 
      y += world_step*sin(th);
    }else if (command == 3){ // Turn Right     elif command =="N":
      std::cout << "Command to Turn Right. Theta changes by " << dtheta_step << std::endl;
     th -= dtheta_step ;
    }    

      std::cout << "Odom Position Estimates: " << dtheta_step << std::endl;
      std::cout << "    X  = " << x << std::endl;
      std::cout << "    Y  ="  << y << std::endl;
      std::cout << "    TH ="  << th << std::endl;      


      wait.sleep();


    //publish the message


/*    flotdelta_x = 

    x += delta_x;
    y += delta_y;
    th += delta_th;*/
}


void Odom_Obj::publish(){
    current_time = ros::Time::now();

      //first, we'll publish the transform over tf


      geometry_msgs::TransformStamped odom_trans;
      odom_trans.header.stamp = current_time;
      odom_trans.header.frame_id = "odom";
      odom_trans.child_frame_id = "base_link";

    //since all odometry is 6DOF we'll need a quaternion created from yaw
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);


      odom_trans.transform.translation.x = x;
      odom_trans.transform.translation.y = y;
      odom_trans.transform.translation.z = 0.0;
      odom_trans.transform.rotation = odom_quat;

      //send the transform
//      odom_broadcaster.sendTransform(odom_trans);


    //next, we'll publish the odometry message over ROS
    odom.header.stamp = current_time;
    odom.header.frame_id = "odom";

    //set the position
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    //set the velocity
    odom.child_frame_id = "base_link";
    odom.twist.twist.linear.x = 0;
    odom.twist.twist.linear.y = 0;
    odom.twist.twist.angular.z = 0;


    odom_pub.publish(odom);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "odometry_publisher");
  Odom_Obj odom_obj;

  odom_obj.odom_pub = odom_obj.n.advertise<nav_msgs::Odometry>("odom", 50);
  odom_obj.odom_sub = odom_obj.n.subscribe<std_msgs::Int8>("CBA_cmd_int", 50, boost::bind(&Odom_Obj::callback, &odom_obj, _1));  

  odom_obj.current_time = ros::Time::now();

  ros::Rate r(100.0);
  while(ros::ok()){
    ros::spinOnce();     
      odom_obj.publish();          // check for incoming messages



    r.sleep();
  }
}

/*
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include "inetclientstream.hpp"

int main(int argc, char** argv){
  ros::init(argc, argv, "odometry_publisher");

  ros::NodeHandle n;
  ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>("odom", 50);
  tf::TransformBroadcaster odom_broadcaster;

  double x = 0.0;
  double y = 0.0;
  double th = 0.0;

  double vx = 0.0;
  double vy = 0.0;
  double vth = 0.0;

  ros::Time current_time, last_time;
  current_time = ros::Time::now();
  last_time = ros::Time::now();

  ros::Rate r(10.0);
  while(n.ok()){

    ros::spinOnce();               // check for incoming messages
    current_time = ros::Time::now();

    //compute odometry in a typical way given the velocities of the robot
    double dt = (current_time - last_time).toSec();
    double delta_x = (vx * cos(th) - vy * sin(th)) * dt;
    double delta_y = (vx * sin(th) + vy * cos(th)) * dt;
    double delta_th = vth * dt;

    x += delta_x;
    y += delta_y;
    th += delta_th;

    //since all odometry is 6DOF we'll need a quaternion created from yaw
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);

    //first, we'll publish the transform over tf
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = current_time;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "base_link";

    odom_trans.transform.translation.x = x;
    odom_trans.transform.translation.y = y;
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation = odom_quat;

    //send the transform
    odom_broadcaster.sendTransform(odom_trans);

    //next, we'll publish the odometry message over ROS
    nav_msgs::Odometry odom;
    odom.header.stamp = current_time;
    odom.header.frame_id = "odom";

    //set the position
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    //set the velocity
    odom.child_frame_id = "base_link";
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.angular.z = vth;

    //publish the message
    odom_pub.publish(odom);

    last_time = current_time;
    r.sleep();
  }
}*/