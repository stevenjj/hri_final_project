Trikey was running Ubuntu 10.04. Updates needed to be made in order to compile libsocket library



Updated g++ and gcc to the 4.8.1
sudo apt-get update
sudo apt-get install gcc-4.8
sudo apt-get install g++-4.8
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.8 20
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.8 20
sudo update-alternatives --config gcc
sudo update-alternatives --config g++



Updated CMake to 2.8.8:
1- make sure that the packge ia-32libs is installed (sudo apt-get install ia-32libs) * only if your OS is 64 bits.
2- download the new cmake package from http://www.cmake.org/files/v2.8/cmake-2.8.8-Linux-i386.tar.gz
3- extract it  
i.e: /home/user/cmake-2.8.8-Linux-i386
4 - update your .bashrc file with the following line
export PATH=/home/user/cmake-2.8.8-Linux-i386/bin:$PATH
5- restart your terminal (close it and reopen it) 
6 - type this command
which -a cmake
7- now you may have two cmakes in your system - one native from ubuntu and another one that is the recently installed
8 - if there are two cmakes in the system, the one to be executed will be the the first one.
 In our example, the PATH environment variable is updated first with the path of the cmake downloaded from site.

export PATH=/home/user/cmake-2.8.8-Linux-i386/bin:$PATH



Updated git to newest version:
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get install git

/home/hri/hri_final_project/trikey_computer/lib
g++ -std=c++11 examples++/client_test.cpp -L./lib/ -lsocket++ -o client_test
