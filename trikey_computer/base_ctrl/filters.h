
class filter
{
public:
	filter(void);
	~filter(void);
	virtual void input(double input_value) = 0;
	virtual double output(void) = 0;
	virtual void clear(void) = 0;
};

class butterworth_filter : filter
{
public:
	butterworth_filter(int num_sample, double dt, double cutoff_frequency);
	~butterworth_filter(void);
	virtual void input(double input_value);
	virtual double output(void);
	virtual void clear(void);
private:
	double *mpBuffer;
	int mCurIdx;
	int mNumSample;
	double mDt;
	double mCutoffFreq;
	double mValue;
};

class digital_lp_filter : filter
{
public:
	digital_lp_filter(float w_c, float t_s);
	~digital_lp_filter(void);
	virtual void input(double input_value);
	virtual double output(void);
	virtual void clear(void);
private:
	float Lpf_in_prev[2];
	float Lpf_out_prev[2];
	float Lpf_in1, Lpf_in2, Lpf_in3, Lpf_out1, Lpf_out2;
	float lpf_out;
};

class deriv_lp_filter : filter
{
public:
	deriv_lp_filter(float w_c, float t_s);
	~deriv_lp_filter(void);
	virtual void input(double input_value);
	virtual double output(void);
	virtual void clear(void);
private:
	double Lpf_in_prev[2];
	double Lpf_out_prev[2];
	double Lpf_in1, Lpf_in2, Lpf_in3, Lpf_out1, Lpf_out2;
	double lpf_out;
};

class ff01_filter : filter
{
public:
	ff01_filter(float t_s, float w_c);
	~ff01_filter(void);
	virtual void input(double input_value);
	virtual double output(void);
	virtual void clear(void);
private:
	double Lpf_in_prev[2];
	double Lpf_out_prev[2];
	double Lpf_in1, Lpf_in2, Lpf_in3, Lpf_out1, Lpf_out2;
	double lpf_out;
};

class ff02_filter : filter
{
public:
	ff02_filter(float t_s, float w_c);
	~ff02_filter(void);
	virtual void input(double input_value);
	virtual double output(void);
	virtual void clear(void);
private:
	double Lpf_in_prev[2];
	double Lpf_out_prev[2];
	double Lpf_in1, Lpf_in2, Lpf_in3, Lpf_out1, Lpf_out2;
	double lpf_out;
};

