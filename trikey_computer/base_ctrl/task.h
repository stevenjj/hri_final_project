#include <iostream>
#include <jspace/wrap_eigen.hpp>
#include "factory.h"
//#include "comm_udp.h"
//#include "Comm.h"
#include "rt_util_base.h"

using namespace jspace;
using namespace wbc_m3_ctrl;


class Task: public Product
{
	public:
	ctrl_mode_t mode;
    long long timeStamp;
    double x[3];
    double xdot[3];
    double q[3];
    double qdot[3];
    double force[3];
    double torque[3];
    double raw_torque[3];
    double torque_d[3];
    double torque_d_raw[3];
	double pwm_cmd[3];
	double acc[3];
	double ang_vel[3];
	double ori[3][3];
	virtual void init(void) = 0;
	virtual void init(int argc, char *argv[]) {init();}
	virtual Vector update(void) = 0;
    virtual void log(void) { cout << "Nothing to log" << endl;}
//    virtual void log(message *pMsg) { pMsg->count = 0; }
	virtual void finish(void) { cout << "Finish Task"; }
	virtual string description(void) { return "Null Task"; }
};

template <typename T>
class TaskRegistrar : public Registrar<Task, T>
{
};

class TaskFactory : public Factory<Task>
{
};
