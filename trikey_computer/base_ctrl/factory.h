#include <iostream>
#include <vector>

using namespace std;

class Product
{
	public:
	virtual string getName(void) = 0;
};

template<class T1>
class Creator
{
	public:
	virtual T1 * create(void) = 0;	
};

template<class T>
class Factory
{
	private:
    static vector<string>   &name_list(void)
#if 1
	{
		static vector<string> ret;
	
		return ret;
	}
    static vector<Creator<T>*>		&object_list(void)
	{
		static vector<Creator<T>*> ret;

		return ret;
	}
#endif

	public:
    static void registerEntity(string name, Creator<T> *pObject);
    static T *createEntity(string name);
    static void listEntity();
};

#if 0
template<class T>
vector<T*>	Factory<T>::object_list;

template<class T>
vector<string>		Factory<T>::name_list;
#endif

template<class T>
void Factory<T>::registerEntity(string name, Creator<T> *pObject)
{
//    cout << "Register Obj List " << &object_list() << " (" << object_list().size() << ")" << endl;
//    cout << "Register Nam List " << &name_list() << " (" << name_list().size() << ")" << endl;
    object_list().push_back(pObject);
    name_list().push_back(name);
}

template<class T>
T *Factory<T>::createEntity(string name)
{
    int i;
	bool found = false;
    T *ret;
    vector<string>::iterator it;

    cout << "Look up Obj List " << &object_list() << " (" << object_list().size() << ")" << endl;
    cout << "Look up Nam List " << &name_list() << " (" << name_list().size() << ")" << endl;
    for ( it = name_list().begin(), i=0 ; it != name_list().end() ; it++,i++ )
    {
        cout << *it << endl;
        if ( (*it).compare(name) == 0 )
		{
			ret = (object_list()[i])->create();
			found = true;
			break;	
		}
    }
	if ( !found )
	{
		cout << "Cannot find object [" << name << "]" << endl;
		ret = NULL;
	}

	return ret;
}
template<class T>
void Factory<T>::listEntity()
{
    vector<string>::iterator it;
	
	cout << "Object List" << endl;
    for ( it = name_list().begin() ; it != name_list().end() ; it++ )
    {
        cout << *it << endl;
		#if 0
        if ( (*it).compare(name) == 0 )
		{
			ret = (object_list()[i])->create();
			found = true;
			break;	
		}
		#endif
    }
}

template<class T1, class T2>
class CreatorImpl : public Creator<T1>
{
	virtual T1 * create(void)	
	{
		return (T1 *) new T2();
	}
};

template<class T1, class T2>
class Registrar
{
	public:
	Registrar(void);
};

template<class T1, class T2>
Registrar<T1, T2>::Registrar(void)
{
	T2 obj;
	Creator<T1> *creator = (Creator<T1> *)new CreatorImpl<T1,T2>();
	cout << "Registering " << obj.getName() << endl;
	Factory<T1>::registerEntity(obj.getName(), creator);
}
