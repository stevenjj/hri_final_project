#include <math.h>
#include <string.h>
#include "filters.h"

filter::filter(void)
{
}

filter::~filter(void)
{
}

butterworth_filter::butterworth_filter(int num_sample, double dt, double cutoff_frequency)
{
	mNumSample	= num_sample;
	mDt			= dt;
	mCutoffFreq	= cutoff_frequency;

	mpBuffer	= new double[num_sample];
	memset((void *)mpBuffer, 0, sizeof(double)*num_sample);
	mCurIdx		= 0;
}

butterworth_filter::~butterworth_filter(void)
{
	delete[] mpBuffer;
}

void butterworth_filter::input(double input_value)
{
	int j;
	double sqrt_2 = sqrt(2);
	double value = 0;
	for ( j = mNumSample-2 ; j >= 0 ; j-- )
		mpBuffer[j+1] = mpBuffer[j];

	mpBuffer[0] = input_value;
	for ( j = 0 ; j < mNumSample ; j++ )
	{
		double t = (double)j*mDt;
		value += sqrt_2 / mCutoffFreq * mpBuffer[j] * exp(-1./sqrt_2*t) * sin(mCutoffFreq/sqrt_2*t ) * mDt;
//		value += sqrt_2 * exp(-1./sqrt_2*t) * sin(1./sqrt_2*t ) * mDt;
	}
	
	mValue = value;
}

double butterworth_filter::output(void)
{
	return mValue;
}

void butterworth_filter::clear(void)
{
}

digital_lp_filter::digital_lp_filter(float w_c, float t_s)
{
	Lpf_in_prev[0] = Lpf_in_prev[1] = 0;
	Lpf_out_prev[0] = Lpf_out_prev[1] = 0;
	Lpf_in1=0, Lpf_in2=0, Lpf_in3=0, Lpf_out1=0, Lpf_out2=0;
	float den = 2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000;

	Lpf_in1 = 2500*t_s*t_s*w_c*w_c / den;
	Lpf_in2 = 5000*t_s*t_s*w_c*w_c / den;
	Lpf_in3 = 2500*t_s*t_s*w_c*w_c / den;
	Lpf_out1 = -(5000*t_s*t_s*w_c*w_c  - 20000) / den;
	Lpf_out2 = -(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;

}

digital_lp_filter::~digital_lp_filter(void)
{
}

void digital_lp_filter::input(double lpf_in)
{
	lpf_out = Lpf_in1*lpf_in + Lpf_in2*Lpf_in_prev[0] + Lpf_in3*Lpf_in_prev[1] + //input component
		Lpf_out1*Lpf_out_prev[0] + Lpf_out2*Lpf_out_prev[1]; //output component
	Lpf_in_prev[1] = Lpf_in_prev[0];
	Lpf_in_prev[0] = lpf_in;
	Lpf_out_prev[1] = Lpf_out_prev[0];
	Lpf_out_prev[0] = lpf_out;
}

double digital_lp_filter::output(void)
{
	return lpf_out;
}

void digital_lp_filter::clear(void)
{
	Lpf_in_prev[1] = 0;
	Lpf_in_prev[0] = 0;
	Lpf_out_prev[1] = 0;
	Lpf_out_prev[0] = 0;
}


deriv_lp_filter::deriv_lp_filter(float w_c, float t_s)
{
	Lpf_in_prev[0] = Lpf_in_prev[1] = 0;
	Lpf_out_prev[0] = Lpf_out_prev[1] = 0;
	Lpf_in1=0, Lpf_in2=0, Lpf_in3=0, Lpf_out1=0, Lpf_out2=0;
    double a = 1.4142;
	double den = 4 + 2*a*w_c*t_s + t_s*t_s*w_c*w_c;

	Lpf_in1 = 2*t_s*w_c*w_c / den;
	Lpf_in2 = 0;
	Lpf_in3 = -2.*t_s*w_c*w_c / den;
	Lpf_out1 = -1. *(-8 + t_s*t_s*w_c*w_c*2) / den; 
	Lpf_out2 = -1. *(4 - 2*a * w_c*t_s + t_s*t_s*w_c*w_c) / den;

    clear();
}

deriv_lp_filter::~deriv_lp_filter(void)
{
}

void deriv_lp_filter::input(double lpf_in)
{
	lpf_out = Lpf_in1*lpf_in + Lpf_in2*Lpf_in_prev[0] + Lpf_in3*Lpf_in_prev[1] + //input component
		Lpf_out1*Lpf_out_prev[0] + Lpf_out2*Lpf_out_prev[1]; //output component
	Lpf_in_prev[1] = Lpf_in_prev[0];
	Lpf_in_prev[0] = lpf_in;
	Lpf_out_prev[1] = Lpf_out_prev[0];
	Lpf_out_prev[0] = lpf_out;
}

double deriv_lp_filter::output(void)
{
	return lpf_out;
}

void deriv_lp_filter::clear(void)
{
	Lpf_in_prev[1] = 0;
	Lpf_in_prev[0] = 0;
	Lpf_out_prev[1] = 0;
	Lpf_out_prev[0] = 0;
}

ff01_filter::ff01_filter(float t_s, float w_c)
{
	Lpf_in_prev[0] = Lpf_in_prev[1] = 0;
	Lpf_out_prev[0] = Lpf_out_prev[1] = 0;
	Lpf_in1=0, Lpf_in2=0, Lpf_in3=0, Lpf_out1=0, Lpf_out2=0;
    double a = 1.4142;
	double den = 4 + 2*a*w_c*t_s + t_s*t_s*w_c*w_c;
    double J = 0.00008;
    double B = 0.0002;

	Lpf_in1 = B*t_s*t_s*w_c*w_c + 2*J*t_s*w_c*w_c;
	Lpf_in2 = 2*B*t_s*t_s*w_c*w_c;
	Lpf_in3 = B*t_s*t_s*w_c*w_c - 2 * J * t_s * w_c * w_c;
	Lpf_out1 = -1. *(-8 + t_s*t_s*w_c*w_c*2) / den; 
	Lpf_out2 = -1. *(4 - 2*a * w_c*t_s + t_s*t_s*w_c*w_c) / den;
}

ff01_filter::~ff01_filter(void)
{
}

void ff01_filter::input(double lpf_in)
{
	lpf_out = Lpf_in1*lpf_in + Lpf_in2*Lpf_in_prev[0] + Lpf_in3*Lpf_in_prev[1] + //input component
		Lpf_out1*Lpf_out_prev[0] + Lpf_out2*Lpf_out_prev[1]; //output component
	Lpf_in_prev[1] = Lpf_in_prev[0];
	Lpf_in_prev[0] = lpf_in;
	Lpf_out_prev[1] = Lpf_out_prev[0];
	Lpf_out_prev[0] = lpf_out;
}

double ff01_filter::output(void)
{
	return lpf_out;
}

void ff01_filter::clear(void)
{
	Lpf_in_prev[1] = 0;
	Lpf_in_prev[0] = 0;
	Lpf_out_prev[1] = 0;
	Lpf_out_prev[0] = 0;
}

ff02_filter::ff02_filter(float t_s, float w_c)
{
    double J = 0.003216;

	Lpf_in_prev[0] = Lpf_in_prev[1] = 0;
	Lpf_out_prev[0] = Lpf_out_prev[1] = 0;
	Lpf_in1=0, Lpf_in2=0, Lpf_in3=0, Lpf_out1=0, Lpf_out2=0;

    double a = 1.4142;
	double den = 4 + 2*a*w_c*t_s + t_s*t_s*w_c*w_c;

	Lpf_in1 = J*2*t_s*w_c*w_c / den;
	Lpf_in2 = 0;
	Lpf_in3 = -2.*J*t_s*w_c*w_c / den;
	Lpf_out1 = -1. *(-8 + t_s*t_s*w_c*w_c*2) / den; 
	Lpf_out2 = -1. *(4 - 2*a * w_c*t_s + t_s*t_s*w_c*w_c) / den;

    clear();

}

ff02_filter::~ff02_filter(void)
{
}

void ff02_filter::input(double lpf_in)
{
	lpf_out = Lpf_in1*lpf_in + Lpf_in2*Lpf_in_prev[0] + Lpf_in3*Lpf_in_prev[1] + //input component
		Lpf_out1*Lpf_out_prev[0] + Lpf_out2*Lpf_out_prev[1]; //output component
	Lpf_in_prev[0] = lpf_in;
	Lpf_in_prev[1] = Lpf_in_prev[0];
	Lpf_out_prev[0] = lpf_out;
	Lpf_out_prev[1] = Lpf_out_prev[0];
}

double ff02_filter::output(void)
{
	return lpf_out;
}

void ff02_filter::clear(void)
{
	Lpf_in_prev[1] = 0;
	Lpf_in_prev[0] = 0;
	Lpf_out_prev[1] = 0;
	Lpf_out_prev[0] = 0;
}

