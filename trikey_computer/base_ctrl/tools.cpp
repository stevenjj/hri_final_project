#include "tools.h"

int sgn(double in)
{
    return (in>0.)?1:(in<0.)?-1:0;        
}

jspace::Vector sgn(jspace::Vector in)
{
	jspace::Vector out;

	out = in;
	out(0) = sgn(in(0));
	out(1) = sgn(in(1));
	out(2) = sgn(in(2));

	return out;
}

double tanh(double in)
{
	double out;

	out = (exp(in) - exp(-in)) / (exp(in) + exp(-in));

	return out;
}
jspace::Vector tanh(jspace::Vector in)
{
	jspace::Vector out;
	
	out = in;
	out(0) = tanh(in(0));
	out(1) = tanh(in(1));
	out(2) = tanh(in(2));

	return out;
}
