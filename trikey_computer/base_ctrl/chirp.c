//chirp params
#include <math.h>

#define SWEEP_FREQ_HZ_LOW 0.001
#define SWEEP_FREQ_HZ_HIGH 50
#define SWEEP_RATE 0.033 //percent change of sweep range per second
#define SWEEP_RANGE (SWEEP_FREQ_HZ_HIGH-SWEEP_FREQ_HZ_LOW)
#define SWEEP_SETPOINT_CURRENT_AMPLITUDE_A_DEFAULT 1.0
#define SWEEP_SETPOINT_MID_CURRENT_A 0.0 

float gESF = 0.;
float Prev_effective_angle = 0.;
float getChirpSignal(float elapsedTime, float samplePeriod, float switching_amplitude, float switching_offset, unsigned char *end){
  float amplitude, offset, current_des_amps=0.0, effective_switching_freq_hz=0.0, effective_angle;
  amplitude = switching_amplitude;
  offset = switching_offset;
  
  if(elapsedTime > 0.0){
    
    //linear chirp
    //effective_switching_freq_hz = SWEEP_FREQ_HZ_LOW + SWEEP_RATE*SWEEP_RANGE*t;
    //current_des_amps = amplitude*sin(t * 2 * PI * effective_switching_freq_hz ) + offset;

    //exponential chirp
    effective_angle = 2 * M_PI * SWEEP_FREQ_HZ_LOW * (pow(SWEEP_RATE*SWEEP_RANGE,elapsedTime)-1)/log(SWEEP_RATE*SWEEP_RANGE);
    current_des_amps = amplitude*sin(effective_angle) + offset;
     
    if(samplePeriod <= 0.0){
      effective_switching_freq_hz = 0.0;
    }
    else{
      effective_switching_freq_hz = (effective_angle - Prev_effective_angle)/samplePeriod/2/M_PI; 
    }
    gESF = effective_switching_freq_hz;

    Prev_effective_angle = effective_angle;
//    fprintf(ifp, "%f" FILE_DELIMITER, effective_switching_freq_hz);
//    printf("%f\n", effective_switching_freq_hz);
    if(effective_switching_freq_hz > SWEEP_FREQ_HZ_HIGH) {
      *end = 1;
    }
  }
  else{
    current_des_amps = offset;
  }
  return current_des_amps;
}
