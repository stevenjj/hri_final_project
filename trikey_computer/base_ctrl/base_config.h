#define OPERATION_CALIBRATE     1
#define OPERATION_NORMAL        2

#define MODE_RAW_TORQUE         1
#define MODE_COMPENSATE_TORQUE  2

#define STATE_STOP              1
#define STATE_ACC               2
#define STATE_STEADY            3
#define STATE_DEACC             4

#define STOP_THRESHOLD          0.1


#define NUM_SAMPLE	10
#define SERVO_RATE	1000

#define SET_PARAM

#include "m3uta/controllers/torque_shm_uta_ec_sds.h"

extern "C" {
extern float getChirpSignal(float elapsedTime, float samplePeriod, float switching_amplitude, float switching_offset, unsigned char *end);
}
extern M3UTATorqueShmEcSdsCommand shm_param;
