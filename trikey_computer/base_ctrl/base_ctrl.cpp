#include "rt_util_base.h"


//mk
#include "inetclientstream.hpp"
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <sstream>
#include <stdio.h>
//mk-end

#include <rtai_sched.h>
#include <rtai_shm.h>
#include <dlfcn.h>

#include <rtai_sem.h>
#include <rtai_nam2num.h>
#include <rtai_registry.h>

//#include <wbc_core/opspace_param_callbacks.hpp>
#include <boost/scoped_ptr.hpp>
#include <err.h>
#include <signal.h>
#include <math.h>
#include "base_config.h"
//#include "comm_udp.h"
#include "filters.h"
//#include "test_loops.h"
#include "task.h"

using namespace wbc_m3_ctrl;
//using namespace opspace;
//using namespace wbc_core_opspace;
//using namespace uta_opspace;
using namespace boost;
using namespace std;
using namespace jspace;
using Eigen::MatrixXd;

//mk
std::string host_ip;
std::string port_num;
std::string answer;



MatrixXd xfv;
MatrixXd xdotfv;
MatrixXd odo;
MatrixXd odo_dot;
MatrixXd moc;
MatrixXd moc_dot;
MatrixXd aRef;
MatrixXd destv;
bool op_pos_initialized = false;
double wheel_vel[3] = { 0 }; 
double raw_adc_value[NUM_SAMPLE][3] = { {0,}, }; 
double sCommand[3] = {0};
double opcmd[3] = {0};
double freq = 0.;
double phase = 0.;
int operation_mode 	= OPERATION_NORMAL;
int control_mode   	= MODE_COMPENSATE_TORQUE;
int state_mode[3]	= {STATE_STOP, STATE_STOP, STATE_STOP};
double wheel_pos[3] = { 0 }; 
double wheel_ori;
double theta, phi, psi;
double pwm_cmd[3];
double up[3],xaxis[3];
double xdes[3];
int cmd_elapsed = 0;

extern float gESF;
double a[3];
extern double error;
double f;
int a_count;
float elapsedTime;
float samplePeriod;
double err_pos;
extern double cur_vel;
double nxt_vel, nxt_err;
int gRegion;
int destIndex;
double gSlope;
double sval[5];


static long long servo_rate;
static double adc_accumulated[3] = {0};
static int    adc_accumulation_count[3] = {0};
//static double force_average[3] = {2073, 2296, 1993};
static double torque_raw[3];
static double torque_calibrated[3];
static double torque_calibrated_last[3];
static double torque_calibrated_filtered[3];
static double torque_d[3];
static double torque_d_raw[3];
//static double force_gap[3] = {5.,5.,5.};
double wheelTorqueErrorSum_Nm[3] = {0};
//static double sqsum[3] = {0};
//static double hand_pos[3] = { 0 }; 
//static double body_pos[2] = { 0 }; 
//static double body_vel[2] = { 0 }; 
//static double arm_pos[7] = { 0 }; 
//static double init_pos[3] = { 0 };
static double orientation[3][3] = {{0}};
static vector<double> head_pos(12);
//static double dest_value[3] = {0};
//static double dest[3] = {0};
//static double delta = 1; //0.01;
//static double kv = 100.0;
//static bool torqueMode = false;
//static int rate;

long long   sBaseTimeStamp = 0;
long long   sTimeStamp;          // micro-second
static long long   sPrevTimeStamp;
static int loop_count = 0;
static long long   periodSum = 0;

float P_term[3] = {0};
float I_term[3] = {0};
float D_term[3] = {0};
float motorTorqueComand_Last_Nm[3] = {0};
static filter *wheel_vel_filter[3];
static filter *mocap_vel_filter[3];
static filter *lp_torque[3];
static filter *torque_vel_filter[3];


//begin new controller code
//static int loop_counter=0; //counter reducing number of screen prints (kind of a hack)
//end new controller code

#define CUTOFF_FREQ (100.)

int run = true;
static void handle(int signum)
{
    fprintf(stderr, "End Application\n");
	run = false;
//    sleep(1);
}

void start_calibration();
void calibrate_offset(double *raw_adc_value);

void start_calibration()
{
	int i;

	operation_mode = OPERATION_CALIBRATE;
	fprintf(stderr, "Calibration start\n");
	for ( i = 0 ; i < 3 ; i++ )
	{
		adc_accumulated[i] 			= 0;
		adc_accumulation_count[i]	= 0;
        odo[i] = 0.;
	}
}

#if 0 
// 10/15
static double adc_offset[3] = {
	(16500-168)*2.604 + 471.890813,
	41929+285 + 200.963153,
	41929+1320 - 230.272186
};
static double scale[3] = {

	4086.032558 + 3141.861309,
	3360.748644 + 3360.748644,
	2785.306277 + 3241.312776
};
// 10/22
static double adc_offset[3] = {
	42887.135806,
	42205.569553,
	43121.461393
};
static double scale[3] = {
	3448.054141,
	3268.822491,
	3249.663308
};
#else 

static double adc_offset[3] = {
43032.076356,
42053.738835,
42913.310563
};
static double scale[3] = {
-3601.602108,
-3388.714086,
-3120.398356
};
#endif

double calibrate_torque(int index, double value)
{
	double ret;

	ret = (value - adc_offset[index]) / scale[index];

	return ret;
}

double safety_check(jspace::Vector &command)
{
    int i;

    for ( i = 0 ; i < 3 ; i++ )
    {
        if ( command[i] > 15000 )
            command[i] = 15000;
        else if ( command[i] < -15000 )
            command[i] = -15000;
    }
	
	return 0.;
}


namespace {
	
	
	class Servo
		: public RTUtilBase
	{
	public:
//		shared_ptr<Skill> skill;    
        Task *mTask;
		
		virtual int init(	jspace::State const & state,
			jspace::State const & hand_state,
			jspace::State const & head_state,
			jspace::State const & arm_state) {

			int i;

			struct timeval tv;

			gettimeofday(&tv, NULL);
			sPrevTimeStamp = ((long long)tv.tv_sec)*1000000 + (long long)tv.tv_usec;

            update_count = 0;
            update_time = 0L;
            max_loop_time = 0L;
            max_update_time = 0L;
            max_read_time = 0L;
            max_write_time = 0L;

            /* Create Low-pass filter */
            for ( i = 0 ; i < 3 ; i++ )
            {
#define FCONTROL_DOB_WC (CUTOFF_FREQ * 2 * 3.14)  // in Hz
                wheel_vel_filter[i] = (filter  *) new deriv_lp_filter( FCONTROL_DOB_WC, 1./SERVO_RATE);
			    wheel_vel_filter[i]->clear();
                mocap_vel_filter[i]    = (filter  *) new deriv_lp_filter( FCONTROL_DOB_WC, 1./SERVO_RATE);
			    mocap_vel_filter[i]->clear();
                lp_torque[i] = (filter  *) new digital_lp_filter( FCONTROL_DOB_WC, 1./SERVO_RATE);
			    lp_torque[i]->clear();
                torque_vel_filter[i] = (filter  *) new deriv_lp_filter( FCONTROL_DOB_WC, 1./SERVO_RATE);
			    torque_vel_filter[i]->clear();
            }

			sTimeStamp = rt_get_cpu_time_ns() / 1000;
			mTask->timeStamp = sTimeStamp;
			for ( i = 0 ; i < 3 ; i++ )
			{
				mTask->q[i] = state.position_[i];	
				mTask->qdot[i] = 0;
//				    mTask->torque[i] = torque_calibrated_filtered[i];
//				    mTask->raw_torque[i] = torque_calibrated[i];
				mTask->torque[i] = torque_calibrated[i];
				mTask->raw_torque[i] = torque_raw[i];
				//torque_calibrated[i];
				mTask->torque_d[i] = torque_d[i];
				mTask->torque_d_raw[i] = torque_d_raw[i];
				mTask->pwm_cmd[i] = pwm_cmd[i];
			}

			mTask->mode = CTRL_MODE_TORQUE;
			mTask->init();
			mode = mTask->mode;

			return 0;
		}
		
		// Don't use
        void calibrate_offset(double *raw_adc_value)
        {
            int i;

            for ( i = 0 ; i < 3 ; i++ )
            {
                adc_accumulated[i] += raw_adc_value[i];
                adc_accumulation_count[i]++;
                adc_offset[i] = adc_accumulated[i] / adc_accumulation_count[i];

                if ( adc_accumulation_count[i] > 1000 )
                {
                    fprintf(stderr, "Calibration Done %d:%f\n", i, adc_offset[i]);
                    operation_mode = OPERATION_NORMAL;
                    lp_torque[i]->clear();
                }
                wheelTorqueErrorSum_Nm[i] = 0.;
                motorTorqueComand_Last_Nm[i] = 0.;
            }
			sTimeStamp = rt_get_cpu_time_ns() / 1000;
            mTask->timeStamp = sTimeStamp;
            mTask->init(); 
        }
		virtual int update(	jspace::State const & state, 
			jspace::State const & hand_state,
			jspace::State const & head_state,
			jspace::State const & arm_state,
		       	jspace::Vector & command,
		       	jspace::Vector & head_command,
		       	jspace::Vector & arm_command)
		{
			int i, j;	
			double speed = 0.;

			sTimeStamp = rt_get_cpu_time_ns() / 1000;

			// Read the joint position/velocity
			for ( i = 0 ; i < 3 ; i++ )
			{
				for ( j = NUM_SAMPLE-2 ; j >= 0 ; j-- )
					raw_adc_value[j+1][i] = raw_adc_value[j][i];
                torque_calibrated_last[i] = torque_calibrated[i];

				wheel_pos[i]		= state.position_[i];	
				torque_raw[i]		= state.force_[i]; 
				raw_adc_value[0][i]	= torque_raw[i];
				torque_calibrated[i] =  calibrate_torque(i, torque_raw[i]);

                wheel_vel_filter[i]->input(state.position_[i]);
                lp_torque[i]->input(torque_calibrated[i]);

				pwm_cmd[i]			= state.remote_command_[i];

#if 0
                static bool read = false;
                double vel = wheel_vel_filter[i]->output();
                if ( (!read && cos(43.01*wheel_pos[i]) > 0.90) || fabs(vel) < 0.1 )
                {
           		    wheel_vel[i] = vel;
                    read = true;
                }
                if ( cos(43.01*wheel_pos[i]) < -0.90 )
                    read = false;
#else
                wheel_vel[i] = wheel_vel_filter[i]->output();
#endif

				for ( j = 0; j < 3; ++j) 
					orientation[i][j] = state.orientation_mtx_[3*i+j];
                double x,y;
                x = orientation[0][0], y = orientation[0][1];
                wheel_ori = atan2(y,x);
				
                torque_vel_filter[i]->input(torque_calibrated[i]);

                torque_d_raw[i] = (torque_calibrated[i] - torque_calibrated_last[i]) * SERVO_RATE;
				torque_calibrated_filtered[i] =  lp_torque[i]->output();
                torque_d[i] = torque_vel_filter[i]->output();
				
				if ( fabs(wheel_vel[i]) < STOP_THRESHOLD )
					state_mode[i] = STATE_STOP;
				else
					state_mode[i] = STATE_STEADY;
				speed += pow(wheel_vel[i], 2.);
			}
			speed = sqrt(speed);

            elapsedTime = (sTimeStamp-sBaseTimeStamp)/1000000.;
            samplePeriod = 1./SERVO_RATE;
//            fprintf(stderr, "%f %f\n", elapsedTime, 1./SERVO_RATE);

            if ( cmd_elapsed > SERVO_RATE )
            {
//                remote_command[0] = 0.;
            }

            cmd_elapsed++;

#if 0
            {
                double q1, q2, q3, q4;
                q1 = pPos->data[3];
                q2 = pPos->data[4];
                q3 = pPos->data[5];
                q4 = pPos->data[6];
                up[0] = 2*(q2*q3 - q1*q4);
                up[1] = 2*(q3*q4 - q1*q2);
                up[2] = 1-2*q2*q2-2*q4*q4;
                phi = atan2(up[1], up[0]);
                theta = asin(sqrt(up[0]*up[0] + up[1]*up[1]) / sqrt(up[0]*up[0] + up[1]*up[1] + up[2]*up[2]));
                xaxis[0] = 2*(q1*q1+q4*q4) -1;
	            xaxis[1] = 2*(q1*q2+q3*q4);
	            xaxis[2] = 2*(q1*q3-q2*q4);
                moc[2] = atan2(xaxis[2], -xaxis[0]);
            }
#else
#endif
            for ( i = 0 ; i < 3 ; i++ )
            {                    
                mocap_vel_filter[i]->input(moc(i));
                moc_dot(i) = mocap_vel_filter[i]->output();
            }
#if 0
                xfv = odo;
                xdotfv = odo_dot;
#else
                xfv = moc;
                xdotfv = odo_dot;
#endif
			switch ( operation_mode )
			{
			case OPERATION_CALIBRATE:
                command = Vector::Zero(3);
//				calibrate_offset(raw_adc_value[0]);
				break;
			case OPERATION_NORMAL:
//				command = test14_loop();
//				command = test02_loop();
//                mTask->x    = xfv;
//                mTask->xdot = xdotfv;
                mTask->timeStamp = sTimeStamp;
                for ( i = 0 ; i < 3 ; i++ )
                {
                    mTask->q[i] = wheel_pos[i];
                    mTask->qdot[i] = wheel_vel[i];
//				    mTask->torque[i] = torque_calibrated_filtered[i];
//				    mTask->raw_torque[i] = torque_calibrated[i];
				    mTask->torque[i] = torque_calibrated[i];
				    mTask->raw_torque[i] = torque_raw[i];
					//torque_calibrated[i];
                    mTask->torque_d[i] = torque_d[i];
                    mTask->torque_d_raw[i]	= torque_d_raw[i];
					mTask->pwm_cmd[i]		= pwm_cmd[i];
					mTask->acc[i]			= state.accelerometer_[i];
					mTask->ang_vel[i]		= state.ang_vel_[i];
					for ( j = 0; j < 3; ++j) 
					mTask->ori[i][j] = state.orientation_mtx_[3*i+j];
                }
                command = mTask->update();
//				command = control_loop();
				break;
			}
			
			#if 0
			{
				static int count = 0;
				if ( count++ > 1000 )
				{
					fprintf(stderr, "RC: %f %f %f %f\n",
						remote_command[0], remote_command[1],
						remote_command[2], remote_command[3]);
					count = 0;
				}
			}
			#endif
            safety_check(command);

			sPrevTimeStamp = sTimeStamp;

			periodSum += sTimeStamp - sPrevTimeStamp;
			loop_count++;
#if 0
            mpMsg->timeStamp = sTimeStamp;
    		mTask->log(mpMsg);
    	    send_udp(mpMsg);
#endif
			return 0;
		}
		
		
		virtual int cleanup(void)
		{
			return 0;
		}
		
		
		virtual int slowdown(long long iteration,
			 long long desired_ns,
			 long long actual_ns)
		{
			return 0;
		}
	};
	
}


void *socketTimer(void* data)
{
 Task* pTask = (Task*) data;
 int tmp;
 
std::string host = "10.0.0.40";
std::string port = "1235";
std::string answer_;
std::string sender;
std::stringstream ss;
char sz[18];
answer_.resize(32);
  
 printf("udp thread\n");

// while(1){

while(1){

try {
       libsocket::inet_stream sock(host,port,LIBSOCKET_IPv4);
        sock >> answer;
        std::cout << answer;
        
       for(int i(0);i<3;i++)
        printf("q for %d : %+5lf",i,pTask->q[i]);
        printf("\n");

        ss<<pTask->q[0];
      sender=ss.str();
      //  sender=boost::lexical_cast<std::string>(pTask->q[0]);
     //  printf("sender %s\n",sender.c_str()); 
    //  sock.snd(ss,5);
     
      sprintf(sz,"%.3f %.3f %.3f",pTask->q[0],pTask->q[1],pTask->q[2]);
       sock<<sz;
      //  sock << "Hello back!\n";
        //sock is closed here automatically!
    } catch (const libsocket::socket_exception& exc){
       std::cerr << exc.mesg;
   }

 sleep(1);
}

}

Task* _pTask=NULL;

double cmd[3];
int prev_sign;
double time_gap;
double elapsed_time;
double cross_point;
int sign[3];
double phasev[3];
double cur_posv[3];
MatrixXd err_posv;
double acc_termv[3];
double pos_termv[3];
double vel_termv[3];
double desired_pos;
int main(int argc, char ** argv)
{
	struct sigaction sa;
    char *task_name;

    if ( argc < 2 )
    {
        fprintf(stderr, "Specify the task\n");         
        exit(-1);
    }
    task_name = argv[1];

	bzero(&sa, sizeof(sa));
	sa.sa_handler = handle;
	if (0 != sigaction(SIGINT, &sa, 0)) {
        fprintf(stderr, "SIGACTION ERROR\n");
		err(EXIT_FAILURE, "sigaction");
	}

    void *dlhandle;

    dlhandle = dlopen("libtasks.so", RTLD_LAZY);

    cout << "DL handle " << dlhandle << endl;
	cout << dlerror() << endl;


  //  cout<<"MK socket test"<<endl;

    xdotfv = MatrixXd::Zero(3,1);
    xfv = MatrixXd::Zero(3,1);
    odo = MatrixXd::Zero(3,1);
    odo_dot = MatrixXd::Zero(3,1);
    moc = MatrixXd::Zero(3,1);
    moc_dot = MatrixXd::Zero(3,1);
    aRef = MatrixXd::Zero(3,1);
//	pthread_t udp_receiver;
//	int boolthread= pthread_create( &(udp_receiver), NULL,socketTimer , (void*)&servo.mTask);

//        printf("boolthread : %d \n",boolthread);
//	pthread_t pos_receiver;
//	pthread_create( &pos_receiver, NULL, receive_pos, (void*)pPos);
	
	servo_rate = SERVO_RATE;
	
	Servo servo;

    servo.mTask = TaskFactory::createEntity(task_name);
    if ( servo.mTask == NULL )
    {
        fprintf(stderr, "Cannot find the task %s\n", task_name);         
		TaskFactory::listEntity();
	    servo.shutdown();
        sleep(1);
        exit(-1);
    }
	sTimeStamp = rt_get_cpu_time_ns() / 1000;
	servo.mTask->timeStamp = sTimeStamp;
    fprintf(stderr, "INIT: %d\n", run);
    servo.mTask->init(argc-1, argv+1);
    _pTask=servo.mTask;

	pthread_t udp_receiver;
	int boolthread= pthread_create( &(udp_receiver), NULL,socketTimer , (void*)_pTask);

//        printf("boolthread : %d \n",boolthread);
//	


//    std::cout<<"MK test"<<endl;
	sBaseTimeStamp = rt_get_cpu_time_ns() / 1000;
    fprintf(stderr, "RUN: %d\n", run);
	try {
			warnx("initializing param callbacks");
		servo.start(servo_rate);
	}
	catch (std::runtime_error const & ee) {
		errx(EXIT_FAILURE, "failed to start servo: %s", ee.what());
	}
	
	warnx("started servo RT thread");
	
	struct timeval prevTv;
//	int prevLoopCount = loop_count;
	long long dump_dt = 1000000;
	long long printStamp = 0;

	sBaseTimeStamp = rt_get_cpu_time_ns() / 1000;

	gettimeofday(&prevTv, NULL);

	while (run) {
		long long diff;
		long long timeStamp;
		long long captured_loop_count;

		captured_loop_count = loop_count;

        timeStamp = rt_get_cpu_time_ns() / 1000 - sBaseTimeStamp;
		diff = timeStamp - printStamp;

        if ( !op_pos_initialized && timeStamp > sBaseTimeStamp+1000000 ) 
        {
            xfv = MatrixXd::Zero(3,1);                
            op_pos_initialized = true;
        }

		if ( diff > dump_dt)
		{
#if 0
			fprintf(stderr, "LOOP #: %d\n", loop_count);
			fprintf(stderr, "wheel_pos\n");
			for ( int i  = 0 ; i < 3 ; i++ )
				fprintf(stderr, "%+7lf ", wheel_pos[i]);
			fprintf(stderr, "\n");

			fprintf(stderr, "wheel_vel\n");
			for ( int i  = 0 ; i < 3 ; i++ )
				fprintf(stderr, "%+7lf ", wheel_vel[i]);
			fprintf(stderr, "\n");

			fprintf(stderr, "OP cmd\n");
			for ( int i  = 0 ; i < 3 ; i++ )
				fprintf(stderr, "%+7lf ", opcmd[i]);
			fprintf(stderr, "\n");

			fprintf(stderr, "command\n");
			for ( int i  = 0 ; i < 3 ; i++ )
				fprintf(stderr, "%+7lf ", sCommand[i]);
			fprintf(stderr, "\n");

			fprintf(stderr, "aRef\n");
			for ( int i  = 0 ; i < 3 ; i++ )
				fprintf(stderr, "%+7lf ", aRef[i]);
			fprintf(stderr, "\n");

			fprintf(stderr, "phase\n");
			for ( int i  = 0 ; i < 3 ; i++ )
				fprintf(stderr, "%+7lf ", phasev[i]);
			fprintf(stderr, "\n");

			fprintf(stderr, "desired_pos: %f\n", desired_pos);

			fprintf(stderr, "cur_pos\n");
			for ( int i  = 0 ; i < 3 ; i++ )
				fprintf(stderr, "%+7lf ", cur_posv[i]);
			fprintf(stderr, "\n");

//			fprintf(stderr, "err_pos\n");
//			for ( int i  = 0 ; i < 3 ; i++ )
//				fprintf(stderr, "%+7lf ", err_posv[i]);
//			fprintf(stderr, "\n");

//			fprintf(stderr, "dest_pos\n");
//			for ( int i  = 0 ; i < 3 ; i++ )
//				fprintf(stderr, "%+7lf ", destv[i]);
//			fprintf(stderr, "\n");

//			fprintf(stderr, "sign\n");
//			for ( int i  = 0 ; i < 3 ; i++ )
//				fprintf(stderr, "%+7lf ", sign[i]);
//			fprintf(stderr, "\n");

            fprintf(stderr, "Update time %f ns(%d times, max %lld ns(%lld , %lld, %lld)\n", (double)servo.update_time / servo.update_count, servo.update_count, servo.max_update_time, servo.max_read_time, servo.max_loop_time, servo.max_write_time);
			prevLoopCount = captured_loop_count;

			fprintf(stderr, "Orientation: %f\n", wheel_ori);
			for ( int i = 0 ; i < 3 ; i++ )
			{
				for ( int j = 0 ; j < 3 ; j++ )
					fprintf(stderr, "%f ", orientation[i][j]);
				fprintf(stderr, "\n");
			}

            fprintf(stderr, "rad_distance:%f sign:%d\n", elapsed_time, prev_sign);
            fprintf(stderr, "phase:%f freq:%f cross: %f\n", phase, freq, cross_point);
            fprintf(stderr, "(X  ) x :%f y :%f theta:%f\n", xfv(0), xfv(1), xfv(2));
            fprintf(stderr, "(ODO) x :%f y :%f theta:%f\n", odo(0), odo(1), odo(2));
            fprintf(stderr, "(MOC) x :%f y :%f theta:%f\n", moc(0), moc(1), moc(2));
			fprintf(stderr, "Region: %d\n", gRegion);
            fprintf(stderr, "Vx:%f Vy:%f omega:%f\n", xdotfv(0), xdotfv(1), xdotfv(2));
            static int old_a_count = 0;
            if ( old_a_count != a_count )
            {
			    fprintf(stdout, "%d: %f a: %f %f err: %f\n", a_count, f, a[0], a[1], error);
                old_a_count = a_count;
            }
            fprintf(stderr, "Remote Commmand(%d) %f\n", destIndex, remote_command[1]);
            fprintf(stderr, "Theta  %+8f, Phi %+8f Psi %+8f\n", theta, phi, psi);
            fprintf(stderr, "Up :%f %f %f\n", up[0], up[1], up[2]);
#else
            fprintf(stderr, "Update time %f ns(%d times, max %lld ns(%lld , %lld, %lld)\n", (double)servo.update_time / servo.update_count, servo.update_count, servo.max_update_time, servo.max_read_time, servo.max_loop_time, servo.max_write_time);
		    servo.mTask->log();
			printStamp = timeStamp;
#endif
		}
	    servo.mTask->finish();

		usleep(100);		// 100Hz-ish
	}
	
        _pTask=NULL;
	servo.shutdown();
}

#if 0
extern double threshold;
extern int prev_sign;
extern int cross_count;
void send_log(message *pMsg)
{
	pMsg->count = 25;
	pMsg->data[0] = wheel_pos[2];
	pMsg->data[1] = err_pos;
	pMsg->data[2] = wheel_vel[2];
	pMsg->data[3] = nxt_err;
//	pMsg->data[4] = a;
//	pMsg->data[4] = nxt_err;
//	pMsg->data[5] = nxt_vel;
//	pMsg->data[5] = phase;
	pMsg->data[6] = cos(freq * (wheel_pos[1]+1.95) + phase);

	pMsg->data[0] = phasev[0];
	pMsg->data[1] = phasev[1];
	pMsg->data[0] = xfv[0];
	pMsg->data[1] = xfv[1];
	pMsg->data[2] = xfv[2];
//	pMsg->data[1] = odo[0];
//	pMsg->data[2] = moc[0];

#if 0
#if 1
	pMsg->data[4] = acc_term;
	pMsg->data[5] = pos_term;
	pMsg->data[6] = vel_term;
#else
	pMsg->data[4] = threshold;
	pMsg->data[5] = cross_count;
//	pMsg->data[6] = elapsed_time;
#endif
#else
//	pMsg->data[3] = err_posv[0];
//	pMsg->data[4] = err_posv[1];
//	pMsg->data[5] = wheel_vel[0];
//	pMsg->data[6] = wheel_vel[1];
	pMsg->data[3] = xdotfv[0];
	pMsg->data[4] = xdotfv[1];
	pMsg->data[5] = xdotfv[2];
//	pMsg->data[5] = phi;
	pMsg->data[6] = theta;
#endif

#if 0
	pMsg->data[7] = sCommand[0];
	pMsg->data[8] = nxt_vel;
//	pMsg->data[8] = prev_sign;
	pMsg->data[9] = time_gap;
#else
#if 0
	pMsg->data[7] = acc_termv[2];
	pMsg->data[8] = pos_termv[2];
	pMsg->data[9] = vel_termv[2];
#else
	pMsg->data[7] = sCommand[0];
	pMsg->data[8] = sCommand[1];
	pMsg->data[9] = sCommand[2];
//	pMsg->data[7] = aRef[0];
//	pMsg->data[8] = aRef[1];
//	pMsg->data[9] = aRef[2];
#endif
#endif
    
#if 0
	pMsg->data[0] = torque_calibrated[0];
	pMsg->data[1] = lp_torque[0]->output();;
    pMsg->data[10] = a[1];
    pMsg->data[11] = f;
    pMsg->data[12] = a_count;
#else
//  pMsg->data[0] = ((int)(xfv(2)*180/M_PI)+3600)%360;
//  pMsg->data[1] = ((int)(atan2(pPos->data[5], pPos->data[3])*2*180/M_PI)+3600)%360;
//  pMsg->data[2] = ((int)(wheel_ori*180/M_PI)+3600)%360;
#if 0
    pMsg->data[10] = pPos->data[0];
    pMsg->data[11] = pPos->data[1];
    pMsg->data[12] = pPos->data[2];
    pMsg->data[13] = pPos->data[3];
    pMsg->data[14] = pPos->data[4];
    pMsg->data[15] = pPos->data[5];
    pMsg->data[16] = pPos->data[6];
#else
//    pMsg->data[7] = sval[0];
//    pMsg->data[8] = sval[1];
//    pMsg->data[9] = sval[2];
//    pMsg->data[10] = sval[3];
//    pMsg->data[11] = sval[4];
#endif
    pMsg->data[17] = aRef[0];
    pMsg->data[18] = aRef[1];
    pMsg->data[19] = aRef[2];
    pMsg->data[20] = xdes[0];
    pMsg->data[21] = xdes[1];
    pMsg->data[22] = xdes[2];
    pMsg->data[23] = gSlope;
    pMsg->data[24] = gRegion;

//	pMsg->data[0] = torque_calibrated[0];
//	pMsg->data[1] = torque_calibrated[1];
//	pMsg->data[2] = torque_calibrated[2];
//	pMsg->data[3] = torque_calibrated_filtered[0];
//	pMsg->data[4] = torque_calibrated_filtered[1];
//  pMsg->data[5] = torque_calibrated_filtered[2];
#endif
	pMsg->data[0] = wheel_pos[0];
	pMsg->data[1] = lp_torque[0]->output();

	send_udp(pMsg);
}
#endif
