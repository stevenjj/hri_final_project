#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "comm_udp.h"

#define BUFLEN 1024

message *allocMessage(int count)
{
	message *pRet;

	pRet = (message *)malloc( sizeof(message) + sizeof(double)*(count-1) );
		
	return pRet;
}

void *notify_address(void *arg)
{
	int interval = (int)arg;	
	int slen;

	int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	struct sockaddr_in si_to;
	memset((char *) &si_to, 0, sizeof(si_to));
	si_to.sin_family = AF_INET;
	si_to.sin_port = htons(NOTIFY_PORT);
	si_to.sin_addr.s_addr = inet_addr(NOTIFY_ADDR);

//	fprintf(stderr, "ADDR: %08x(%d:%d)\n", si_to.sin_addr.s_addr, slen, sizeof(si_to));
	while (1)
	{
		char buf[10];
		sendto(s, buf, sizeof(buf), 0, &si_to, sizeof(si_to));
		fprintf(stderr, "NOTIFY CONTROL PC Address\n");
		sleep(interval);
	}
}

struct sockaddr_in si_other;
int slen;
void *receive_udp(void *arg)
{
	struct sockaddr_in si_me;
	int s;
	command *cmd = (double *)arg;

	if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
		return NULL;

	memset((char *) &si_me, 0, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(CMD_PORT);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
	bind(s, &si_me, (socklen_t*)sizeof(si_me));

	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(STT_PORT);
	si_other.sin_addr.s_addr = inet_addr("127.0.0.1");
	slen = sizeof(si_other);

	double buf[BUFLEN];
	
	fprintf(stderr, "COMM CMD THREAD started\n");
	while (1)
	{
        int len;
		len = recvfrom(s, cmd, sizeof(command), 0, &si_other, (socklen_t*)&slen);
		fprintf(stderr, "PACKET RECEIVE... %08x(%d),%d[%d],%d[%d],%d[%d],%d[%d],%d[%d],%d[%d],%d[%d] %d:%d\n", 
				cmd->command, cmd->data_len, 
                cmd->buf[0], cmd->exp[0], cmd->buf[1], cmd->exp[1], cmd->buf[2], cmd->exp[2], 
                cmd->buf[3], cmd->exp[3], cmd->buf[4], cmd->exp[4], cmd->buf[5], cmd->exp[5], 
                cmd->buf[6], cmd->exp[6], len, sizeof(command));	
		fprintf(stderr, "ADDR: %08x(%d)\n", si_other.sin_addr.s_addr, slen);
		cmd->received_check = 0;
//		head_command[4] = buf[4] * M_PI / 180.;
	}

	return NULL;
}

struct sockaddr_in si_pos;
int slen;
void *receive_pos(void *arg)
{
	struct sockaddr_in si_me;
	int s;
	message *msg = (double *)arg;

	if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
		return NULL;

	memset((char *) &si_me, 0, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(POS_PORT);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
	bind(s, &si_me, (socklen_t*)sizeof(si_me));

	double buf[BUFLEN];
	
	fprintf(stderr, "COMM POS THREAD started\n");
	while (1)
	{
		recvfrom(s, msg, sizeof(message)+sizeof(double)*20, 0, &si_pos, (socklen_t*)&slen);
//		fprintf(stderr, "PACKET RECEIVE... %d(%d),%d,%d,%d,%d,%d,%d,%d,%d\n", 
//				msg->index, msg->count, msg->data[0], msg->data[1], msg->data[2], 
//				msg->data[3], msg->data[4], msg->data[5], msg->data[6], msg->data[7]);	
//		fprintf(stderr, "ADDR: %08x(%d)\n", si_pos.sin_addr.s_addr, slen);
//		head_command[4] = buf[4] * M_PI / 180.;
	}

	return NULL;
}

int s = 0;
int index = 0;
void send_udp(message *pMsg)
{
	if ( s == 0 )
	{
		if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)

		return;
	}
    if ( pMsg == NULL || pMsg->count == 0 )
        return;

	struct sockaddr_in si_to;
	memset((char *) &si_to, 0, sizeof(si_to));
	si_to.sin_family = AF_INET;
	si_to.sin_port = htons(STT_PORT);
//	si_to.sin_addr.s_addr = si_other.sin_addr.s_addr;
	si_to.sin_addr.s_addr = inet_addr("192.168.1.106");
    slen = sizeof(si_to);

//	fprintf(stderr, "ADDR: %08x(%d:%d)\n", si_to.sin_addr.s_addr, slen, sizeof(si_to));
	pMsg->index = index;
	sendto(s, pMsg, sizeof(message) + sizeof(double)*(pMsg->count-1), 0, &si_to, slen);
	++index;
}
