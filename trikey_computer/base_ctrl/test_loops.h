jspace::Vector control_loop(void );
jspace::Vector test01_loop(void );
jspace::Vector test02_loop(void );
jspace::Vector test03_loop(void );
jspace::Vector test04_loop(void );
jspace::Vector test05_loop(void );
jspace::Vector test06_loop(void );
jspace::Vector test07_loop(void );
jspace::Vector test08_loop(void );
jspace::Vector test09_loop(void );
jspace::Vector test10_loop(void );
jspace::Vector test11_loop(void );
jspace::Vector test12_loop(void );
jspace::Vector test13_loop(void );
jspace::Vector test14_loop(void );
jspace::Vector test15_loop(void );
jspace::Vector test16_loop(void );
jspace::Vector test17_loop(void );
jspace::Vector test18_loop(void );
jspace::Vector test19_loop(void );
jspace::Vector test20_loop(void );
jspace::Vector test21_loop(void );
jspace::Vector test22_loop(void );
jspace::Vector test23_loop(void );
jspace::Vector simple_vel_control_loop(void );

class test_loop
{
    test_loop(void);
    ~test_loop(void);

    virtual void init(void) = 0;
    virtual void loop(double time_ns)= 0;
};

void test06_init(void);
void test13_init(void);
void test17_init(void);
void test15_set_command(double remote_command[3]);
