#include <fileio.h>
#include <Eigen/Dense>

using namespace std;
using Eigen::MatrixXf;
using Eigen::MatrixXd;
using Eigen::VectorXd;

#define R   	(.204)
#define r   .1 
#define B		(9.)
#define alpha	(20.1)
#define M   40.8 
#define m   0.001 
#define IM  2.11 
#define Imx  0.009127 
#define Imz  0 

#if 0
double tanh(double in)
{
	double out;

	out = (exp(in) - exp(-in)) / (exp(in) + exp(-in));

	return out;
}
#endif
VectorXd tanh(VectorXd in)
{
	VectorXd out;
	
	out = in;
	out(0) = tanh(in(0));
	out(1) = tanh(in(1));
	out(2) = tanh(in(2));

	return out;
}
int main(int argc, char* argv[])
{
	if ( argc < 2 )
	{
		exit(-1);
	}

	data_stream ds = data_stream(argv[1]);

	int num;

	fprintf(stderr, "sizeof(double): %d\n", (unsigned int)sizeof(double));
	VectorXd x;

	x = VectorXd::Zero(3);

	double lastTime = 0.;
	while ( (num = ds.read_line()) > 0 )
	{
		vector<double> entry = ds.get_entry();
		VectorXd q, qdot, torque, q_des, qd_des, qdd_des, x_des;
		VectorXd xdot, xd_des, xdd_des, u ,g, Br; 
		MatrixXd Jcr, Jcw, H, Q, QInv, C, D, Px, Rv;
		MatrixXd Mass, JcrT, JcrTInv, JcwT, JcwInv, JcwTInv, gdot, Br_dot;
		double phi;
		int		idx = 0;
		double timeStamp, dt;

		q = qdot = torque = q_des = qd_des = qdd_des = x_des = VectorXd::Zero(3);
		
		timeStamp	= entry[idx++];
		q[0]		= entry[idx++];
		q[1]		= entry[idx++];
		q[2]		= entry[idx++];
		qdot[0]		= entry[idx++];
		qdot[1]		= entry[idx++];
		qdot[2]		= entry[idx++];
		torque[0]	= entry[idx++];
		torque[1]	= entry[idx++];
		torque[2]	= entry[idx++];
		q_des[0]	= entry[idx++];
		q_des[1]	= entry[idx++];
		q_des[2]	= entry[idx++];
		qd_des[0]	= entry[idx++];
		qd_des[1]	= entry[idx++];
		qd_des[2]	= entry[idx++];
		qdd_des[0]	= entry[idx++];
		qdd_des[1]	= entry[idx++];
		qdd_des[2]	= entry[idx++];
		x_des[0]	= entry[idx++];
		x_des[1]	= entry[idx++];
		x_des[2]	= entry[idx++];

		Px = MatrixXd::Identity(6,6);
		Px(0,0) = 1;
		Px(1,1) = 1;
		Px(2,2) = 1;
		Px(3,3) = 10;
		Px(4,4) = 10;
		Px(5,5) = 10;
		Rv = MatrixXd::Identity(6,6)*100;
		Rv(0,0) = 10;
		Rv(1,1) = 10;
		Rv(2,2) = 10;
		Rv(3,3) = .1;
		Rv(4,4) = .1;
		Rv(5,5) = .1;

		if ( lastTime != 0. )
			dt = (timeStamp - lastTime) / 1000000.;
		else
			dt = 0.;

		phi = x(2);

		Mass = MatrixXd::Zero(3,3);
		Mass(0,0) = M + 3*m;
		Mass(1,1) = M + 3*m;
		Mass(2,2) = IM + 3*Imx;

		Jcw = MatrixXd::Zero(3,3);
		Jcw(0,0) = -sin(phi);
		Jcw(0,1) = cos(phi);
		Jcw(0,2) = R;
		Jcw(1,0) = -sin(phi-2*M_PI/3);
		Jcw(1,1) = cos(phi-2*M_PI/3);
		Jcw(1,2) = R;
		Jcw(2,0) = -sin(phi+2*M_PI/3);
		Jcw(2,1) = cos(phi+2*M_PI/3);
		Jcw(2,2) = R;
		JcwT	= Jcw.transpose();
		JcwInv	= Jcw.inverse();
		JcwTInv	= JcwInv.transpose();

		Jcr = MatrixXd::Zero(3,3);
		Jcr(0,0) = cos(phi);
		Jcr(0,1) = sin(phi);
		Jcr(1,0) = cos(phi-2*M_PI/3);
		Jcr(1,1) = sin(phi-2*M_PI/3);
		Jcr(2,0) = cos(phi+2*M_PI/3);
		Jcr(2,1) = sin(phi+2*M_PI/3);
		JcrT	= Jcr.transpose();
		JcrTInv	= JcrT.inverse();

		xdot	= JcwInv * qdot;
		xd_des	= JcwInv * qd_des;
		xdd_des	= JcwInv * qdd_des;

		VectorXd qrdot = Jcr*xdot;
		Br = VectorXd::Zero(3);
		Br(0) = B * tanh(alpha*((Jcr*xdot)(0)));
		Br(1) = B * tanh(alpha*((Jcr*xdot)(1)));
		Br(2) = B * tanh(alpha*((Jcr*xdot)(2)));

#if 0
		VectorXd Br2;
		Br2 = B * tanh(alpha*(qrdot));

		if (Br[0] != Br2[0] || Br[1] != Br2[1] || Br[2] != Br2[2] )
		{
			fprintf(stderr, "===%f %f %f %f %f %f\n",
				Br[0], Br2[0], Br[1], Br2[1], Br[2], Br2[2] );
		}
#endif
		Br_dot = MatrixXd::Zero(3,3);
		Br_dot(0,0) = B * (1 + pow(tanh(alpha*qrdot(0)),2.))*alpha*Jcr(0,0);
		Br_dot(0,1) = B * (1 + pow(tanh(alpha*qrdot(0)),2.))*alpha*Jcr(0,1);
		Br_dot(0,2) = B * (1 + pow(tanh(alpha*qrdot(0)),2.))*alpha*Jcr(0,2);
		Br_dot(1,0) = B * (1 + pow(tanh(alpha*qrdot(1)),2.))*alpha*Jcr(1,0);
		Br_dot(1,1) = B * (1 + pow(tanh(alpha*qrdot(1)),2.))*alpha*Jcr(1,1);
		Br_dot(1,2) = B * (1 + pow(tanh(alpha*qrdot(1)),2.))*alpha*Jcr(1,2);
		Br_dot(2,0) = B * (1 + pow(tanh(alpha*qrdot(2)),2.))*alpha*Jcr(2,0);
		Br_dot(2,1) = B * (1 + pow(tanh(alpha*qrdot(2)),2.))*alpha*Jcr(2,1);
		Br_dot(2,2) = B * (1 + pow(tanh(alpha*qrdot(2)),2.))*alpha*Jcr(2,2);

//////////////////////////////////
		VectorXd xh, y, w;

		xh = VectorXd::Zero(6);
		xh.block(0,0,3,1) = x;
		xh.block(3,0,3,1) = xdot;
		
		C = MatrixXd::Zero(6,6);
		C.block(0,3,3,3) = Jcw;
		C.block(3,3,3,3) = r*JcwTInv * JcrT * Br_dot;
		D = MatrixXd::Zero(6,3);
		D.block(3,0,3,3) = JcwTInv;
		Q = C * Px * C.transpose() + Rv;
		QInv = Q.inverse();
		H = (D.transpose() * QInv * D).inverse() * D.transpose() * QInv;

		g = VectorXd::Zero(6);
		g.block(0,0,3,1) = qdot;
		g.block(3,0,3,1) = r* JcwTInv * JcrT * Br;
		y = VectorXd::Zero(6);
		y.block(0,0,3,1) = qdot;
		y.block(3,0,3,1) = -torque - r * JcwTInv* M*0.1*xdd_des;

		w = y - g + C*xh;

		u = H * ( w - C*xh );
//		u =  ( w - C*xh );

////////////////////////////////
		VectorXd qrd, lcr, lcw, intforce, Fext;
//		qrd		= Jcr * xdot;
	//	lcr		= tanh(qrd_des*20.1)*9;
	//	lcr		= B*tanh(alpha*Jcr*xdot);
		lcr		= Br;
		lcw		= - JcwTInv * (0.1*Mass* xdd_des + JcrT * lcr) ;
		intforce	=  Imz*qdd_des - r * lcw;

	//	ext = 0.1*Mass* xdd_des + Jcw.transpose()*ts/r + Jcr.transpose() * B;
		Fext = JcwTInv*(torque + intforce);
//		torque +  Imz*qdd_des - r * ( - JcwInv.transpose() * (0.1*Mass* xdd_des + Jcr.transpose() * Br)) ;
//		torque +  Imz*qdd_des + r * JcwInv.transpose() * (0.1*Mass* xdd_des + Jcr.transpose() * Br) ;

////////////////////////////////
		x = x + xdot * dt;
		lastTime = timeStamp;
	
		printf("%f ", timeStamp);
		printf("%f %f %f ", u(0), u(1), u(2));
		printf("%f %f %f ", Fext(0), Fext(1), Fext(2));
		printf("%f %f %f %f %f %f ", xh[0], xh[1], xh[2], xh[3], xh[4], xh[5] );
//		printf("%f %f %f %f %f %f\n", Br[0], Br2[0], Br[1], Br2[1], Br[2], Br2[2] );
//		printf("%f %f %f %f %f %f\n", y(0), y(1), y(2), y(3), y(4), y(5));
//		printf("%f %f %f\n", torque(0), torque(1), torque(2));
		printf("\n");
#if 0
		fprintf(stderr, "=====================\n");
		fprintf(stderr, "%5f %5f %5f %5f %5f %5f\n", H(0,0), H(0,1), H(0,2), H(0,3), H(0,4), H(0,5)); 
		fprintf(stderr, "%5f %5f %5f %5f %5f %5f\n", H(1,0), H(1,1), H(1,2), H(1,3), H(1,4), H(1,5)); 
		fprintf(stderr, "%5f %5f %5f %5f %5f %5f\n", H(2,0), H(2,1), H(2,2), H(2,3), H(2,4), H(2,5)); 
#endif
		fprintf(stderr, "%f %f %f\n", x(0), x(1), x(2));

	}

	return 0;
}
