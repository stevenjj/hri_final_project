#include <stdio.h>
#include "base_config.h"
#include "task.h"
#include <vector>
#include <fstream>
#include <string>
#include <math.h>
#include <Eigen/Dense>
#include "inetclientstream.hpp"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#define S_PI (3.14159265358979323846)

using Eigen::MatrixXd;

extern double sCommand[3];

using libsocket::inet_stream;

class Task02_kin: public Task
{
	public:
	Task02_kin(void);
       ~Task02_kin(void){
logData.close();
};


	virtual void init(void);
    virtual void init(int argc, char *argv[]);
	virtual jspace::Vector update(void);
	virtual string getName(void);
    virtual void log(void);


//    void getTargetbystep(int step);
    void UpdateJacobian(const double* config);
    void Updatepos(const double* despos);
    void Estimatepos();
    double getError(const Vector TargetPos,const double* curPos);
    Vector Get_des_Q(const double* config,Vector Targetpos);

     ofstream logData;
     ifstream r_logData;  

     double Jacobi[3][3];
     double currentconfig[3];
     double zeroconfig[3];
     double Targetfig[3];
     double currentpos[3];
     double ErrorTarget[3];
     double  des_pos[3];
     double estimated_pos[3];
     double q_before[3];
     int _count;
     int flag;
     double Gain_split;

    private:
    int index;

    //mk;
    MatrixXd m_Jacobi;

    std::string host;
    std::string port;
    std::string answer;

};

string Task02_kin::getName(void)
{
	return "Task02_kin";
}
Task02_kin::Task02_kin(void)
{
	cout << "Creating " << getName() << endl;
    host = "192.168.1.119";
    port = "1335";
    answer.resize(32);

    for(int i(0);i<3;i++)
    {
        currentconfig[i]=0.0;
        Targetfig[i]=0.0;
        currentpos[i]=0.0;
        ErrorTarget[i]=0.0;
	zeroconfig[i]=0.0;
        des_pos[i]=0.0;
        estimated_pos[i]=0.0;    
        q_before[3];

      for(int j(0);j<3;j++)
         Jacobi[i][j]=0.0;
    }

    
  m_Jacobi.resize(3,3);   
    m_Jacobi.setZero();

 
     Gain_split=0.005;
     flag=0;
    _count=0;
     vector<string> tokens;
     string lines;

     r_logData.open("configureation_history.dat");

     if(!r_logData.is_open()){
       printf("Data file load error\n");    
     }
     else{

      while(!r_logData.eof())
      {  getline(r_logData,lines);
      boost::split(tokens,lines,boost::is_any_of("\t"));
 
     if(tokens.size()>2)
      {
        for(int i(0);i<3;i++)
          currentconfig[i]=boost::lexical_cast<double>(tokens[i]); 
       }
     }
     }
    

    printf("Read file is \n");
    for(int i(0);i<3;i++)
      cout<<i<<": "<< currentconfig[i]<<endl;
     
   
}
void Task02_kin::init(void)
{
	mode = CTRL_MODE_THETA;
}

double q_orgi[3];
void Task02_kin::init(int argc, char *argv[])
{
    index = 0;
	mode = CTRL_MODE_THETA;

        for(int i(0);i<3;i++){
           q_orgi[i]=currentconfig[i];
           estimated_pos[i]=currentconfig[i];
        }
//	q_orgi[0] = currentconfig[0];
//	q_orgi[1] = currentconfig[1];
//	q_orgi[2] =currentconfig[2];
 
     //File open
     logData.open("configureation_history.dat");

	//If there is flag when execution, save it as a flag 
       if(argv[1])
       { 
          flag = boost::lexical_cast<int>(argv[1]);   
          printf("Input flag : %d \n",flag);
       }
       else
       {
        	flag=1;
       }  
}

jspace::Vector Task02_kin::update(void)
{

//   std::cout << "hello" << std::endl;
/*
   try {
       libsocket::inet_stream sock(host,port,LIBSOCKET_IPv4);
        sock >> answer;
        std::cout << answer;
        sock << "Hello back!\n";
        //sock is closed here automatically!
    } catch (const libsocket::socket_exception& exc){
       std::cerr << exc.mesg;
   }

*/
    Vector command = Vector::Zero(3);
	static int count = 0;
  //  static int flag=0;

    Vector desiredPos= Vector::Zero(3);
    Vector Targetpos= Vector::Zero(3);
   
        switch(flag)
        {
           case 0 : 
                     q_orgi[0]=0.0;
                     q_orgi[1]=0.0;
                     q_orgi[2]=0.0;
                     
                     Targetpos[0]=0.0;
                     Targetpos[1]=0.0;
                     Targetpos[2]=0.0;

                     break;  
            case 1:
                     Targetpos[0]=-1.5;
                     Targetpos[1]=0.0;
                     Targetpos[2]=0.0;
            break;
            case 2:  Targetpos[0]=1.5;
                     Targetpos[1]=0.0;
                     Targetpos[2]=0.0;

            break;

          case 3:
                     Targetpos[0]=-0.0;
                     Targetpos[1]=1.5;
                     Targetpos[2]=0.0;
            break;
            case 4:  Targetpos[0]=0.0;
                     Targetpos[1]=-0.8;
                     Targetpos[2]=0.0;

            break;

            case 5:  Targetpos[0]=0.6;
                     Targetpos[1]=0.6;
                     Targetpos[2]=0.0;

            break;
            case 6 :
                     Targetpos[0]=-0.6;
                     Targetpos[1]=-0.6;
                     Targetpos[2]=0.0;
               break;
             case 7 :
                      Targetpos[0]=0.0;
                     Targetpos[1]=0.0;
                     Targetpos[2]=-S_PI;


               

                  break;

            default:  Targetpos[0]=0.0;
                      Targetpos[1]=0.0;
                      Targetpos[2]=0.0;
             break;

        }
   
          int n_step=10;       
         //get dq with dx 
         desiredPos =Get_des_Q(currentpos,Targetpos);
         
	for(int i(0);i<3;i++) 
         desiredPos[i]+=currentconfig[i];
 
         //desired q = currentq + dq
         command[0]=desiredPos[0];
         command[1]=desiredPos[1];
         command[2]=desiredPos[2];


       double errors= getError(desiredPos,q);
    
       if(errors<0.015)
       {
        // command[0]=q_orgi[0]+desiredPos[0];
        // command[1]=q_orgi[1]+desiredPos[1];
        // command[2]=q_orgi[2]+desiredPos[2];
    
        //  command[0]=q[0];
        // / command[1]=q[1];
        //  command[2]=q[2];

          Updatepos(des_pos);
        
          for(int i(0);i<3;i++) 
          currentconfig[i]=q[i];
 
   //      printf("errors is :  %+7lf \n", errors);  
            

       }
   //   if(count>1500)       
    // {

      //    Updatepos(des_pos);
    // }

      // else


  	// command[0] = q[0]-count/10000.;
	// command[1] = q[1]-count/10000.;
	// command[2] = q[2]-count/10000.;
/*
       printf("current pos is :");
       for(int i(0);i<3;i++)
           printf(" %d : %+7lf ", i, currentpos[i]);
      printf("\n");

       printf("estimated pos is :");
       for(int i(0);i<3;i++)
           printf(" %d : %+7lf ", i, estimated_pos[i]);
      printf("\n");




       printf("current q is :");
       for(int i(0);i<3;i++)
           printf(" %d : %+7lf ", i, q[i]);
      printf("\n");

       printf("current des_pos: ");
       for(int i(0); i<3; i++)
         printf(" %d : %+7lf ", i, desiredPos[i]);
      printf("\n");

/*       printf("current pos: ");
       for(int i(0); i<3; i++)
         printf(" %d : %+7lf ", i, currentpos[i]);
      printf("\n");
*/
//      printf("errors is :  %+7lf \n", errors);  
 
       // currentconfig[0]=command[0];
       // currentconfig[1]=command[1];
       // currentconfig[2]=command[2];


	mode = CTRL_MODE_THETA;
	count++;
       _count++;

       for(int i(0);i<3;i++)
        q_before[i]=q[i];

//	std::cout<<"Count: "<<cout<<endl;
	return command;
}

double Task02_kin::getError(const Vector TargetPos, const double* curPos)
{
/*
    double q_res[3];

  for(int i(0);i<3;i++)
{   q_res[i]=q[i];


    while(abs(q[i])>S_PI)
{
    if(q[i]>0)
      q_res[i]=q_res[i]-S_PI;
     else
       q_res[i]+=S_PI;  

}
}
*/
//    double currentpos;

//    if((TaggetPos!=NULL) && (Error!=NULL))
   
 
        ErrorTarget[0]=q[0]-TargetPos[0];
        ErrorTarget[1]=q[1]-TargetPos[1];
        ErrorTarget[2]=q[2]-TargetPos[2];        

        double err=0.0;
       for(int i(0);i<3;i++)
          err+=ErrorTarget[i]*ErrorTarget[i];

	err=sqrt(err);

return err;

}

void Task02_kin::Estimatepos()
{
double wheel_radius=0.106; 

  MatrixXd diff_q;
  MatrixXd diff_x;
  diff_q.resize(3,1);
  diff_q.setZero();

   for(int i(0);i<3;i++)
     diff_q(i)=q[i]-q_before[i];

// double aa= m_Jacobi.determinant();
 // MatrixXd J_inv= m_Jacobi.inverse(); 
 
   diff_x=wheel_radius* m_Jacobi.inverse()*diff_q;

   for(int i(0);i<3;i++)
      estimated_pos[i]=estimated_pos[i]+diff_x(i);    

}



void Task02_kin::UpdateJacobian(const double* curconfig)
{

    double _x = curconfig[0];
    double _y = curconfig[1];
    double _theta=curconfig[2];
 //   double _theta=1.1;

    double   R_robot=0.2286;      //9in measured                                           
    double  wheel_radius = 0.106; //4in measured
    double  r_inv=1.0/wheel_radius;
    //This Jacobian can be calculated as in e:quation (14) in 
    m_Jacobi.resize(3,3);   
    m_Jacobi.setZero();

    Jacobi[0][0]=-r_inv*sin(_theta);                 Jacobi[0][1]=r_inv*cos(_theta);                  Jacobi[0][2]=r_inv* R_robot;
    Jacobi[1][0]=-r_inv*sin(_theta-(2.0/3.0*S_PI));  Jacobi[1][1]=r_inv*cos(_theta-(2.0/3.0*S_PI));   Jacobi[1][2]=r_inv*R_robot;  
    Jacobi[2][0]=-r_inv*sin(_theta+(2.0/3.0*S_PI));  Jacobi[2][1]=r_inv*cos(_theta+(2.0/3.0*S_PI)) ;  Jacobi[2][2]=r_inv*R_robot;
   
    m_Jacobi(0,0)=-sin(_theta);                 m_Jacobi(0,1)=cos(_theta);                  m_Jacobi(0,2)= R_robot;
    m_Jacobi(1,0)=-sin(_theta-(2.0/3.0*S_PI));  m_Jacobi(1,1)=cos(_theta-(2.0/3.0*S_PI));   m_Jacobi(1,2)=R_robot;  
    m_Jacobi(2,0)=-sin(_theta+(2.0/3.0*S_PI));  m_Jacobi(2,1)=cos(_theta+(2.0/3.0*S_PI)) ;  m_Jacobi(2,2)=R_robot;
 

}

void Task02_kin::Updatepos(const double* des_pos)
{

   for(int i(0);i<3;i++)
        currentpos[i]+=des_pos[i];
 
}


Vector Task02_kin::Get_des_Q(const double* config, Vector Targetpos)
{

    int dim_const=3;
   // error2Target = new double [dim_const];
 //   for(int i=0; i< dim_const;i++)
   //     error2Target[i]=0.0;

    UpdateJacobian(currentpos);
//    hey.resize(3);
//    getError( TargetPos, error2Target);
  //  Vector des_pos= Vector::Zero(3);
  //  des_pos.resize(3,1);

    for(int i=0; i< dim_const;i++)
    { 
        des_pos[i]=Gain_split*(Targetpos[i]-currentpos[i]);
     //  currentpos[i]=currentpos[i]+des_pos[i];
    }


    Vector Des_Q=Vector::Zero(3);
    Des_Q.setZero();

  for(int j(0) ; j<3; j++)
   {
        for(int i(0);i<3;i++)
        Des_Q[j]+=Jacobi[j][i]*des_pos[i];
        
    }
    
   Estimatepos();       

//for(int i(0);i<3;i++)
  //   printf("Targetpos %d : %+7lf \n" , i, des_pos[i]);

//for(int i(0);i<3;i++)
// printf("des pos %d : %+7lf\n",i,Des_Q[i]); 
/*
for(int k(0);k<3;k++)
 {
 for(int l(0);l<3;l++)
    printf("Jacobian %d, %d = %+7lf",k,l,Jacobi[k][l]);  

printf("\n");
}  
printf("\n");

printf("sin(30): %lf , cons(30) : %lf\n ", sin(S_PI/6),cos(S_PI/6));
*/


/*    for(int j(0);j<3;j++)
       { 
            printf("Desired %d is %+7lf ",j,Des_Q[j]);
        
        }
	printf("\n");
       printf("Count number is : %d \n",_count);
  //  delete [] error2Target;
*/
return Des_Q;
}


void Task02_kin::log(void)
{
/*
    fprintf(stderr, "q\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", q[i]);

/*
    fprintf(stderr, "\n");
    fprintf(stderr, "qdot\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", qdot[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "raw_torque\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", raw_torque[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "command\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", sCommand[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "pwm\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", pwm_cmd[i]);
    fprintf(stderr, "\n");
	fprintf(stderr, "SP %f, %f\n", shm_param.sp_g[index], shm_param.sp_d[index]);
 //  fprintf(stderr,"count : %d \n ",_count);
*/

//logData.open("config_history.dat");

for(int i(0);i<3;i++)
  logData<<q[i]<<"\t";
logData<<endl;

//logData.close();

}


TaskRegistrar<Task02_kin> Task02_kin;
