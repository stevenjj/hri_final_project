#include <stdio.h>
#include "task.h"

extern double sCommand[3];
extern double remote_command[10];

#define ACC     (.05) // rad/s^2
#define DEACC     (.07) // rad/s^2
#define W_MAX (0.1) // rad/s
double cur_vel[3];
double err_pos;
double nxt_vel, nxt_err;
int getSign(double a, double pos, double vel)
{
    int sign; 

    if ( pos == 0. )
    {
        sign = 0;
    }
    else if ( pos > a*vel*vel )
    {
        sign = -1;
    }
    else if ( pos < -a*vel*vel )
    {
        sign = 1;
    }
    else if ( vel > 0 )
    {
        sign = -1;
    }
    else
    {
        sign = 1;
    }
    
    return sign;
}
class Task10: public Task
{
	public:
	Task10(void);
	virtual void init(void);
    virtual void init(int argc, char *argv[]);
	virtual jspace::Vector update(void);
	virtual string getName(void);
    virtual void log(void);
    virtual void log(message *pMsg);
    int getSign(double a, double pos, double vel);

    private:
    int index;
    double omega;
    double dir;
    double theta;
    double lastSec;
    double min_torque;
    double max_torque;
    double period;
    double elapsed;
    double impulse;
    double torque_cmd;
    double torque_step;

    double desired_pos;
    double err_posv[3];
    double cur_vel[3];
    double err_pos;
    double nxt_vel, nxt_err;
};
string Task10::getName(void)
{
	return "Task10";
}
Task10::Task10(void)
{
	cout << "Creating " << getName() << endl;
    desired_pos = 0;
}

void Task10::init(void)
{
    period = 3.;
    impulse = 0.5;
    elapsed = timeStamp / 1000000.;

    torque_cmd = min_torque;
}

void Task10::init(int argc, char *argv[])
{
    index = 0;
    min_torque = 0.3;
    max_torque = 1.;
    torque_step = 0.1;
    if ( argc >= 2 )
        index = atoi(argv[1]);
    if ( argc >= 3 )
        min_torque = atof(argv[2]); 
    if ( argc >= 4 )
        max_torque = atof(argv[2]); 

    init();
}

jspace::Vector Task10::update(void)
{
    static double direction = 1.;
    double acc;
    static int old_sign = 0;

    // New Command arrived 
    if ( desired_pos != remote_command[0] )
    {
        desired_pos = remote_command[0];
	    err_posv[index] = q[index] - desired_pos;
        sign[index] = getSign(.5/ACC, err_posv[index], qdot[index]);
        phasev[index] = 0.;
        old_sign = 0.;

        cross_point[index] = err_posv[index] - .5/ACC*sign[index]*qdot[index]*qdot[index];
    }
	err_posv[index] = q[index] - desired_pos;

    jspace::Vector command(3);
    double mass = remote_command[2];
	double kp =remote_command[3];
	double kv =remote_command[4];
    double nxt_step = 3.0;
    double alpha, beta;
    double a, b, c;
    double abs_err = fabs(err_posv[index]);
    double speed;
    double cmd;

    if ( phasev[index] == 0. )
    {
        sign[index] = getSign(.5/ACC, err_posv[index], qdot[index]);
        if ( old_sign != 0 && sign[index] != old_sign )
        {
            phasev[index] = 1;
            cross_point[index] = 0.;
        }
        if ( qdot[index] > 0 )
            nxt_vel = sqrt(fabs(err_posv[index]-cross_point[index])*2*ACC) ;
        else if ( qdot[index] < 0 )
            nxt_vel = -sqrt(fabs(err_posv[index]-cross_point[index])*2*ACC) ;


    }
    else if ( phasev[index] == 1. )
    {
        nxt_vel = - sign[index] * sqrt(2*ACC*abs_err);
    }
    nxt_err = nxt_vel * nxt_vel * sign[index] * 0.5 / ACC + cross_point[index];
    acc = ACC * sign[index];

    if ( nxt_vel >= W_MAX )
    {
        acc = -ACC;
        nxt_vel = W_MAX; // + acc * nxt_step;
        kp = 0.;
    }
    else if ( nxt_vel <= -W_MAX )
    {
        acc = ACC;
        nxt_vel = -W_MAX; // + acc * nxt_step;
        kp = 0.;
    }

//    if ( acc*nxt_err > 0 )
//       nxt_err = 0;

    acc_termv[index] = mass * acc * (1-exp(-0.3*abs_err));
    pos_termv[index] = kp * ( err_posv[index] - nxt_err );
    vel_termv[index] = kv * ( qdot[index] - nxt_vel );// * (1-exp(-0.1*fabs(qdot - nxt_vel)));
    cmd = acc_termv[index] - pos_termv[index] - vel_termv[index];
    command[index] = cmd + compensate_friction(cmd, qdot[index], nxt_vel, abs_err, phasev[index]);
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

    old_sign = sign[index];

	return command;
}

void Task10::log(void)
{
    fprintf(stderr, "q\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", q[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "qdot\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", qdot[i]);
    fprintf(stderr, "Time : %f", timeStamp/1000000.);
    fprintf(stderr, " Omega : %f", omega);
    fprintf(stderr, "\n");
    fprintf(stderr, "command\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", sCommand[i]);
    fprintf(stderr, "\n");
}

void Task10::log(message *pMsg) 
{ 
    pMsg->count = 10; 

    pMsg->data[0] = q[index];
    pMsg->data[1] = qdot[index];
    pMsg->data[2] = torque[index];
    pMsg->data[3] = raw_torque[index];
    pMsg->data[4] = omega;
    pMsg->data[5] = timeStamp/1000000.;
    pMsg->data[7] = sCommand[index];
}

int Task10::getSign(double a, double pos, double vel)
{
    int sign; 

    if ( pos == 0. )
    {
        sign = 0;
    }
    else if ( pos > a*vel*vel )
    {
        sign = -1;
    }
    else if ( pos < -a*vel*vel )
    {
        sign = 1;
    }
    else if ( vel > 0 )
    {
        sign = -1;
    }
    else
    {
        sign = 1;
    }
    
    return sign;
}
TaskRegistrar<Task10> task10;

jspace::Vector test07_loop(void )
