#include <stdio.h>
#include "base_config.h"
#include "task.h"
#include <vector>
#include <math.h>
#include "inetclientstream.hpp"


#define S_PI (3.14159265358979323846)

using Eigen::MatrixXd;

extern double sCommand[3];

using libsocket::inet_stream;

class Task02_socket: public Task
{
	public:
	Task02_socket(void);

	virtual void init(void);
    virtual void init(int argc, char *argv[]);
	virtual jspace::Vector update(void);
	virtual string getName(void);
    virtual void log(void);


   // static int count=0;

    private:
    int index;

    //mk;
    MatrixXd m_Jacobian;

    std::string host;
    std::string port;
    std::string answer;

};

string Task02_socket::getName(void)
{
	return "Task02_socket";
}
Task02_socket::Task02_socket(void)
{
	cout << "Creating " << getName() << endl;
    host = "10.0.0.40";
    port = "1235";
    answer.resize(32);
 //  printf("host is %s \n",host);
 // printf("prot is %s \n",port);

}
void Task02_socket::init(void)
{
	mode = CTRL_MODE_THETA;
}

double q_orginal[3];
void Task02_socket::init(int argc, char *argv[])
{
    index = 0;
	mode = CTRL_MODE_THETA;
	q_orginal[0] = q[0];
	q_orginal[1] = q[1];
	q_orginal[2] = q[2];



}

jspace::Vector Task02_socket::update(void)
{
   std::cout << "hello" << std::endl;
   try {
       libsocket::inet_stream sock(host,port,LIBSOCKET_IPv4);
        sock >> answer;
        std::cout << answer;
        sock << "Hello back!\n";
        //sock is closed here automatically!
    } catch (const libsocket::socket_exception& exc){
       std::cerr << exc.mesg;
   }

    Vector command = Vector::Zero(3);

  static int count=0;
   
	command[0] = q_orginal[0]-count/10000.;
	command[1] = q_orginal[1]-count/10000.;
	command[2] = q_orginal[2]-count/10000.;
	mode = CTRL_MODE_THETA;
	count++;

	return command;
}



void Task02_socket::log(void)
{
/*    fprintf(stderr, "q\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", q[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "qdot\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", qdot[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "raw_torque\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", raw_torque[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "command\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", sCommand[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "pwm\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", pwm_cmd[i]);
    fprintf(stderr, "\n");
	fprintf(stderr, "SP %f, %f\n", shm_param.sp_g[index], shm_param.sp_d[index]);
    fprintf(stderr,"cout \n");
  */ 
}


TaskRegistrar<Task02_socket> task02_socket;
