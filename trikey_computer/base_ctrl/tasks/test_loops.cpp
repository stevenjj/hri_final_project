#include <stdio.h>
#include <iostream>
#include <Eigen/Dense>
#include <vector>
#include <jspace/test/sai_util.hpp>
#include <opspace/Skill.hpp>
#include <opspace/Factory.hpp>
#include "base_config.h"
#include "filters.h"
#include "test_loops.h"
#include "tools.h"

using Eigen::MatrixXd;
using Eigen::VectorXd;
using namespace std;

#define M   40.8
#define m   0.001
#define R   .204
#define r   .1
#define IM  2.11
#define Imx  0.009127
#define Imz  0
#define g 9.81

extern MatrixXd xdotfv;
extern MatrixXd xfv;
extern MatrixXd odo;
extern MatrixXd odo_dot;
extern MatrixXd destv;
extern long long   sBaseTimeStamp;
extern long long   sTimeStamp;          // micro-second
extern double wheel_pos[3];
extern double wheel_ori;
extern double wheel_vel[3];
extern double raw_adc_value[NUM_SAMPLE][3];
extern double remote_command[10];
extern double sCommand[3];
extern double opcmd[3];
extern double calibrate_torque(int index, double value);
extern double freq;
extern double phase;
extern float motorTorqueComand_Last_Nm[3];
extern int operation_mode;
extern int control_mode;
extern int state_mode[3];
extern filter *lp_object[3];
extern filter *lp_torque[3];
filter *lp;
extern double theta, phi, psi;
extern MatrixXd aRef;
extern double xdes[3];
int destIndex = -1;
MatrixXd err_posv;
double sval[5];

extern float P_term[3];
extern float I_term[3];
extern float D_term[3];
extern double wheelTorqueErrorSum_Nm[3];
static int loop_counter=0; //counter reducing number of screen prints (kind of a hack)


static bool firstLoop[3] = {true, true, true};

double compensate_friction(double cmd, double curVel, double desVel, double err, double phase)
{
    double b = remote_command[3];

#if 0
    if ( desVel > 0.1 )
        b *= 1.;
    else if ( desVel < -0.1 )
        b *= -1.;
    else if ( cmd > 0. )
        b *= 1.;
    else if ( cmd < 0. )
        b *= -1.;
    else
        b = 0.;
#else
#if 0
    if ( curVel > 0.1 && desVal > curVel )
        b *= 1.;
    else if ( curVel > 0.1 && desVal > curVel )
        b *= 2.;
    else if ( desVel < -0.1 )
        b *= -1.;
    else if ( cmd > 0. )
        b *= 1.;
    else if ( cmd < 0. )
        b *= -1.;
    else
        b = 0.;
#else
    double sign = 1.;
//    if ( curVel * desVel < 0 )
 //       sign *= -1.;
    if ( curVel > 0.3 )
        b *= 1;
    else if ( curVel < -0.3 )
        b *= -1;
    else if ( cmd > 0. )
        b *= 1.0;
    else if ( cmd < 0. )
        b *= -1.0;
    else
        b = 0.;
#endif
#endif

#if 0
    if ( phase == 0.5 )
        b = 0.;

    if ( err < 0.5 )
        b *= err*2;
#endif

    return b;
}

double torque_command(int i, double Nm)
{
	float MaxMotorTorque_Nm = 10;
	
	double offset[3] = { 0., -0.235, 0.};
	double scale[3] = { 1000., 1000., 1000.};
//	double scale[3] = { 0., 1000., 1200.};

	//safety checks
	if (Nm > MaxMotorTorque_Nm){
	  Nm = MaxMotorTorque_Nm;
	}
	else if (Nm < -MaxMotorTorque_Nm){
	  Nm = -MaxMotorTorque_Nm;
	}

	sCommand[i] = (Nm + offset[i]) * scale[i];
	return sCommand[i];
}

jspace::Vector test01_loop(void )
{
	int i;
	static double force = 0;
	jspace::Vector command(3);
	double speed;
	static int wait = 0;

//	double ratio[3] = { 0., 1.135, -1.};
	double ratio[3] = { 0., -1., 1.};
    double offset[3] = { 0., .048, 0.};

	speed = 0;
	for ( i = 0 ; i < 3 ; i++ )
	{
		speed += pow(wheel_vel[i], 2.);
	}
	speed = sqrt(speed);


    if ( phase == 0 )
    {
       force = 0;
       phase = 1;
    }
    else if ( phase == 1 )
    {
       if ( speed > 1.0 )
       {
          phase = 2;
       }
       else
       {
           force += 0.01;
       }
    }
    else if ( phase == 1 )
    {
//        if ( speed > 0.8 )
//        {
//            force -= 0.01;
//        }
    }

    force = 240.;


	for ( i = 0 ; i < 3 ; i++ )
	{
//		command[i] = force * ratio[i] + offset[i];
        command[i] = remote_command[i];
	}

    if ( fabs(wheel_vel[1]) > fabs(wheel_vel[2]) )
    {
        command[2] *= 1.2;    
    }
    else if ( fabs(wheel_vel[1]) < fabs(wheel_vel[2]) )
    {
        command[1] *= 1.2;    
    }

    if ( fabs(wheel_vel[0]) > fabs(wheel_vel[1]) )
    {
        command[1] *= 1.2;    
    }
    else if ( fabs(wheel_vel[0]) < fabs(wheel_vel[1]) )
    {
        command[0] *= 1.2;    
    }

    if ( fabs(wheel_vel[0]) > fabs(wheel_vel[2]) )
    {
        command[2] *= 1.2;    
    }
    else if ( fabs(wheel_vel[0]) < fabs(wheel_vel[2]) )
    {
        command[0] *= 1.2;    
    }

	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

	return command;
}

float wheelTorqueErr_Nm[3] = {0};
float motorTorqueCommand_Nm[3] = {0};
float wheelTorqueErrFiltered_Nm[3] = {0};

jspace::Vector simple_vel_control_loop(void )
{
	int i;
	jspace::Vector command(3);

	double direction = remote_command[0];
	double ratio[3] = {0};

	if ( direction > M_PI / 3. )
	{
		ratio[0] = -1.;
		ratio[1] = 0.;
		ratio[2] = 1.;
	}
	else if ( direction < -M_PI/3. )
	{
		ratio[0] = 1.;
		ratio[1] = -1.;
		ratio[2] = 0.;
	}
	else
	{
		ratio[0] = 0.;
		ratio[1] = 1.;
		ratio[2] = -1.;
	}

    // For each wheel....
	for ( i = 0 ; i < 3 ; i++ )
	{

		//begin new torque controller
		//inputs to controller
		float measuredWheelTorque_Nm = calibrate_torque(i, raw_adc_value[0][i]);
		float desiredWheelTorque_Nm = (float)remote_command[1] * ratio[i];
		float gearboxEfficiency[3] = {0.75, 0.75, 0.75};

		//outputs from controller
		float desiredGearMotorTorque_Nm = 0;

		//internal controller variables
		float torque_to_velocity_ratio = 1.;
		float desiredVelocity_RadPerSec = (float)remote_command[1] * ratio[i];
		float measuredVelocity_RadPerSec = (float)wheel_vel[i];

		desiredGearMotorTorque_Nm = desiredVelocity_RadPerSec / torque_to_velocity_ratio; 
//		desiredGearMotorTorque_Nm *= sin(loop_count/5000.*2*M_PI);
	    if (firstLoop[i]) {
			motorTorqueComand_Last_Nm[i] = desiredGearMotorTorque_Nm;
			firstLoop[i] = false;
		}
		if ( control_mode == MODE_RAW_TORQUE )
			wheelTorqueErr_Nm[i] = measuredVelocity_RadPerSec;
		else
			wheelTorqueErr_Nm[i] = measuredVelocity_RadPerSec / torque_to_velocity_ratio - motorTorqueComand_Last_Nm[i]; // T_{err}

//		lp_object[i]->input(wheelTorqueErr_Nm[i]); // Filtered T_{err}
//		wheelTorqueErrFiltered_Nm[i] = lp_object[i]->output(); // Filtered T_{err}
		wheelTorqueErrFiltered_Nm[i] = wheelTorqueErr_Nm[i];

		motorTorqueCommand_Nm[i] = desiredGearMotorTorque_Nm - wheelTorqueErrFiltered_Nm[i]; // T_{cmd}

		if ( control_mode != MODE_COMPENSATE_TORQUE )
			motorTorqueCommand_Nm[i] = desiredGearMotorTorque_Nm;
//		if ( state_mode[i] == STATE_STOP )
//			motorTorqueCommand_Nm[i] = 3. * ((motorTorqueCommand_Nm[i]>0)?1.0:(motorTorqueCommand_Nm[i]<0)?-1.0:0.0);
		double sign = ((motorTorqueCommand_Nm[i]>0)?1.0:(motorTorqueCommand_Nm[i]<0)?-1.0:0.0);
		motorTorqueCommand_Nm[i] = 3. * sign * exp(-fabs(wheel_vel[i])) + motorTorqueCommand_Nm[i] * (1 - exp(-fabs(wheel_vel[i])));

		//output to motor
		command[i] = torque_command(i, -motorTorqueCommand_Nm[i]); // send T_{cmd} to motor
		//end new torque controller

		motorTorqueComand_Last_Nm[i] = motorTorqueCommand_Nm[i]; // saving T_{cmd} for next round
	}

	return command;
}

#if 0
jspace::Vector control_loop(void )
{
#define Il 50
	int i;
	jspace::Vector command(3);

#if 0
	if (0)
	{
		static int count;
		static double bias = 0;
		double phase = 0;
		double max_phase = 0;
		double sine, cosine;
		static double mag = Il * 0.01;
		static double next_pos_mag = mag;
		static double next_neg_mag = mag;

		phase = count++ * 2 * M_PI / 5000;
		sine = sin(phase);
		cosine = cos(phase);

		if ( fabs(sine) > 0.99 )
		{
			next_pos_mag = Il * 0.01 * exp((double)wheel_vel[0]* -0.1);
			next_neg_mag = Il * 0.01 * exp((double)wheel_vel[0]* 0.1);
		}
		else if ( fabs(sine) < 0.001 )
		{
			if ( cosine > 0. )
			{
				mag = next_pos_mag;
				fprintf(stderr, "Positive mag0: %lf\n", mag);
			}
			else
			{
				mag = next_neg_mag;
				fprintf(stderr, "Negative mag0: %lf\n", mag);
			}
		}

		command[0] = torque_command(0, mag * sine); 
		command[1] = 0.;
		command[2] = 0.;

		return command;
	}
#endif


#if 1
    // For each wheel....
	for ( i = 0 ; i < 3 ; i++ )
	{

		//begin new torque controller
		//inputs to controller
		float measuredWheelTorque_Nm = calibrate_torque(i, raw_adc_value[0][i]);
		float desiredWheelTorque_Nm = (float)remote_command[i];
		float gearboxEfficiency[3] = {0.75, 0.75, 0.75};

		//outputs from controller
		float desiredGearMotorTorque_Nm = 0;

		//internal controller variables
//		float wheelTorqueErr_Nm = 0;
		float P_gain = 2.0;
		float I_gain = 0; // -0.02;
		float D_gain = 0;

#if 1
		wheelTorqueErr_Nm[i] = desiredWheelTorque_Nm - measuredWheelTorque_Nm;

		lp_object[i]->input(wheelTorqueErr_Nm[i]);
		wheelTorqueErrFiltered_Nm[i] = lp_object[i]->output();
		//open loop term
		desiredGearMotorTorque_Nm = desiredWheelTorque_Nm / gearboxEfficiency[i]; 

		//proportional feedback
//		P_term[i] = wheelTorqueErr_Nm[i]*P_gain;  
		P_term[i] = wheelTorqueErrFiltered_Nm[i]*P_gain;  

	  //integrate feedback
		if ( fabs(wheelTorqueErrFiltered_Nm[i]) > 0.1 )
			wheelTorqueErrorSum_Nm[i] += wheelTorqueErrFiltered_Nm[i];
		I_term[i] = wheelTorqueErrorSum_Nm[i] * I_gain;

	  //derivative feedback

//		if ( control_mode == MODE_COMPENSATE_TORQUE )
		{
			desiredGearMotorTorque_Nm += P_term[i];
			desiredGearMotorTorque_Nm += I_term[i];
		}

#if 1
	  if( ++loop_counter >= 1000){
		loop_counter=0;
	    printf("%d: desiredWheelTorque_Nm: %f\tmeasuredWheelTorque_Nm: %f\twheelTorqueErr_Nm: %f\tdesiredMotorTorque_Nm: %f\n", i+1, desiredWheelTorque_Nm, measuredWheelTorque_Nm, wheelTorqueErr_Nm[i], desiredGearMotorTorque_Nm);
	  }
#endif
#else

#if 1
		desiredGearMotorTorque_Nm = desiredWheelTorque_Nm / gearboxEfficiency[i]; // T_{desire} 
		desiredGearMotorTorque_Nm *= sin(loop_count/5000.*2*M_PI);
	    if (firstLoop[i]) {
			motorTorqueComand_Last_Nm[i] = desiredGearMotorTorque_Nm;
			firstLoop[i] = false;
		}
		if ( control_mode == MODE_RAW_TORQUE )
			wheelTorqueErr_Nm[i] = measuredWheelTorque_Nm;
		else
			wheelTorqueErr_Nm[i] = measuredWheelTorque_Nm - motorTorqueComand_Last_Nm[i]; // T_{err}

		lp_object[i]->input(wheelTorqueErr_Nm[i]); // Filtered T_{err}
		wheelTorqueErrFiltered_Nm[i] = lp_object[i]->output(); // Filtered T_{err}

		motorTorqueCommand_Nm[i] = (desiredGearMotorTorque_Nm - wheelTorqueErrFiltered_Nm[i]); // T_{cmd}

		if ( control_mode != MODE_COMPENSATE_TORQUE )
			motorTorqueCommand_Nm[i] = desiredGearMotorTorque_Nm;

		//open loop term

		//proportional feedback
//		P_term[i] = wheelTorqueErr_Nm[i]*P_gain;  
		//P_term[i] = wheelTorqueErrFiltered_Nm[i]*P_gain;  

	  //derivative feedback

		//if ( control_mode == MODE_COMPENSATE_TORQUE )
		//{
		//	desiredGearMotorTorque_Nm += P_term[i];
		//}
#else
		float torque_to_velocity_ratio = 1.;
		float desiredVelocity_RadPerSec = (float)remote_command[i];
		float measuredVelocity_RadPerSec = (float)wheel_vel[i];

		desiredGearMotorTorque_Nm = desiredVelocity_RadPerSec / torque_to_velocity_ratio; 
//		desiredGearMotorTorque_Nm *= sin(loop_count/5000.*2*M_PI);
	    if (firstLoop[i]) {
			motorTorqueComand_Last_Nm[i] = desiredGearMotorTorque_Nm;
			firstLoop[i] = false;
		}
		if ( control_mode == MODE_RAW_TORQUE )
			wheelTorqueErr_Nm[i] = measuredVelocity_RadPerSec;
		else
			wheelTorqueErr_Nm[i] = measuredVelocity_RadPerSec / torque_to_velocity_ratio - motorTorqueComand_Last_Nm[i]; // T_{err}

		lp_object[i]->input(wheelTorqueErr_Nm[i]); // Filtered T_{err}
		wheelTorqueErrFiltered_Nm[i] = lp_object[i]->output(); // Filtered T_{err}
		wheelTorqueErrFiltered_Nm[i] = wheelTorqueErr_Nm[i];

		motorTorqueCommand_Nm[i] = desiredGearMotorTorque_Nm - wheelTorqueErrFiltered_Nm[i]; // T_{cmd}

		if ( control_mode != MODE_COMPENSATE_TORQUE )
			motorTorqueCommand_Nm[i] = desiredGearMotorTorque_Nm;

		//open loop term
#endif

#endif
        motorTorqueCommand_Nm[i] = desiredGearMotorTorque_Nm;

		// add D_term
		//D_term[i] = -1. * D_gain * (desiredGearMotorTorque_Nm - motorTorqueComand_Last_Nm[i]);

		//if ( control_mode == MODE_COMPENSATE_TORQUE )
			//desiredGearMotorTorque_Nm += D_term[i];


		//output to motor
		command[i] = torque_command(i, -motorTorqueCommand_Nm[i]); // send T_{cmd} to motor
		//end new torque controller

		motorTorqueComand_Last_Nm[i] = motorTorqueCommand_Nm[i]; // saving T_{cmd} for next round
	}
#endif
	
#if 1
	command[0] = remote_command[0];
	command[1] = remote_command[1];
	command[2] = remote_command[2];
#else
	command[0] = 0.;
	command[1] = remote_command[0] * 50.;
	command[2] = -1. * remote_command[0] * 50.;

	double gain = 25;
//	command[0] += remote_command[1] * gain;
//	command[1] += remote_command[1] * gain;
//	command[2] += remote_command[1] * gain;
#endif
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

	return command;
}
#endif

jspace::Vector test02_loop(void )
{
    static double direction = 1.;
    jspace::Vector command(3);
    double offset[3] = { 8, 19, 2 };
    double compensation;
    double mag;

    if ( wheel_pos[1] >= 10. && direction == 1. )
        direction = -1;
    else if ( wheel_pos[1] <= -10. && direction == -1 )
    {
        direction = 1.;
    }
//    phase = 2*M_PI*.4;
    if ( remote_command[0] > 0 )
        mag = remote_command[0];
    else
        mag = 0.;

    if ( remote_command[1] >= 0. )
        phase = remote_command[1];
    else
        phase += 0.000001;

    if ( remote_command[2] > 0. )
        freq = remote_command[2];
    else
        freq = 43.;

#if 0
    command[1] = 40 * direction - mag * cos(freq*(wheel_pos[1]+1.95) + phase);
    command[0] = 0;
    command[2] = 0;
#else
    command[0] = remote_command[1];
    command[1] = remote_command[2];
    command[2] = 0;
#endif

	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

	return command;
}

jspace::Vector test03_loop(void )
{
    static double direction = 1.;
    static double prev_desired_vel = 0;
	double desired_vel = remote_command[0];
	double err = wheel_vel[0] - desired_vel;
    jspace::Vector command(3);
	double kp =remote_command[3];
    double ki = remote_command[4];
    static double sum = 0.;

    if ( prev_desired_vel != desired_vel )
    {
        sum = 0.;        
    }

    sum += err;

    command[0] = -kp * err - ki * sum;
    command[1] = 0;
    command[2] = 0;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

    prev_desired_vel = desired_vel;

	return command;
}

#define ACC     (.05) // rad/s^2
#define DEACC     (.07) // rad/s^2
#define W_MAX (0.1) // rad/s
double cur_vel[3];
double err_pos;
double nxt_vel, nxt_err;
int getSign(double a, double pos, double vel)
{
    int sign; 

    if ( pos == 0. )
    {
        sign = 0;
    }
    else if ( pos > a*vel*vel )
    {
        sign = -1;
    }
    else if ( pos < -a*vel*vel )
    {
        sign = 1;
    }
    else if ( vel > 0 )
    {
        sign = -1;
    }
    else
    {
        sign = 1;
    }
    
    return sign;
}
double getNxtStep(double a, double pos, double vel, double def)
{
    int sign = getSign(a, pos, vel);
    double acc = .5/a;
    double next_vel = vel + sign*acc*def;
    double c;

    if ( sign < 0 )
    {
        c = -sqrt((pos+a*vel*vel)/(2*a));
    }
    else
    {
        c = sqrt((pos+a*vel*vel)/(2*a));
    }

    if ( ((c - next_vel) * (c - vel)) < 0. )
    {
        def = (c - vel) / (sign*acc);
    }
    return def;
}
jspace::Vector test04_loop(void )
{
    static double direction = 1.;
    double desired_pos = remote_command[0];
    double cur_pos = wheel_pos[0];
    cur_vel[0] = wheel_vel[0];
	err_pos = cur_pos - desired_pos;

    jspace::Vector command(3);
    double offset = remote_command[1];
    double mass = remote_command[2];
	double kp =remote_command[3];
	double kv =remote_command[4];
    double acc;
    double old_acc;
    double nxt_step = 3.0;
    double alpha, beta;
    double a, b, c;
    static bool initialized = false;
    double abs_err = fabs(err_pos);

    if ( !initialized )
    {
        #define CUTOFF_FREQ 100
        #define FCONTROL_DOB_WC (CUTOFF_FREQ * 2 * 3.14)  // in Hz
        lp = (filter  *) new digital_lp_filter( FCONTROL_DOB_WC, 1./SERVO_RATE);
        lp->clear();
        initialized = true; 
    }

    old_acc = acc;
    acc = ACC * getSign(.5/ACC, err_pos, cur_vel[0]);

//    if ( acc < 0)
 //       fc *= -1.;
    nxt_step = getNxtStep(.5/ACC, err_pos, cur_vel[0], 3.);
    nxt_vel = cur_vel[0] + (acc+old_acc) * nxt_step / 2.;
    if ( nxt_vel >= W_MAX )
    {
        if ( acc > 0.)
            acc = 0.;
        nxt_vel = W_MAX; // + acc * nxt_step;
    }
    if ( nxt_vel <= -W_MAX )
    {
        if ( acc < 0.)
            acc = 0.;
        nxt_vel = -W_MAX; // + acc * nxt_step;
    }
//    lp->input( nxt_vel * nxt_step );
//    nxt_err = lp->output();
    nxt_err = (nxt_vel+cur_vel[0]) * nxt_step / 2.;

//    if ( acc*nxt_err > 0 )
//       nxt_err = 0;

    command[2] = mass * acc * (1-exp(-abs_err)) - kp * ( err_pos ) * exp(-abs_err) - kv * ( cur_vel[0] - nxt_vel ) + offset;
    command[0] = 0;
    command[1] = 0;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

	return command;
}

extern float chirpSignal;
jspace::Vector test05_loop(void )
{

    jspace::Vector command(3);
    unsigned char end = 0;
    float elapsedTime = (sTimeStamp-sBaseTimeStamp)/1000000.;
    float chirpSignal = getChirpSignal(elapsedTime, 1./SERVO_RATE, 53., 0., &end);

    command[1] = chirpSignal;
    command[0] = 0;
    command[2] = 0;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

    if ( end == 1)
    {
        fprintf(stderr, "end\n");
        exit(0);
    }
	return command;
}

filter *ff_object;
filter *inv_p;
void test06_init(void)
{
    ff_object = (filter  *) new ff01_filter( 1./SERVO_RATE, 1000);
    inv_p = (filter  *) new ff01_filter( 1./SERVO_RATE, 100);

    ff_object->clear();
}

jspace::Vector test06_loop(void )
{
#define GR 43

    jspace::Vector command(3);
    unsigned char end = 0;
    double ff_term;
    double disturbance;
    double cmd;
    double prev_cmd[3] = {0};
    double b;

#if 0
    inv_p->input(wheel_vel[1]*sin(sTimeStamp*1000));
    ff_object->input(remote_command[0]);

    disturbance = inv_p->output() - prev_cmd[1];
    cmd = ff_object->output() - disturbance;
#else
    err_posv[1] = remote_command[0]*sin((double)sTimeStamp/1000000.*remote_command[1]);
    ff_object->input(err_posv[1]*GR);
    cmd = ff_object->output();
#endif

    if ( wheel_vel[1] > 0.1 )
        b = 34.21;
    else if ( wheel_vel[1] < -0.1 )
        b = -34.21;
    else if ( cmd > 0 )
        b = 34.21;
    else if ( cmd < 0 )
        b = -34.21;

    command[1] = cmd*1000; // + b;
    command[0] = 0;
    command[2] = 0;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

    prev_cmd[1] = cmd;

	return command;
}

double cur_posv[3];
double phasev[3];
double acc_termv[3];
double pos_termv[3];
double vel_termv[3];
int sign[3] = {0};
double cross_point[3];
double desired_pos = 0;
jspace::Vector test07_loop(void )
{
    static double direction = 1.;
    double acc;
    static int old_sign = 0;
    
    cur_posv[1] = wheel_pos[1];
    cur_vel[1] = wheel_vel[1];


    // New Command arrived 
    if ( desired_pos != remote_command[0] )
    {
        desired_pos = remote_command[0];
	    err_posv[1] = cur_posv[1] - desired_pos;
        sign[1] = getSign(.5/ACC, err_posv[1], cur_vel[1]);
        phasev[1] = 0.;
        old_sign = 0.;

        cross_point[1] = err_posv[1] - .5/ACC*sign[1]*cur_vel[1]*cur_vel[1];
    }
	err_posv[1] = cur_posv[1] - desired_pos;

    jspace::Vector command(3);
    double mass = remote_command[2];
	double kp =remote_command[3];
	double kv =remote_command[4];
    double nxt_step = 3.0;
    double alpha, beta;
    double a, b, c;
    double abs_err = fabs(err_posv[1]);
    double speed;
    double cmd;

    if ( phasev[1] == 0. )
    {
        sign[1] = getSign(.5/ACC, err_posv[1], cur_vel[1]);
        if ( old_sign != 0 && sign[1] != old_sign )
        {
            phasev[1] = 1;
            cross_point[1] = 0.;
        }
        if ( cur_vel[1] > 0 )
            nxt_vel = sqrt(fabs(err_posv[1]-cross_point[1])*2*ACC) ;
        else if ( cur_vel[1] < 0 )
            nxt_vel = -sqrt(fabs(err_posv[1]-cross_point[1])*2*ACC) ;


    }
    else if ( phasev[1] == 1. )
    {
        nxt_vel = - sign[1] * sqrt(2*ACC*abs_err);
    }
    nxt_err = nxt_vel * nxt_vel * sign[1] * 0.5 / ACC + cross_point[1];
    acc = ACC * sign[1];

    if ( nxt_vel >= W_MAX )
    {
        acc = -ACC;
        nxt_vel = W_MAX; // + acc * nxt_step;
        kp = 0.;
    }
    else if ( nxt_vel <= -W_MAX )
    {
        acc = ACC;
        nxt_vel = -W_MAX; // + acc * nxt_step;
        kp = 0.;
    }

//    if ( acc*nxt_err > 0 )
//       nxt_err = 0;

    acc_termv[1] = mass * acc * (1-exp(-0.3*abs_err));
    pos_termv[1] = kp * ( err_posv[1] - nxt_err );
    vel_termv[1] = kv * ( cur_vel[1] - nxt_vel );// * (1-exp(-0.1*fabs(cur_vel - nxt_vel)));
    cmd = acc_termv[1] - pos_termv[1] - vel_termv[1];
    command[1] = cmd + compensate_friction(cmd, cur_vel[1], nxt_vel, abs_err, phasev[1]);
    command[0] = 0;
    command[2] = 0;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

    old_sign = sign[1];

	return command;
}

#define WIN 200
double threshold = 0;
int pos_buf[WIN] = {0};
int pos_idx = 0;
int prev_sign = 0;
double elapsed_time;
int cross_count = 0;
double time_gap = 0;
jspace::Vector test08_loop(void )
{
    int jj;
    bool on = (remote_command[2] > 0.0);
    jspace::Vector command(3);
    double cmd = remote_command[0] * cos(2*M_PI*remote_command[1]*(sTimeStamp/1000000.));
    static double max_vel = 0, min_vel = 0;
    int sign; 
    static long long baseTime;
    int *buf;

    if ( on )
    {
        sign = ( wheel_vel[1] > threshold )? 1:-1;
        pos_buf[pos_idx] = sign;
#if 0
        for ( jj = 1 ; jj < WIN ; jj++ )
        {
            if ( sign != (pos_buf[(pos_idx + jj)%WIN]) )
            {
                sign = 0;
                break;
            }
        }
#else
        buf = pos_buf;
        for ( jj = 1 ; jj < WIN ; jj++ )
        {
            if ( *buf * sign != 1 )
            {
                sign = 0;
                break;
            }
            buf++;
        }
#endif
        pos_idx = (pos_idx + WIN - 1) % WIN;

        elapsed_time = ((double)wheel_pos[1] - (double)baseTime);
        if ( sign == 1 &&  prev_sign != sign )
        {
            static double last_time = 0.;

            time_gap = elapsed_time - last_time;    
            cross_count++;
            last_time = elapsed_time;
            time_gap / ( 2*M_PI);
        }

        max_vel = -100;
        min_vel = 100.;
    }
    else
    {
        if ( wheel_vel[1] > max_vel )
            max_vel = wheel_vel[1];
        if ( wheel_vel[1] < min_vel )
            min_vel = wheel_vel[1];
        threshold = (max_vel + min_vel)/2;
        sign = 0;
        baseTime = wheel_pos[1];
        cross_count = 0;
    }

    command[1] = cmd;
    command[0] = 0;
    command[2] = 0;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

    prev_sign = sign;

	return command;
}

double a[3] = {0};
double error[3] = {0};
double f = 0;
int a_count = 0;
static double pauseTime;
jspace::Vector test09_loop(void )
{
    static int count = 0;
    static double xsum = 0.;
    static double ysum = 0.;
    static double xysum = 0.;
    static double xxsum = 0.;
    static double cmd;
    double sec = sTimeStamp/1000000.;
    double vel = wheel_vel[1];
    jspace::Vector command(3);
    
    if ( phase == 0 && cmd == 0. )
        cmd = 40. + (((double)random())/RAND_MAX)* 60; //remote_command[0]; // * cos(2*M_PI*remote_command[1]*(sTimeStamp/1000000.));
    else if ( phase == 1 )
            cmd = -20.;
    else if ( phase == 2 )
            cmd = 0.;
    else if ( phase == 3 )
        cmd = 0.;

    xsum += sec;
    ysum += vel;
    xxsum += sec*sec;
    xysum += sec*vel;
    command[1] = cmd;
    command[0] = 0;
    command[2] = 0;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];
    count++;

    if ( phase == 0 && abs(wheel_vel[1]) >= 8. )
    {
        phase = 1;
        a[1] = (-xsum*ysum + count*xysum) / (count*xxsum - xsum*xsum);
        f = cmd;
        a_count++;
    }
    if ( phase == 1 && abs(wheel_vel[1]) <= 4.5 )
    {
        phase = 2;
        pauseTime = sTimeStamp;
    }
    if ( phase == 2 && abs(wheel_vel[1]) <= 0.00001 )
    {
        phase = 3;
    }
    if ( phase == 3 && pauseTime+3000000. < sTimeStamp)
    {
        xsum = ysum = xysum = xxsum = 0.;
        count = 0;
        phase = 0; 
    }

	return command;
}

jspace::Vector test10_loop(void )
{
    jspace::Vector command(3);
    int ii;
    double acc;
    static double desired_jpos[3];
    double abs_err[3];
    static int old_sign[3] = {0};

    for ( ii = 0 ; ii < 3 ; ii++ )
    {
        cur_vel[ii] = wheel_vel[ii];
        cur_posv[ii] = wheel_pos[ii];
    }


    // New Command arrived 
    if ( desired_pos != remote_command[0] )
    {
        desired_pos = remote_command[0];
        desired_jpos[2] = cur_posv[2];                           
        desired_jpos[0] = cur_posv[0] + desired_pos;
        desired_jpos[1] = cur_posv[1] - desired_pos;
        for ( ii = 0 ; ii < 3 ; ii++ )
        {
	        err_posv[ii] = cur_posv[ii] - desired_jpos[ii];
            sign[ii] = getSign(.5/ACC, err_posv[ii], cur_vel[ii]);
            phasev[ii] = 0.;
            old_sign[ii] = 0.;
            cross_point[ii] = err_posv[ii] - .5/ACC*sign[ii]*cur_vel[ii]*cur_vel[ii];
        }

    }

    for ( ii = 0 ; ii < 3 ; ii++ )
    {
        err_posv[ii] = cur_posv[ii] - desired_jpos[ii];
        abs_err[ii] = fabs(err_posv[ii]);
    }

    double cmd;

    for ( ii = 0 ; ii < 3 ; ii++ )
    {
        double mass = remote_command[2];
        double kp =remote_command[3];
        double kv =remote_command[4];

        if ( phasev[ii] == 0. )
        {
            sign[ii] = getSign(.5/ACC, err_posv[ii], cur_vel[ii]);
            if ( cur_vel[ii] * sign[ii] >= 0. )
            {
                phasev[ii] = 0.5;
            }
            if ( abs_err[ii] < fabs(cross_point[ii]) )
            {
                nxt_vel = -sign[ii] * sqrt(fabs(err_posv[ii]-cross_point[ii])*2*ACC) ;
                nxt_err = nxt_vel * nxt_vel * sign[ii] * 0.5 / ACC + cross_point[ii];
            }
            else
            {
                nxt_vel = 0;
                nxt_err = cross_point[ii];
            }
            kp = 0.;
            acc = ACC * sign[ii];
        }
        if ( phasev[ii] == 0.5 )
        {
            bool change = true;
            for ( int jj = 0 ; jj < 3 ; jj++ )
                if ( phasev[jj] < 0.5 )
                    change = false;

            if ( change )
                phasev[ii] = 1.;
            nxt_vel = 0;
            nxt_err = 0;
            acc = 0;
            kp = 0.;
            kv = 0.;
        }
        if ( phasev[ii] == 1. )
        {
            sign[ii] = getSign(.5/ACC, err_posv[ii], cur_vel[ii]);
            if ( old_sign[ii] != 0 && sign[ii] != old_sign[ii] )
            {
                phasev[ii] = 2;
                cross_point[ii] = 0.;
            }
            nxt_vel = sign[ii] * sqrt(fabs(err_posv[ii]-cross_point[ii])*2*ACC) ;
            nxt_err = nxt_vel * nxt_vel * sign[ii] * 0.5 / ACC + cross_point[ii];
            acc = ACC * sign[ii];
        }
        else if ( phasev[ii] == 2. )
        {
            nxt_vel = - sign[ii] * sqrt(2*ACC*abs_err[ii]);
            nxt_err = nxt_vel * nxt_vel * sign[ii] * 0.5 / ACC;

            if ( abs_err[ii] < 0.5 )
                phasev[ii] = 3.;
            acc = ACC * sign[ii];
        }
        else if ( phasev[ii] == 3. )
        {
            nxt_vel = 0.;
            nxt_err = 0.;
            acc = 0.;
        }


        if ( nxt_vel >= W_MAX )
        {
            acc = -ACC;
            nxt_vel = W_MAX; // + acc * nxt_step;
            kp = 0.;
        }
        else if ( nxt_vel <= -W_MAX )
        {
            acc = ACC;
            nxt_vel = -W_MAX; // + acc * nxt_step;
            kp = 0.;
        }

        acc_termv[ii] = mass * acc * (1-exp(-0.3*abs_err[ii]));
        pos_termv[ii] = kp * ( err_posv[ii] - nxt_err );
//        if ( phasev[ii] != 3 )
            vel_termv[ii] = kv * ( cur_vel[ii] - nxt_vel ) * (1-exp(-0.3*abs_err[ii]));
 //       else
  //          vel_termv[ii] = 0.;
        cmd = acc_termv[ii] - pos_termv[ii] - vel_termv[ii];
        if ( phasev[ii] < 3 )
            command[ii] = cmd + compensate_friction(cmd, cur_vel[ii], nxt_vel, abs_err[ii], phasev[ii]);
        else
            command[ii] = cmd;
	    sCommand[ii] = command[ii];
        old_sign[ii] = sign[ii];
    }

    command[2] = 0.;
	return command;
}

jspace::Vector test11_loop(void )
{
    jspace::Vector command(3);
    double reverse = remote_command[4];

    int sign = (remote_command[0] > 0)?-1:1;
    if ( fabs(wheel_vel[0]) < remote_command[3] )
        command[0] = remote_command[0];
    else
        command[0] = remote_command[0] + sign * reverse;

    sign = (remote_command[1] > 0)?-1:1;
    if ( fabs(wheel_vel[1]) < remote_command[3] )
        command[1] = remote_command[1];
    else
        command[1] = remote_command[1] + sign * reverse;

    sign = (remote_command[2] > 0)?-1:1;
    if ( fabs(wheel_vel[2]) < remote_command[3] )
        command[2] = remote_command[2];
    else
        command[2] = remote_command[2] + sign * reverse;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

	return command;
}

jspace::Vector test12_loop(void )
{
    int i;
    static int count = 0;
    static double xsum = 0.;
    static double xxsum = 0.;
    static double ysum[3] = {0.};
    static double xysum[3] = {0.};
    static double yysum[3] = {0.};
    static double cmd;
    double sec = sTimeStamp/1000000.;
    double vel[3];
    jspace::Vector command(3);
    static int sign = 1;
    
    if ( phase == 0 && cmd == 0. )
        cmd = 400.; // + (((double)random())/RAND_MAX)* 60; //remote_command[0]; // * cos(2*M_PI*remote_command[1]*(sTimeStamp/1000000.));
    else if ( phase == 1 )
        cmd = -20.;
    else if ( phase == 2 )
        cmd = 100.;
    else if ( phase == 3 )
        cmd = 0.;

    xsum += sec;
    xxsum += sec*sec;
    for ( i = 0 ; i < 3 ; i++ )
    {
        vel[i] = wheel_vel[i];
        ysum[i] += vel[i];
        xysum[i] += sec*vel[i];
        yysum[i] += vel[i]*vel[i];
    }
    command[0] = cmd * sign;
    command[1] = -cmd * sign;
    command[2] = 0;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];
    count++;

    if ( phase == 0 && abs(wheel_vel[1]) >= 8. )
    {
        phase = 1;

    }
    if ( phase == 1 && abs(wheel_vel[1]) <= 6. )
    {
        phase = 2;
        pauseTime = sTimeStamp;

        xsum = ysum[0] = ysum[1] = xysum[0] = xysum[1] = yysum[0] = yysum[1] = xxsum = count = 0.;
    }
    if ( phase == 2 && abs(wheel_vel[1]) <= 1.0 )
    {
        phase = 3;

        for ( i = 0 ; i < 3 ; i++ )
        {
            double b;
            a[i] = (-xsum*ysum[i] + count*xysum[i]) / (count*xxsum - xsum*xsum);
            b = 1./count*(ysum[i] - a[i]*xsum);
            f = cmd;
            error[i] =  a[i]*a[i]*xxsum + yysum[i]*yysum[i] + 2*a[i]*b*xsum - 2*b*ysum[i] - 2*a[i]*xysum[i];
        }
        a_count++;
    }
    if ( phase == 3 && pauseTime+3000000. < sTimeStamp)
    {
        phase = 0; 
        sign *= -1;
    }

	return command;
}


//filter *ff_object;
//filter *inv_p;
void test13_init(void)
{
    ff_object = (filter  *) new ff01_filter( 1./SERVO_RATE, 100);
    inv_p = (filter  *) new ff01_filter( 1./SERVO_RATE, 100);

    ff_object->clear();
}

jspace::Vector test13_loop(void )
{
#define GR 43

    jspace::Vector command(3);
    unsigned char end = 0;
    double ff_term;
    double disturbance;
    double cmd;
    double prev_cmd[3] = {0};
    double b;
    double kp = remote_command[2];
    double input;
    double err;
    double output = wheel_pos[1];

#if 0
    inv_p->input(wheel_vel[1]*sin(sTimeStamp*1000));
    ff_object->input(remote_command[0]);

    disturbance = inv_p->output() - prev_cmd[1];
    cmd = ff_object->output() - disturbance;
#else
    input = remote_command[0]*sin((double)sTimeStamp/1000000.*remote_command[1]);
    err = input - output;
    ff_object->input(err_posv[1]*GR);
    cmd = ff_object->output() + kp * err;

#endif

    if ( wheel_vel[1] > 0.1 )
        b = 34.21;
    else if ( wheel_vel[1] < -0.1 )
        b = -34.21;
    else if ( cmd > 0 )
        b = 34.21;
    else if ( cmd < 0 )
        b = -34.21;

    command[1] = cmd*1000; // + b;
    command[0] = 0;
    command[2] = 0;
    err_posv[1] = input;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

    prev_cmd[1] = cmd;

	return command;
}

double center[2] = {1., 0.};
#define nSector  30

bool update_trj_cmd(double *trj_cmd) 
{
    int region;
    static int last_region;
    static double next_theta = -0.01; //2*M_PI/nSector;
    static double last_theta = 0;
    bool change = false;
    double pos[2];
    double last_pos[2];
    double vel[2];
    double theta;
    double dest[2];
    double dest_vector[2];

    pos[0] = xfv(0);
    pos[1] = xfv(1);
    vel[0] = xdotfv(0);
    vel[1] = xdotfv(1);

    theta = atan2(pos[1]-center[1],pos[0]-center[0]);
    region = ((int)(theta*180/M_PI+3600)%360)*nSector/360;
    if ( region != last_region )
    {
        dest[0] = center[0]+cos(theta+2*M_PI/nSector);
        dest[1] = center[1]+sin(theta+2*M_PI/nSector);

        dest_vector[0] = dest[0] - pos[0];
        dest_vector[1] = dest[1] - pos[1];

        trj_cmd[0] = pos[0] + 1 * dest_vector[0];
        trj_cmd[1] = pos[1] + 1 * dest_vector[1];
        trj_cmd[2] = 0.;

        change = true;
        next_theta += 2*M_PI/nSector;
#if 0
        fprintf(stdout, "=========================\n");
        fprintf(stdout, "  Change Dest            \n");
        fprintf(stdout, " %d -> %d\n", last_region, region);
        fprintf(stdout, " %f -> %f\n", theta, next_theta);
        fprintf(stdout, " %f , %f\n", trj_cmd[0], trj_cmd[1]);
        fprintf(stdout, "=========================\n");
#else
        fprintf(stdout, "%f %f %f %f %f %f\n", pos[0], pos[1], dest[0], dest[1], trj_cmd[0], trj_cmd[1]);
#endif
    }
    last_region = region;

    return change;
}

void print_mat(MatrixXd &mat)
{
	int ii, jj;
	if ( mat.cols() == 1 )
	{
		for ( ii = 0 ; ii < mat.rows() ; ii++ )
			fprintf(stderr, "%+8f ", mat(ii));
		fprintf(stderr, "\n");
	}
	else	
	{
		for ( ii = 0 ; ii < mat.rows() ; ii++ )
		{
			for ( jj = 0 ; jj < mat.cols() ; jj++ )
				fprintf(stderr, "%+8f ", mat(ii, jj));
			fprintf(stderr, "\n");
		}
	}
}

void pseudoInverse(MatrixXd const & matrix,
         double sigmaThreshold,
         MatrixXd & invMatrix,
         VectorXd * opt_sigmaOut)
{
    if ((1 == matrix.rows()) && (1 == matrix.cols())) {
      // workaround for Eigen2
      invMatrix.resize(1, 1);
      if (matrix.coeff(0, 0) > sigmaThreshold) {
    invMatrix.coeffRef(0, 0) = 1.0 / matrix.coeff(0, 0);
      }
      else {
    invMatrix.coeffRef(0, 0) = 0.0;
      }
      if (opt_sigmaOut) {
    opt_sigmaOut->resize(1);
    opt_sigmaOut->coeffRef(0) = matrix.coeff(0, 0);
      }
      return;
    }

    Eigen::SVD<MatrixXd> svd(matrix);
    // not sure if we need to svd.sort()... probably not
    int const nrows(svd.singularValues().rows());
    MatrixXd invS;
    invS = MatrixXd::Zero(nrows, nrows);
    for (int ii(0); ii < nrows; ++ii) {
      if (svd.singularValues().coeff(ii) > sigmaThreshold) {
    invS.coeffRef(ii, ii) = 1.0 / svd.singularValues().coeff(ii);
      }
    }
    invMatrix = svd.matrixV() * invS * svd.matrixU().transpose();
    if (opt_sigmaOut) {
      *opt_sigmaOut = svd.singularValues();
    }
}

#define tanpsi  1.1
#define ysec    0.42
#define REGION_FREE  0
#define REGION_CONTACT  1
int gRegion = REGION_FREE;
double gSlope = 1.;
double gXSlope = 1.;
double gYSlope = 1.;
void updateRegion(double x, double y)
{
    double ref;

    ref = tanpsi*x + ysec;

    if ( y > ref )
        gRegion =  REGION_CONTACT;
    else
        gRegion =  REGION_FREE;
}

bool sInitialized = false;

#define NUM_TRJ 100
#define CONTACT_THRESHOLD 0.12
double trj[NUM_TRJ][2];
int trjIdx;
int noContactIdx;
void updateContact(double *pXdes, const MatrixXd &xv)
{
    double ref;
    MatrixXd xdes = MatrixXd::Zero(3,1);
    MatrixXd xerr;
    double err;

   xdes[0] = pXdes[0];
   xdes[1] = pXdes[1];
   xdes[2] = pXdes[2];
    
   xerr = xdes - xv;

    if ( sInitialized == false)
    {
        sInitialized = true;
        trjIdx = 0;
    }
    err = xerr.norm();

    trj[(trjIdx++)%NUM_TRJ][0] = xv[0];
    trj[(trjIdx++)%NUM_TRJ][1] = xv[1];

    if ( err > CONTACT_THRESHOLD && trjIdx > NUM_TRJ )
    {
        gRegion =  REGION_CONTACT;
    }
    else
    {
        gRegion =  REGION_FREE;
    }

#if 1
    int i;
    double x, y, xx, yy, xy, sumx,sumy, sumxx, sumxy, sumyy;
    sumx = sumy = sumxx = sumyy = sumxy = 0.;

    for ( i = 0 ; i < NUM_TRJ ; i++ )
    {
        if ( trjIdx <= NUM_TRJ )
            break;

        x = trj[(trjIdx-i)%NUM_TRJ][0];     
        y = trj[(trjIdx-i)%NUM_TRJ][1];     
        xx = x*x;
        yy = y*y;
        xy = x*y;

        sumx += x;
        sumy += y;
        sumxx += xx;
        sumyy += yy;
        sumxy += xy;
    }
    if ( fabs(NUM_TRJ*sumxx - sumx*sumx) < 0.01 ) 
    {
        gXSlope = (NUM_TRJ * sumxy - sumx*sumy) / (NUM_TRJ*sumxx - sumx*sumx); 
        gSlope = gXSlope;
    }
    else
        gXSlope = gSlope = 1000000.;

    if ( fabs(NUM_TRJ*sumxx - sumx*sumx) < 0.01 ) 
    {
        gYSlope = (NUM_TRJ * sumxy - sumx*sumy) / (NUM_TRJ*sumyy - sumy*sumy); 
    }
    else
        gYSlope = 1000000.;
#endif
}

jspace::Vector test14_loop(void )
{
    jspace::Vector command(3);

	static double timestamp = 0.;
    static int count = 0;
    MatrixXd A, Ainv, G, Jc, JcBar, Jstar, Ld, Ldinv, U, Nc, Jts, Ldts, Ldtsinv, J, UNc, UNcBar, I, Phi, Phiinv;
	MatrixXd tau;
    double theta = xfv(2); //wheel_ori; 
    
#if 0
    double trj_cmd[3] = {0};
    if ( update_trj_cmd(trj_cmd) )
        test15_set_command(trj_cmd);
#endif

#if 0
    aRef = MatrixXd::Zero(3,1);
//    aRef(0) = remote_command[0];
#else
//    aRef = test16_loop();
    aRef = test22_loop();
#endif


    A = MatrixXd::Zero(6,6);
    A(0,0) = M + 3*m;
    A(1,1) = M + 3*m;
    A(2,2) = IM + 3*Imz;
    A(3,3) = Imx;
    A(4,4) = Imx;
    A(5,5) = Imx;

#if 1
    if ( gRegion == REGION_CONTACT )
    {   
        Jc = MatrixXd::Zero(4,6);
#if 0
        if ( abs(gXSlope) < abs(gYSlope) )
        {
            Jc(3,0) = gXSlope; //0.6667;
            Jc(3,1) = -1;
        }
        else
        {
            Jc(3,0) = -1; //0.6667;
            Jc(3,1) = gYSlope;
        }
#else
            Jc(3,0) = 1;
            Jc(3,1) = -1;
#endif
//        Jc(4,2) = 1;
    }
    else
#endif
    {
        Jc = MatrixXd::Zero(3,6);
    }
    Jc(0,0) = -sin(theta);
    Jc(0,1) = cos(theta);
    Jc(0,2) = R;
    Jc(0,3) = -r;
    Jc(0,4) = 0;
    Jc(0,5) = 0;
    Jc(1,0) = -sin(theta-2*M_PI/3);
    Jc(1,1) = cos(theta-2*M_PI/3);
    Jc(1,2) = R;
    Jc(1,3) = 0;
    Jc(1,4) = -r;
    Jc(1,5) = 0;
    Jc(2,0) = -sin(theta+2*M_PI/3);
    Jc(2,1) = cos(theta+2*M_PI/3);
    Jc(2,2) = R;
    Jc(2,3) = 0;
    Jc(2,4) = 0;
    Jc(2,5) = -r;

    J = MatrixXd::Zero(3,6);
    J(0,0) = 1;
    J(1,1) = 1;
    J(2,2) = 1;

    U = MatrixXd::Zero(3,6);
    U(0,3) = 1;
    U(1,4) = 1;
    U(2,5) = 1;

    G = MatrixXd::Zero(6,1);
//    G(0) = 4.0*(M+3*m)*cos(phi)*cos(psi)/ 43;
//    G(1) = 4.0*(M+3*m)*cos(phi)*sin(psi)/ 43;

    I = MatrixXd::Identity(6,6);

    Ainv = A.inverse();
    Ldinv  = Jc*Ainv*(Jc.transpose());
//    Ld  = (Jc*Ainv*(Jc.transpose())).inverse();
    pseudoInverse(Ldinv, 0.0001, Ld, 0);
    JcBar = Ainv*(Jc.transpose())*Ld;
    Nc = I - JcBar *Jc;
    Jts = J * Nc; 
    Ldtsinv = Jts * Ainv * (Jts.transpose());
//    Ldts = (Jts * Ainv * (Jts.transpose())).inverse();
    pseudoInverse(Ldtsinv, 0.0001, Ldts, 0);
    UNc = U * Nc;
    Phi = UNc * Ainv * (UNc.transpose());
//    Phiinv = Phi.inverse();
    pseudoInverse(Phi, 0.0001, Phiinv, 0);
    UNcBar = Ainv * (UNc.transpose()) * Phiinv;

    Jstar = J * UNcBar;

    tau = (Jstar.transpose()) * Ldts * (aRef + Jts * Ainv * Nc.transpose() * G);

#if 0
    if ((count++ % 100) == 0 )
    {
    
        fprintf(stderr, "aRef  : \n"); print_mat(aRef);
        fprintf(stderr, "tau   : \n"); print_mat(tau);
        fprintf(stderr, "jstar : \n"); print_mat(Jstar);
        fprintf(stderr, "A     : \n"); print_mat(A);
        fprintf(stderr, "Jc    : \n"); print_mat(Jc);
        fprintf(stderr, "UNc   : \n"); print_mat(UNc);
        fprintf(stderr, "Nc    : \n"); print_mat(Nc);
        fprintf(stderr, "UNcBar: \n"); print_mat(UNcBar);
        fprintf(stderr, "Ld    : \n"); print_mat(Ld);
        fprintf(stderr, "Ldinv : %f\n", Ldinv.determinant()); print_mat(Ldinv);
        fprintf(stderr, "Ldts  : \n"); print_mat(Ldts);
        fprintf(stderr, "Ldtsinv: %f\n", Ldtsinv.determinant()); print_mat(Ldtsinv);
        fprintf(stderr, "Jts   : \n"); print_mat(Jts);
        fprintf(stderr, "Phi(%f): \n", Phi.determinant()); print_mat(Phi);
        fprintf(stderr, "Phiinv: \n"); print_mat(Phiinv);
        
//        fprintf(stderr, "xddot : \n"); print_mat(xddotfv);
//        fprintf(stderr, "xdot : \n"); print_mat(xdotfv);
    }
#endif
    J = MatrixXd::Zero(3,3);

    J(0,0) = -sin(theta);
    J(0,1) = cos(theta);
    J(1,0) = -sin(theta-2*M_PI/3);
    J(1,1) = cos(theta-2*M_PI/3);
    J(2,0) = -sin(theta+2*M_PI/3);
    J(2,1) = cos(theta+2*M_PI/3);
    J(0,2) = J(1,2) = J(2,2) = R;
    J = J / r;

    MatrixXd qdot,xdot;
    qdot = MatrixXd::Zero(3,1);
    qdot(0) = wheel_vel[0];
    qdot(1) = wheel_vel[1];
    qdot(2) = wheel_vel[2];
    xdot = J.inverse() * qdot;
    odo = odo + xdot/SERVO_RATE;
    odo_dot = xdot;

    command = tau * 1000.;
    for ( int i = 0 ; i < 3 ; i++ )
        command[i] = command[i] + compensate_friction(command[i], wheel_vel[i], 0., 1., 0.);
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

	return command;
}

static double goal_pos[3];
static int old_sign[3] = {0};
void test15_set_command(double command[3])
{
    int ii;

    goal_pos[0] = command[0];
    goal_pos[1] = command[1];
    goal_pos[2] = command[2];
    fprintf(stderr, " SET GOAL %f %f %f\n", goal_pos[0], goal_pos[1], goal_pos[2]);
    for ( ii = 0 ; ii < 3 ; ii++ )
    {
        cur_vel[ii] = xdotfv[ii];
        cur_posv[ii] = xfv[ii];
    }
    for ( ii = 0 ; ii < 3 ; ii++ )
    {
        err_posv[ii] = cur_posv[ii] - goal_pos[ii];
        sign[ii] = getSign(.5/ACC, err_posv[ii], cur_vel[ii]);
        phasev[ii] = 0.;
        old_sign[ii] = 0.;
        cross_point[ii] = err_posv[ii] - .5/ACC*sign[ii]*cur_vel[ii]*cur_vel[ii];
    }
}

jspace::Vector test15_loop(void )
{
    jspace::Vector command(3);
    int ii;
    double acc;
    double abs_err[3];

    for ( ii = 0 ; ii < 3 ; ii++ )
    {
        cur_vel[ii] = xdotfv[ii];
        cur_posv[ii] = xfv[ii];
    }

    for ( ii = 0 ; ii < 3 ; ii++ )
    {
        err_posv[ii] = cur_posv[ii] - goal_pos[ii];
        abs_err[ii] = fabs(err_posv[ii]);
    }

    double cmd;

    double mass = remote_command[4];
    double kp =remote_command[5];
    double kv =remote_command[6];
    for ( ii = 0 ; ii < 3 ; ii++ )
    {
        bool use_kp, use_kv;

        use_kp = use_kv = true;

        if ( phasev[ii] == 0. )
        {
            sign[ii] = getSign(.5/ACC, err_posv[ii], cur_vel[ii]);
            if ( cur_vel[ii] * sign[ii] >= 0. )
            {
                phasev[ii] = 0.5;
            }
            if ( abs_err[ii] < fabs(cross_point[ii]) )
            {
                nxt_vel = -sign[ii] * sqrt(fabs(err_posv[ii]-cross_point[ii])*2*ACC) ;
                nxt_err = nxt_vel * nxt_vel * sign[ii] * 0.5 / ACC + cross_point[ii];
            }
            else
            {
                nxt_vel = 0;
                nxt_err = cross_point[ii];
            }
            use_kp = false;
            acc = ACC * sign[ii];
        }
        if ( phasev[ii] == 0.5 )
        {
            bool change = true;
            for ( int jj = 0 ; jj < 3 ; jj++ )
                if ( phasev[jj] < 0.5 )
                    change = false;

            if ( change )
                phasev[ii] = 1.;
            nxt_vel = 0;
            nxt_err = 0;
            acc = 0;
            use_kp = false;
            use_kv = false;
        }
        if ( phasev[ii] == 1. )
        {
            sign[ii] = getSign(.5/ACC, err_posv[ii], cur_vel[ii]);
            if ( old_sign[ii] != 0 && sign[ii] != old_sign[ii] )
            {
                phasev[ii] = 2;
                cross_point[ii] = 0.;
            }
            nxt_vel = sign[ii] * sqrt(fabs(err_posv[ii]-cross_point[ii])*2*ACC) ;
            nxt_err = nxt_vel * nxt_vel * sign[ii] * 0.5 / ACC + cross_point[ii];
            acc = ACC * sign[ii];
            use_kp = false;
        }
        else if ( phasev[ii] == 2. )
        {
            nxt_vel = - sign[ii] * sqrt(2*ACC*abs_err[ii]);
            nxt_err = nxt_vel * nxt_vel * sign[ii] * 0.5 / ACC;

            acc = ACC * sign[ii] * -1;
            use_kp = false;
        }

        if ( abs_err[ii] < 0.7 )
            phasev[ii] = 3.;

        if ( phasev[ii] == 3. )
        {
            nxt_vel = 0.;
            nxt_err = 0.;
            acc = 0.;
            use_kv = false;
        }


        if ( nxt_vel >= W_MAX )
        {
            acc = ACC*.7;
            nxt_vel = W_MAX; // + acc * nxt_step;
            use_kp = false;
        }
        else if ( nxt_vel <= -W_MAX )
        {
            acc = -ACC*.7;
            nxt_vel = -W_MAX; // + acc * nxt_step;
            use_kp = false;
        }

        acc_termv[ii] = mass * acc * (1-exp(-3*abs_err[ii]));
        pos_termv[ii] = kp * ( err_posv[ii] );
        vel_termv[ii] = kv * ( cur_vel[ii] - nxt_vel ) * (1-exp(-1*abs_err[ii]));
        cmd = acc_termv[ii] - ((use_kp)?(pos_termv[ii]):0.) - ((use_kv)?(vel_termv[ii]):0);
        command[ii] = cmd;
        old_sign[ii] = sign[ii];
       
        if ( fabs(cmd) > fabs(pos_termv[ii]) )
            phasev[ii] = 3.;
    }

	return command;
}

#define W_DES (-0.5)
#define Radius 0.5
jspace::Vector test16_loop(void )
{
	int i;
	static double force = 0;
	jspace::Vector command(3);
	double speed;
	static int wait = 0;
    double xdotdes[3];
    double xddotdes[3];
    double sec = sTimeStamp / 1000000.;
    double mass = remote_command[4];
    double kp =remote_command[5];
    double kv =remote_command[6];
    static double phase = 0;
    bool calibrate = false;

    xdes[0] = Radius*cos(sec*W_DES + phase);
    xdes[1] = Radius*sin(sec*W_DES + phase);
    xdes[2] = 1.53;
#if 0
#if 0
    updateRegion(xdes[0], xdes[1]);
#else
    updateContact(xdes, xfv);
#endif
#else
    gRegion = REGION_FREE;
#endif
    if ( gRegion == REGION_CONTACT )
    {
        double x, y, x0, y0;
        
        x = xdes[0], y = xdes[1], x0 = xfv[0], y0 = xfv[1];
        if ( abs(gXSlope) < abs(gYSlope) )
        {
            if ( fabs(x*x + pow((gXSlope * (x - x0 ) + y0),2.) - Radius*Radius) < CONTACT_THRESHOLD )
            {
                phase -= W_DES*1./SERVO_RATE;
            }
                                       
        }
        else
        {
            if ( fabs(x*x + pow((gYSlope * (y - y0 ) + x0),2.) - Radius*Radius) < CONTACT_THRESHOLD )
            {
                phase -= W_DES*1./SERVO_RATE;
            }
        }
    }
    xdes[0] = Radius*cos(sec*W_DES + phase);
    xdes[1] = Radius*sin(sec*W_DES + phase);
    xdotdes[0] = -W_DES*Radius*sin(sec*W_DES + phase);
    xdotdes[1] = W_DES*Radius*cos(sec*W_DES + phase);
    xdotdes[2] = 0.;
    xddotdes[0] = -W_DES*W_DES*Radius*cos(sec*W_DES + phase);
    xddotdes[1] = -W_DES*W_DES*Radius*sin(sec*W_DES + phase);
    xddotdes[2] = 0.;

    for ( i  = 0 ; i < 3 ; i++ )
    {
        err_posv[i] = xfv(i) - xdes[i];
        if ( i == 2 )
        {
            int iteration = 0;
            kp *= 2;
            while ( fabs(err_posv[2]) > M_PI && iteration < 10 )
            {
                if ( err_posv[2] > 0 )
                    err_posv[2] -= 2*M_PI;
                else if ( err_posv[2] < 0 )
                    err_posv[2] += 2*M_PI;
                iteration++;
            }
        }
        command[i] = mass * xddotdes[i] - kp*err_posv[i] - kv*(xdotfv(i) - xdotdes[i]);
//        sCommand[i] = command[i];
        if ( fabs(command[i]) > 1000 )
        {
            calibrate = true;
//            command[i] = 0;
        }
    }

    if ( calibrate )
    {
    }

	return command;
}

MatrixXd getVec(double *pDest, MatrixXd &org)
{
    MatrixXd vec;

    vec = MatrixXd::Zero(3,1);
    vec[0] = pDest[0] - org[0];
    vec[1] = pDest[1] - org[1];
    vec[2] = pDest[2] - org[2];

    return vec;
}

static MatrixXd org;
void test17_init(void)
{
     org = MatrixXd::Zero(3,1);
     err_posv = MatrixXd::Zero(3,1);
     destv = MatrixXd::Zero(3,1);
}

jspace::Vector test17_loop(void )
{
	int i;
	static double force = 0;
	jspace::Vector command(3);
	static int wait = 0;
    static double offset = 0;
    double xdotdes[3];
    double xddotdes[3];
    double sec = sTimeStamp / 1000000. - offset;
    double mass = remote_command[4];
    double kp =remote_command[5];
    double kv =remote_command[6];
    static double phase = 0;
    bool calibrate = false;
//    double speed = fabs(W_DES);
    double speed[] = { 0.15, 0.15, 0.15, 0.08 };
    double dest[][3] = 
    {
        { -0.1, -0.65, 1.03 },
        { -1.86, -0.45, 1.03 },
        { -1.86, 0.33, 1.03 },
        { 1.0, 0.33, 1.03 }
    };
    int numDest = sizeof(dest) / (sizeof(double)*3);
    double err, dist;

    if ( remote_command[7] > 0. )
    {
        offset = sTimeStamp/ 1000000.;
        remote_command[7] = 0.;
        org = xfv;
    }
    else if ( remote_command[7] < 0. )
    {
    
        offset = sTimeStamp/ 1000000.;
        destIndex = -1;
    }

    MatrixXd vec, vel;
    vec = getVec(dest[destIndex], org);
    dist = vec.norm();

    if ( speed[destIndex]*sec < dist )
        vel = speed[destIndex] * vec / dist;
    else
        vel = vec / sec;

    destv[0] = xdes[0] = org[0] + sec*vel[0];
    destv[1] = xdes[1] = org[1] + sec*vel[1];
    destv[2] = xdes[2] = org[2] + sec*vel[2];

//    xdes[0] = xfv[0] + sec*speed;
//    xdes[1] = xfv[1];// + sec*speed;
//    xdes[2] = 1.51;
    xdotdes[0] = vel[0];
    xdotdes[1] = vel[1];
    xdotdes[2] = vel[2];
    xddotdes[0] = 0;
    xddotdes[1] = 0;
    xddotdes[2] = 0;

    for ( i  = 0 ; i < 3 ; i++ )
    {
        err_posv[i] = xfv(i) - xdes[i];
        if ( i == 2 )
        {
            int iteration = 0;
            kp *= 2;
            kv *= 2;
            while ( fabs(err_posv[2]) > M_PI && iteration < 10 )
            {
                if ( err_posv[2] > 0 )
                    err_posv[2] -= 2*M_PI;
                else if ( err_posv[2] < 0 )
                    err_posv[2] += 2*M_PI;
                iteration++;
            }
        }
        command[i] = mass * xddotdes[i] - kp*err_posv[i] - kv*(xdotfv(i) - xdotdes[i]);
        opcmd[i] = command[i];
//        sCommand[i] = command[i];
        if ( fabs(command[i]) > 1000 )
        {
            calibrate = true;
//            command[i] = 0;
        }
    }

    MatrixXd _destv = MatrixXd::Zero(3,1);
    _destv[0] = dest[destIndex][0];
    _destv[1] = dest[destIndex][1];
    _destv[2] = dest[destIndex][2];
    err_posv = _destv - xfv;
    err = err_posv.norm();
    desired_pos = err;

    if ( err < 0.1 || destIndex < 0 )
    {
        org = xfv;
        offset = sTimeStamp/ 1000000.;
        if ( destIndex-1 < numDest )
            destIndex++; 
        vec = getVec(dest[destIndex], org);
        dist = vec.norm();
    }

    if ( calibrate )
    {
    }

	return command;
}


#if 0
jspace::Vector test18_loop(void )
{
	int i;
	jspace::Vector command(3);

#if 1
    // For each wheel....
	for ( i = 0 ; i < 3 ; i++ )
	{

		//begin new torque controller
		//inputs to controller
		float measuredWheelTorque_Nm = calibrate_torque(i, raw_adc_value[0][i]);
		float desiredWheelTorque_Nm = (float)remote_command[i];
		float gearboxEfficiency[3] = {0.75, 0.75, 0.75};

		//outputs from controller
		float desiredGearMotorTorque_Nm = 0;

		//internal controller variables
//		float wheelTorqueErr_Nm = 0;
		float P_gain = remote_command[4];
		float I_gain = 0; // -0.02;
		float D_gain = 0;

		wheelTorqueErr_Nm[i] = desiredWheelTorque_Nm - measuredWheelTorque_Nm;

#if 1
		lp_torque[i]->input(wheelTorqueErr_Nm[i]);
		wheelTorqueErrFiltered_Nm[i] = lp_torque[i]->output();
#else
		wheelTorqueErrFiltered_Nm[i] = wheelTorqueErr_Nm[i];
#endif
		//open loop term
		desiredGearMotorTorque_Nm = desiredWheelTorque_Nm / gearboxEfficiency[i]; 

		//proportional feedback
//		P_term[i] = wheelTorqueErr_Nm[i]*P_gain;  
		P_term[i] = wheelTorqueErrFiltered_Nm[i]*P_gain;  

	  //integrate feedback
		if ( fabs(wheelTorqueErrFiltered_Nm[i]) > 0.1 )
			wheelTorqueErrorSum_Nm[i] += wheelTorqueErrFiltered_Nm[i];
		I_term[i] = wheelTorqueErrorSum_Nm[i] * I_gain;

	  //derivative feedback

//		if ( control_mode == MODE_COMPENSATE_TORQUE )
		{
			desiredGearMotorTorque_Nm += P_term[i];
			desiredGearMotorTorque_Nm += I_term[i];
		}

	  if( ++loop_counter >= 1000){
		loop_counter=0;
	    printf("%d: desiredWheelTorque_Nm: %f\tmeasuredWheelTorque_Nm: %f\twheelTorqueErr_Nm: %f\twheelTorqueErrFiltered_Nm: %f\tdesiredMotorTorque_Nm: %f\n", 
                i+1, desiredWheelTorque_Nm, measuredWheelTorque_Nm, wheelTorqueErr_Nm[i], desiredGearMotorTorque_Nm);
	  }

        motorTorqueCommand_Nm[i] = desiredGearMotorTorque_Nm;

		//output to motor
		command[i] = torque_command(i, -motorTorqueCommand_Nm[i]); // send T_{cmd} to motor
		//end new torque controller

		motorTorqueComand_Last_Nm[i] = motorTorqueCommand_Nm[i]; // saving T_{cmd} for next round
	}
#endif
	
	command[1] = 0.;
	command[2] = 0.;
	sCommand[0] = command[0];
	sCommand[1] = command[1];
	sCommand[2] = command[2];

	return command;
}
#endif

// state feedback controller 
jspace::Vector test19_loop(void )
{
    jspace::Vector acc = jspace::Vector::Zero(3);
    double s1, s2, s3, s1p, s2p;
    double s1dot;
    double x, y, xdot, ydot;
    MatrixXd sdot_left, sdot_right, sdot_left_inv;
    double eta1 = remote_command[4]; //1.0;
    double eta2 = remote_command[5]; //0.01;
    double l = remote_command[6]; //0.1;
    double MAX_ACC = remote_command[7];
        
    x = xfv(0);
    y = xfv(1);
    xdot = xdotfv(0);
    ydot = xdotfv(1);
        
    s1 = x*x + y*y - Radius*Radius;
    s2 = xdot + W_DES*y;
    s3 = ydot - W_DES*x;
    s1dot = 2*x*xdot + 2*y*ydot;
    s1p = s1dot + l * s1;
    s2p = s2*s2 + s3*s3;

    sval[0] = s1;
    sval[1] = s2;
    sval[2] = s3;
    sval[3] = s1p;
    sval[4] = s2p;
        
    sdot_left = MatrixXd::Zero(2,2);
    sdot_left(0,0) = 2*x;
    sdot_left(0,1) = 2*y;
    sdot_left(1,0) = 2*s2;
    sdot_left(1,1) = 2*s3;
        
    sdot_right = MatrixXd::Zero(2,1);
#if 0
    sdot_right(0) = -eta1*sgn(s1p) - 2*xdot*xdot - 2*ydot*ydot - l*s1dot;
    sdot_right(1) = -eta2*sgn(s2p) - 2*s2*W_DES*ydot + 2*s3*W_DES*xdot;
#else
//    eta1 *= 1. * (abs(s1)*300+1);
//    eta2 *= 1. / (abs(s1)*1000+1);
    sdot_right(0) = -eta1*tanh(s1p*5) - 2*xdot*xdot - 2*ydot*ydot - l*s1dot;
    sdot_right(1) = -eta2*tanh(s2p*5) - 2*s2*W_DES*ydot + 2*s3*W_DES*xdot;
#endif
        
    if ( abs(sdot_left.determinant()) > 0.001 )
    {
        acc.block(0,0,2,1) = sdot_left.inverse() * sdot_right;
    }
    else
    {  
//        pseudoInverse(sdot_left, 0.0001, sdot_left_inv, 0);
//        acc.block(0,0,2,1) = sdot_left_inv * sdot_right;
//      cout << "SDOF: " << sdot_left << "::" << sdot_right <<endl;
//      cout << "S:    " << s1 << " " << s2 << " " << s3 << " : " << s1p << " " << s2p << endl;
//      cout << "DET:  " << sdot_left.determinant() << endl;
    }
    double acc_mag = sqrt(acc[0]*acc[0] + acc[1]*acc[1]);
    
    if ( acc_mag > MAX_ACC )
    {
        acc = acc*MAX_ACC/acc_mag;            
    }
//    acc[2] = -1.5*xfv[2] - 0.4*xdotfv[2];

    return acc; 
}

jspace::Vector test20_loop(void )
{
    jspace::Vector acc = jspace::Vector::Zero(3);
    double s1, s2, s3, s1p, s2p;
    double s1dot;
    double x, y, xdot, ydot;
    MatrixXd s1dot_left;
    MatrixXd s1dot_left_inv;
    MatrixXd s2dot_left;
    MatrixXd s2dot_left_inv;
    double s1dot_right;
    double s2dot_right;
    double b = remote_command[3];
    double eta1 = remote_command[4]; //1.0;
    double eta2 = remote_command[5]; //0.01;
    double l = remote_command[6]; //0.1;
    double MAX_ACC = remote_command[7];
        
    x = xfv(0);
    y = xfv(1);
    xdot = xdotfv(0);
    ydot = xdotfv(1);
        
    s1 = x*x + y*y - Radius*Radius;
    s2 = xdot + W_DES*y;
    s3 = ydot - W_DES*x;
    s1dot = 2*x*xdot + 2*y*ydot;
    s1p = s1dot + l * s1;
    s2p = s2*s2 + s3*s3;

    sval[0] = s1;
    sval[1] = s1p;
    sval[2] = s3;
    sval[3] = s1p;
    sval[4] = s2p;
        
    s1dot_left = MatrixXd::Zero(1,2);
    s1dot_left(0,0) = 2*x;
    s1dot_left(0,1) = 2*y;

    s2dot_left = MatrixXd::Zero(1,2);
    s2dot_left(0,0) = 2*s2;
    s2dot_left(0,1) = 2*s3;
        
#if 0
    s1dot_right = -eta1*sgn(s1p) - 2*xdot*xdot - 2*ydot*ydot - l*s1dot;
    s2dot_right = -eta2*sgn(s2p) - 2*s2*W_DES*ydot + 2*s3*W_DES*xdot;
#else
    s1dot_right = -eta1*tanh(s1p) - 2*xdot*xdot - 2*ydot*ydot - l*s1dot;// + b*tanh(xdot) + b*tanh(ydot);
    s2dot_right = -eta2*tanh(s2p) - 2*s2*W_DES*ydot + 2*s3*W_DES*xdot;
#endif
    pseudoInverse(s1dot_left.transpose(), 0.0001, s1dot_left_inv, 0);
    pseudoInverse(s2dot_left.transpose(), 0.0001, s2dot_left_inv, 0);
        
    acc.block(0,0,2,1) = s1dot_left_inv.transpose() * s1dot_right;
//        s1dot_left_inv.transpose() * s1dot_right;
//                       + s2dot_left_inv.transpose() * s2dot_right;
    double acc_mag = sqrt(acc[0]*acc[0] + acc[1]*acc[1]);
    
    if ( acc_mag > MAX_ACC )
    {
        acc = acc*MAX_ACC/acc_mag;            
    }
//    acc[2] = -1.5*xfv[2] - 0.4*xdotfv[2];

    return acc; 
}

jspace::Vector test21_loop(void )
{
    jspace::Vector acc = jspace::Vector::Zero(3);
    double s0, s1, s2, s3, s1p, s2p;
    double s1dot;
    double x, y, xdot, ydot;
    MatrixXd sdot_left;
    MatrixXd sdot_left_inv;
    MatrixXd sdot_right;
    double eta1 = remote_command[4]; //1.0;
    double eta2 = remote_command[5]; //0.01;
    double l = remote_command[6]; //0.1;
    double MAX_ACC = remote_command[7];
    double d,u;
        
    x = xfv(0);
    y = xfv(1);
    xdot = xdotfv(0);
    ydot = xdotfv(1);
        
    d =  x*x + y*y - Radius*Radius;
    u =  sqrt(x*x + y*y);
    s0 = xdot + l*d*x/u;
    s1 = ydot + l*d*y/u;
    s2 = xdot + W_DES*y;
    s3 = ydot - W_DES*x;
    s1dot = 2*x*xdot + 2*y*ydot;
    s1p = s1dot + l * s1;
    s2p = s2*s2 + s3*s3;

    double s = sqrt(s0*s0 + s1*s1);
    static int state = 0;
    sval[0] = s0;
    sval[1] = s1;
    sval[2] = state;
    sval[3] = s;
        
    sdot_left = MatrixXd::Zero(2,2);
    sdot_left(0,0) = 1;
    sdot_left(0,1) = 0;
    sdot_left(1,0) = 0;
    sdot_left(1,1) = 1;
//    sdot_left(2,0) = 1;
//    sdot_left(2,1) = 0;
//    sdot_left(3,0) = 0;
//    sdot_left(3,1) = 1;

        
#if 0
    s1dot_right = -eta1*sgn(s1p) - 2*xdot*xdot - 2*ydot*ydot - l*s1dot;
    s2dot_right = -eta2*sgn(s2p) - 2*s2*W_DES*ydot + 2*s3*W_DES*xdot;
#elif 0

    if ( s < 0.15)
       state = 1;
    if ( s > 0.20 )
       state = 0;
//    if ( state == 0 )
    {
        acc(0) = -eta1*tanh(s0/2.) - l*(2*x*xdot + 2*y*ydot)*x/u - l*d*xdot/u + .5*l*d*x/pow(u,3.)*(2*x*xdot + 2*y*ydot);
        acc(1) = -eta1*tanh(s1/2.) - l*(2*x*xdot + 2*y*ydot)*y/u - l*d*ydot/u + .5*l*d*y/pow(u,3.)*(2*x*xdot + 2*y*ydot);
    }
//    else
    {
//        acc(0) = -eta2*tanh(s2) + W_DES*W_DES*x;
        acc(0) += -eta2*tanh(s2) - W_DES*ydot;
//        acc(1) = -eta2*tanh(s3) + W_DES*W_DES*y;
        acc(1) += -eta2*tanh(s3) + W_DES*xdot;
    }
#else
    double threshold = 1*(s - 0.15);
    double alpha;
    MatrixXd acc1, acc2;
    acc1 = acc2 = MatrixXd::Zero(3,1);
    acc1(0) = -eta1*tanh(s0/2.) - l*(2*x*xdot + 2*y*ydot)*x/u - l*d*xdot/u + .5*l*d*x/pow(u,3.)*(2*x*xdot + 2*y*ydot);
    acc1(1) = -eta1*tanh(s1/2.) - l*(2*x*xdot + 2*y*ydot)*y/u - l*d*ydot/u + .5*l*d*y/pow(u,3.)*(2*x*xdot + 2*y*ydot);
    acc2(0) = -eta2*tanh(s2) - W_DES*ydot;
    acc2(1) = -eta2*tanh(s3) + W_DES*xdot;

    alpha = (tanh(threshold)+1)/2.;

//    acc = acc1 * alpha + acc2 * (1-alpha);
    acc = acc1;
#endif
    double acc_mag = sqrt(acc[0]*acc[0] + acc[1]*acc[1]);
    
    if ( acc_mag > MAX_ACC )
    {
        acc = acc*MAX_ACC/acc_mag;            
    }
    acc[2] = -2.5*(xfv[2]-0.04) - 0.4*xdotfv[2];

    return acc; 
}

jspace::Vector test22_loop(void )
{
    jspace::Vector acc = jspace::Vector::Zero(3);
    double s0, s1, s2, s3;
    double x, y, xdot, ydot;
    double eta1 = remote_command[4]; //1.0;
    double eta2 = remote_command[5]; //0.01;
    double MAX_ACC = remote_command[7];
    double v_max = remote_command[1];
    double beta = remote_command[2];
    double d,ddot,u;
        
    x = xfv(0);
    y = xfv(1);
    xdot = xdotfv(0);
    ydot = xdotfv(1);
        
    d =  x*x + y*y - Radius*Radius;
    ddot = 2*x*xdot + 2*y*ydot;
    u =  sqrt(x*x + y*y);
    s0 = xdot + v_max*tanh(beta*d)*x/u;
    s1 = ydot + v_max*tanh(beta*d)*y/u;
    s2 = xdot + W_DES*y;
    s3 = ydot - W_DES*x;

    double s = sqrt(s0*s0 + s1*s1);
    static int state = 0;
    sval[0] = s0;
    sval[1] = s1;
    sval[2] = s2;
    sval[3] = s3;
        
    double threshold = 5*(s - 0.05);
    double alpha;
    MatrixXd acc1, acc2;
    acc1 = acc2 = MatrixXd::Zero(3,1);
    acc1(0) = -eta1*tanh(s0/1.) - v_max*(1+tan(beta*d)*tan(beta*d))*beta*ddot*x/u - v_max*tanh(beta*d)*xdot/u + .5*v_max*tanh(beta*d)*x/pow(u,3.)*(2*x*xdot + 2*y*ydot);
    acc1(1) = -eta1*tanh(s1/1.) - v_max*(1+tan(beta*d)*tan(beta*d))*beta*ddot*y/u - v_max*tanh(beta*d)*ydot/u + .5*v_max*tanh(beta*d)*y/pow(u,3.)*(2*x*xdot + 2*y*ydot);
#if 1
    acc2(0) = -eta2*tanh(s2/4.) - W_DES*ydot;
    acc2(1) = -eta2*tanh(s3/4.) + W_DES*xdot;
#else
    acc2(0) = -eta2*s2 - W_DES*ydot;
    acc2(1) = -eta2*s3 + W_DES*xdot;
#endif

    alpha = (tanh(threshold)+1)/2.;

    acc = acc1 * alpha + acc2 * (1-alpha);
//    acc = acc1;
//    acc = acc1 + acc2 * (1-alpha);
    acc = acc2;

    double acc_mag = sqrt(acc[0]*acc[0] + acc[1]*acc[1]);
    
    if ( acc_mag > MAX_ACC )
    {
        acc = acc*MAX_ACC/acc_mag;            
    }
    acc[2] = -2.5*(xfv[2]-0.04) - 0.4*xdotfv[2];

    return acc; 
}
