#include <stdio.h>
#include "base_config.h"
#include "task.h"

extern double sCommand[3];

class Task02: public Task
{
	public:
	Task02(void);
	virtual void init(void);
    virtual void init(int argc, char *argv[]);
	virtual jspace::Vector update(void);
	virtual string getName(void);
    virtual void log(void);

    private:
    int index;
};
string Task02::getName(void)
{
	return "Task02";
}
Task02::Task02(void)
{
	cout << "Creating " << getName() << endl;
}
void Task02::init(void)
{
	mode = CTRL_MODE_THETA;
}

double q_org[3];
void Task02::init(int argc, char *argv[])
{
    index = 0;
	mode = CTRL_MODE_THETA;
	q_org[0] = q[0];
	q_org[1] = q[1];
	q_org[2] = q[2];

}

jspace::Vector Task02::update(void)
{
    Vector command = Vector::Zero(3);
	static int count = 0;

	command[0] = q_org[0]-count/10000.;
	command[1] = q_org[1]-count/10000.;
	command[2] = q_org[2]-count/10000.;
	mode = CTRL_MODE_THETA;
	count++;

	return command;
}

void Task02::log(void)
{
    fprintf(stderr, "q\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", q[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "qdot\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", qdot[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "raw_torque\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", raw_torque[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "command\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", sCommand[i]);
    fprintf(stderr, "\n");
    fprintf(stderr, "pwm\n");
    for ( int i  = 0 ; i < 3 ; i++ )
        fprintf(stderr, "%+7lf ", pwm_cmd[i]);
    fprintf(stderr, "\n");
	fprintf(stderr, "SP %f, %f\n", shm_param.sp_g[index], shm_param.sp_d[index]);
}


TaskRegistrar<Task02> task02;
