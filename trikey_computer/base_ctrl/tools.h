#include "base_config.h"
#include <iostream>
#include <math.h>
#include <jspace/wrap_eigen.hpp>

int sgn(double in);
double tanh(double in);
jspace::Vector tanh(jspace::Vector in);
jspace::Vector sgn(jspace::Vector in);
