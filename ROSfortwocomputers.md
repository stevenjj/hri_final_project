Set up ROS for two computers.

Ensure that both computers are connected to the same network. Find each computer's IP address.
$ hostname - I


Different IP address types:
10.0.0.0 – 10.255.255.255 (Class A)
172.16.00 – 172.31.255.255 (Class B)
192.168.0.0 – 192.168.255.255 (Class C)

When connected to the university it might look something like
10.x.x.x

When using a local network it might be something like
192.168.x.x


Assuming you use Ubuntu for ROS, edit ~/.bashrc with your text editor:
$ subl ~/.bashrc

Add the following lines to the master computer
Master computer:
export ROS_MASTER_URI=http://10.x.x.x:11311
export ROS_IP=10.x.x.x

Slave Computer
export ROS_MASTER_URI=http://10.x.x.x:11311
export ROS_IP=10.x.x.x


where ROS_MASTER_URI is the same for both computer
and ROS_IP is each computer's respective IP addresses.
