#include "ros/ros.h"
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/JointState.h>


// #define TRIKEY_SIDE_WIDTH 0.6

// #define CELL_X_WIDTH 0.7
// #define CELL_Y_WIDTH 0.7

// #define NUM_CELL_WIDTH 50
// #define NUM_CELL_HEIGHT 50

// #define FREE_CELL 0
// #define ROBOT_OCCUPIED 1
// #define HUMAN_OCCUPIED 2
// #define OBSTACLE_OCCUPIED 3

// This class creates a grid of the world starting at x=0, y=0 and goes to x=CELL_X_WIDTH*NUM_CELL_WIDTH, y =CELL_Y_WIDTH*NUM_CELL_HEIGHT 

// class Cell{
// public:
// 	float x_center;
// 	float y_center;

// 	int grid_x_index;
// 	int grid_y_index;

// 	int id;
// 	Cell(int id_);
// 	Cell(float x_init, float y_init, int x_index_init, int y_index_init, int cell_id);
// 	Cell();
// 	~Cell();
// };
// Cell::Cell(){
// }
// Cell::~Cell(){}

// Cell::Cell(float x_init, float y_init, int x_index_init, int y_index_init, int cell_id): x_center(x_init),
// 																			y_center(y_init),
// 																			grid_x_index(x_index_init),
// 																			grid_y_index(y_index_init),
// 																			id(cell_id) {
// }


// class GridMap{
// public:
// 	ros::NodeHandle node;
// 	ros::Publisher array_pub;
// 	ros::Publisher cell_array_pub;

// 	ros::Publisher CBA_grid_pub;


// 	ros::Subscriber trikey_state_sub;

//     // Grid Variables
// 	float cell_x_width;
// 	float cell_y_width;
// 	int num_of_cells_width;
// 	int num_of_cells_height;	

//     std::vector< std::vector<Cell> > map_grid;
// 	visualization_msgs::MarkerArray cell_array;

// 	std::map<int, int> robot_occupied_cells;


//     // Functions
//     visualization_msgs::Marker make_cell_marker(int index, int grid_x_index, int grid_y_index, int cell_color);
//     void publish();
//     void joint_state_callback(const sensor_msgs::JointState::ConstPtr &msg);


// 	void change_cell_color(int marker_index, int occupancy_type);
// 	void bounding_box_to_occupany(float x_center, float y_center, float width, float length, int occupancy_type);
// 	int ij_index_to_true_index(int index_i, int index_j);


// 	void CBA_publish();

//     // Constructor Destructor Functions
// 	GridMap();
// 	~GridMap();	

// private:
//     void initialize_marker_array();
// };


// GridMap::GridMap(){
// 	cell_x_width = CELL_X_WIDTH;
// 	cell_y_width = CELL_Y_WIDTH;	
// 	num_of_cells_width = NUM_CELL_WIDTH;
// 	num_of_cells_height = NUM_CELL_HEIGHT;	

// 	int cell_id = 0;
//     // -----------------------------------------------------------
//     // Initialize cell positions and construct map_grid nxm vector
// 	for (size_t j = 0; j < num_of_cells_height; j++){
//         std::vector<Cell> row;
//         // Iterate over columns
// 		for (size_t i = 0; i < num_of_cells_width; i++){
//             float cell_x_pos = i*CELL_X_WIDTH;
//             float cell_y_pos = j*CELL_Y_WIDTH;            		
// 			Cell cell_new( cell_x_pos, cell_y_pos, i, j, cell_id);
//             // Add to current row
//             row.push_back(cell_new);
//             cell_id++;
// 		}
//         // Create first row
//         map_grid.push_back(row);            
// 	}
//     // -----------------------------------------------------------
// 	initialize_marker_array();
// }

// void GridMap::initialize_marker_array(){
// 	int current_index = 0;
//     for(size_t i = 0; i < map_grid.size(); i++){
//         for(size_t j = 0; j < map_grid[i].size(); j++){
//             Cell current_cell;
//             current_cell = map_grid[i][j];     

//             int cell_status = FREE_CELL;

//             visualization_msgs::Marker current_marker;
//             current_marker = make_cell_marker(current_index, current_cell.grid_x_index, current_cell.grid_y_index, cell_status);
               
//             cell_array.markers.push_back(current_marker);
//             current_index++;
//         }
//     }
// }

// GridMap::~GridMap(){}


// visualization_msgs::Marker GridMap::make_cell_marker(int index, int grid_x_index, int grid_y_index, int cell_status){
// 	visualization_msgs::Marker marker;
// 	marker.header.frame_id = "world";
// 	marker.header.stamp = ros::Time();
// 	marker.ns = "cells";
// 	marker.id = index;
// 	marker.type = visualization_msgs::Marker::CUBE;
// 	marker.action = visualization_msgs::Marker::ADD;
// 	marker.pose.position.x = cell_x_width*grid_x_index;
// 	marker.pose.position.y = cell_y_width*grid_y_index;
// 	marker.pose.position.z = -0.05;
// 	marker.pose.orientation.x = 0.0;
// 	marker.pose.orientation.y = 0.0;
// 	marker.pose.orientation.z = 0.0;
// 	marker.pose.orientation.w = 1.0;
// 	marker.scale.x = 0.95*cell_x_width;
// 	marker.scale.y = 0.95*cell_y_width;
// 	marker.scale.z = 0.1;
// 	marker.color.a = 0.5; // Don't forget to set the alpha!

// 	if (cell_status == ROBOT_OCCUPIED){
// 		marker.color.r = 0.0; // BLUE color
// 		marker.color.g = 0.0;
// 		marker.color.b = 0.8;		
// 	}else if (cell_status == HUMAN_OCCUPIED){
// 		marker.color.r = 0.8; // YELLOW color
// 		marker.color.g = 0.8;
// 		marker.color.b = 0.0;				
// 	}else if(cell_status == OBSTACLE_OCCUPIED){
// 		marker.color.r = 0.8; // RED color
// 		marker.color.g = 0.0;
// 		marker.color.b = 0.0;	
// 	}else{
// 		marker.color.r = 0.8; // Gray color
// 		marker.color.g = 0.8;
// 		marker.color.b = 0.8;
// 	}

//     marker.lifetime = ros::Duration();

//     return marker;
// }


// void GridMap::publish(){
//  	for (std::map<int,int>::iterator it=robot_occupied_cells.begin(); it!=robot_occupied_cells.end(); ++it){
//     	int marker_index = it->first; 
//     	if ((marker_index < (NUM_CELL_WIDTH*NUM_CELL_HEIGHT)) && marker_index >= 0){
// 	    	change_cell_color(marker_index, ROBOT_OCCUPIED);   
// 	    }    	
// 	}

// 	cell_array_pub.publish(cell_array);
// }

// void GridMap::change_cell_color(int marker_index, int occupancy_type){
// 	if (occupancy_type == ROBOT_OCCUPIED){
// 	    cell_array.markers[marker_index].color.r = 0.0;
// 	    cell_array.markers[marker_index].color.g = 0.0;
// 	    cell_array.markers[marker_index].color.b = 0.8;
// 	}else if (occupancy_type == HUMAN_OCCUPIED){
// 		cell_array.markers[marker_index].color.r = 0.8; // YELLOW color
// 		cell_array.markers[marker_index].color.g = 0.8;
// 		cell_array.markers[marker_index].color.b = 0.0;				
// 	}else if(occupancy_type == OBSTACLE_OCCUPIED){
// 		cell_array.markers[marker_index].color.r = 0.8; // RED color
// 		cell_array.markers[marker_index].color.g = 0.0;
// 		cell_array.markers[marker_index].color.b = 0.0;
// 	}else{
// 		cell_array.markers[marker_index].color.r = 0.8; // Gray color
// 		cell_array.markers[marker_index].color.g = 0.8;
// 		cell_array.markers[marker_index].color.b = 0.8;
// 	}
// }


// void GridMap::joint_state_callback(const sensor_msgs::JointState::ConstPtr &msg){
// 	float robot_x = msg->position[3];
// 	float robot_y = msg->position[4];	

//  	for (std::map<int,int>::iterator it=robot_occupied_cells.begin(); it!=robot_occupied_cells.end(); ++it){
//     	int marker_index = it->first; 
// 		if ((marker_index < (NUM_CELL_WIDTH*NUM_CELL_HEIGHT)) && marker_index >= 0){
// 	    	change_cell_color(marker_index, FREE_CELL);   
// 	    }
// 	}

// 	robot_occupied_cells.clear();
// 	bounding_box_to_occupany(robot_x, robot_y, TRIKEY_SIDE_WIDTH, TRIKEY_SIDE_WIDTH, ROBOT_OCCUPIED);


// }

// void GridMap::bounding_box_to_occupany(float x_center, float y_center, float width, float length, int occupancy_type){
// 	float lower_left_corner_x = (x_center - width/2.0);
// 	float lower_left_corner_y = (y_center - length/2.0);

// 	float upper_right_corner_x = (x_center + width/2.0);
// 	float upper_right_corner_y = (y_center + length/2.0);

// 	int lower_left_corner_index_i = (int) round( (lower_left_corner_x)/cell_x_width);
// 	int lower_left_corner_index_j = (int) round( (lower_left_corner_y)/cell_y_width);

// 	int upper_right_corner_index_i = (int) round( (upper_right_corner_x)/cell_x_width);
// 	int upper_right_corner_index_j = (int) round( (upper_right_corner_y)/cell_y_width);

// /*
// 	std::cout << lower_left_corner_x << " " << lower_left_corner_y << std::endl;
// 	std::cout << upper_right_corner_x << " " << upper_right_corner_y << std::endl;	
// //	std::cout << "START" << std::endl;
// 	std::cout << lower_left_corner_index_i << " " << lower_left_corner_index_j << std::endl;
// 	std::cout << upper_right_corner_index_i << " " << upper_right_corner_index_j << std::endl;	
// */
// 	// Color all cells inside the box formed by the two corners
// 	for(int i = 0; i < (upper_right_corner_index_i - lower_left_corner_index_i + 1); i++){
// 		for(int j = 0; j < (upper_right_corner_index_j - lower_left_corner_index_j+1); j++){
// 			int true_index = ij_index_to_true_index(lower_left_corner_index_i + i, lower_left_corner_index_j + j);
// //			std::cout << "INDEX:" << true_index << std::endl;
// 			if (occupancy_type == ROBOT_OCCUPIED){
// 				robot_occupied_cells[true_index] = ROBOT_OCCUPIED;
// 			}

// 		}
// 	}


// }

// int GridMap::ij_index_to_true_index(int index_i, int index_j){
// 	int true_index = index_j*(NUM_CELL_WIDTH) + index_i;//index_i*(NUM_CELL_HEIGHT) + index_j;
// 	return true_index;
// }

// void GridMap::CBA_publish(){
// 	classifier::CBA_NavInfo nav_info_msg;
// 	nav_info_msg.header.stamp = ros::Time();
// 	nav_info_msg.width = 10;
// 	nav_info_msg.height = 12;

// 	int counter = 0; 
// 	for(int i = 0; i < (int) nav_info_msg.width; i++){
// 		for(int j = 0; j < (int) nav_info_msg.height; j++){
// 			nav_info_msg.cell_occupancy_type.push_back(FREE_CELL);
// 			nav_info_msg.action_policy_type.push_back(counter);
// 			counter++;			
// 		}
// 	}
// 	CBA_grid_pub.publish(nav_info_msg);

// }

int main(int argc, char **argv)
{
  ros::init(argc, argv, "grid");
  // GridMap gridmap;

  // gridmap.cell_array_pub = gridmap.node.advertise<visualization_msgs::MarkerArray>( "grid/cell_array_markers", 0 );
  // gridmap.array_pub = gridmap.node.advertise<visualization_msgs::Marker>( "visualization_marker", 0 );
  // gridmap.CBA_grid_pub = gridmap.node.advertise<classifier::CBA_NavInfo>("/CBA_grid_occ_topic", 0);

  // gridmap.trikey_state_sub = gridmap.node.subscribe<sensor_msgs::JointState>("joint_states", 0, boost::bind(&GridMap::joint_state_callback, &gridmap, _1));

std:: cout<<"Hello"<<std::endl;

  ros::Rate r(1); 

  bool published_once = false;

  while (ros::ok())
  {
	ros::spinOnce();

  	//gridmap.publish();
  //	gridmap.CBA_publish();
	r.sleep();
  }

//  ros::spin();
  return 0;
}
	