package matlab_msg;

public interface m_gridInfo extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "matlab_msg/m_gridInfo";
  static final java.lang.String _DEFINITION = "Header header\nuint32 width\nuint32 height\nint8[] cell_occupancy_type\nint8[] action_policy_type\nint32[] robot_local_cell_indices \n\n\n";
  static final boolean _IS_SERVICE = false;
  static final boolean _IS_ACTION = false;
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  int getWidth();
  void setWidth(int value);
  int getHeight();
  void setHeight(int value);
  org.jboss.netty.buffer.ChannelBuffer getCellOccupancyType();
  void setCellOccupancyType(org.jboss.netty.buffer.ChannelBuffer value);
  org.jboss.netty.buffer.ChannelBuffer getActionPolicyType();
  void setActionPolicyType(org.jboss.netty.buffer.ChannelBuffer value);
  int[] getRobotLocalCellIndices();
  void setRobotLocalCellIndices(int[] value);
}
