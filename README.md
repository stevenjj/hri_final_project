ROS Indigo for Ubuntu 14.04 LTS

Launch trikey visualization in rviz:
$ roslaunch visualize_trikey viz_trikey.launch 

Trikey
TODO List
- Cartesian Controller
- Kinect with People Detection
	- Darknet/YOLO people detection
- Kinect on URDF with correct transform
- SLAM/Localization with Trikey

Installing Kinect
	sudo apt-get install libfreenect-dev
	sudo apt-get install ros-indigo-freenect-launch

Installing Pointcloud to laser scan
	sudo apt-get install ros-indigo-pointcloud-to-laserscan 

	sudo apt-get install ros-indigo-depthimage-to-laserscan

	sudo apt-get install ros-indigo-laser-scan-matcher



Installing SLAM
	sudo apt-get install ros-indigo-slam-gmapping

	sudo apt-get install ros-indigo-rtabmap-ros	


SLAM Components
	sudo apt-get install ros-indigo-depthimage-to-laserscan
	sudo apt-get install ros-indigo-rtabmap-ros	

Running SLAM with built in map

roslaunch trikey_integrated_system rgbd_mapping.launch rviz:=true rtabmapviz:=false database_path:=~/hcrl_map.db localization:=true



Installing People Detection
	Install darknet http://pjreddie.com/darknet/install/
		1. git clone https://github.com/pjreddie/darknet.git
		2. cd darknet
		3. make
		4. ./darknet
	Install / Ensure that OpenCV is installed
		sudo apt-get install libopencv-dev python-opencv
	Next, change the 2nd line of the Makefile to read:
	OPENCV=1
	You're done! To try it out, first re-make the project. Then use the imtest routine to test image loading and displaying:
		./darknet imtest data/eagle.jpg

	Get the Pretrained weights
	wget http://pjreddie.com/media/files/yolo.weights

	Ensure that you have an NVIDIA GPU before perfoming the steps below
	**Installing CUDA
	https://developer.nvidia.com/cuda-downloads

	Download the local .deb file

	http://developer.download.nvidia.com/compute/cuda/8.0/secure/prod/docs/sidebar/CUDA_Quick_Start_Guide.pdf?autho=1479692318_31b6a75ce87be1037960faffb4a0ef79&file=CUDA_Quick_Start_Guide.pdf

	http://docs.nvidia.com/cuda/cuda-installation-guide-linux/#axzz4QbSA8Uht

	Download the weights
	wget http://pjreddie.com/media/files/tiny-yolo.weights
	./darknet yolo test cfg/yolov1/tiny-yolo.cfg tiny-yolo.weights data/person.jpg

	Hints for exporting darknet as a library
	https://github.com/BriSkyHekun/py-darknet-yolo

	Installing the darknet for ROS package
	git clone --recursive https://github.com/kunle12/dn_object_detect
	sudo apt-get install ros-indigo-image-transport-plugins 


Launching the kinect:
	roslaunch freenect_launch freenect.launch
	rosrun rviz rviz
	change frame to camera_rgb_frame
	publish PointCloud2 topic /camera/depth_registered/points



Detecting People with dn_object_detect
	roslaunch dn_object_detect objdetect.launch 



Converting pointcloud to image
	sudo apt-get install ros-indigo-image-pipeline
	
	rosrun pcl_ros convert_pointcloud_to_image input:=/camera/depth_registered/points output:=/camera/depth/cloud_image
	rosrun image_view image_view image:=/camera/depth/cloud_image


