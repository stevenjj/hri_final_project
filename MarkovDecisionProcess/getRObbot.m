function RobotSet=getRObbot(occupacymap,xstep)

[row col]=find(occupacymap==2);

RobotSet=cell(length(row),1);
for i=1:length(row)
    obsxycoord=getxycoord(row(i),xstep);
    RobotSet{i}=[obsxycoord.x+1;obsxycoord.y+1];
end
