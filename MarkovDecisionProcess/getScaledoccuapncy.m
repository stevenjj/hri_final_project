
function ScaledVector=getScaledoccuapncy(occupancymap,x_originalstep,y_originalstep,scaled)


start_i=10;
end_i=159;

start_j=90;
end_j=150;



cons_x=scaled;
cons_y=scaled;

num_x=floor(x_originalstep/scaled);
num_y=floor(y_originalstep/scaled);

ScaledVector=zeros(num_x*num_y,1);
votingVector=zeros(cons_x*cons_y,1);

lindex=0;
localindex=1;

iter=0;
for n=1:num_y
 for m=1:num_x

     iter=iter+1;
     local_x=(m-1)*cons_x+1;
     local_y=(n-1)*cons_y+1;
     %     localindex=(m-1)*num_x+n;   
    votingvector=zeros(cons_x*cons_y+2,1);
     lindex=0;
    for i=local_x:local_x+cons_x-1
        for j=local_y:local_y+cons_y-1
            lindex=lindex+1;
            globalindex=findglobalcellindex(i,j,x_originalstep);
            votingVector(lindex)=occupancymap(globalindex);
         end
    end
%     votingVector(cons_x*cons_y+1)=0;
%     votingVector(cons_x*cons_y+2)=0;
%     votingVector(cons_x*cons_y+3)=0;
%     votingVector(end)=0;
    iter
    ScaledVector(iter)=occupancymap(globalindex);;
%     ScaledVector(iter)=mode(votingVector);

 end

end