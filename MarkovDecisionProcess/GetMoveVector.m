function MoveVector= GetMoveVector(Action_num)
%% Function which mas from Action number to Moving Vector
% 1 : Right : [1 0]
% 2 : Up  : [0 1]
% 3 : Left : [-1 0]
% 4 : Down : [0 -1]

%% Initialize Movevector
MoveVector=[0,0];

switch Action_num
    case 1 
        MoveVector=[1,0];
    case 2
        MoveVector=[0,1];
    case 3
         MoveVector=[-1,0];
    case 4
         MoveVector=[0,-1];
        
    otherwise
        MoveVector=[0,0];
end

end