function uValue = aEast(xval,yval,U,POINTS,k,ObsSet,MaxVector)
moveVector=[1, 0];
yy=CheckObsall(xval,yval,ObsSet,MaxVector,moveVector);

% can't go south if at Y=1 or if in cell above sink 'because it will go to sink cell'
% if ((yval==1) || (xval==xSink && yval==ySink+1))
if yy==1
    uValue=U(k);
else
    for h=1:size(POINTS,1)
        if ((POINTS(h,1)==xval+moveVector(1)) && POINTS(h,2)==(yval+moveVector(2)))
            row=h;
        end
    end
    
    uValue=U(row);
end

end



% yy=CheckObsall(xval,yval,ObsSet,MaxVector,[1,0]);
% 
% % can't go east if at Xmax or if in cell above sink 'because it will go to sink cell'
% % if ((xval==Xmax) || (xval==xSink-1 && yval==ySink))
% if yy==1
%     uValue=U(k);
% else
%     for h=1:size(POINTS,1)
%         if (POINTS(h,1)==xval+1 && POINTS(h,2)==yval)
%             row=h;
%         end
%     end
%     uValue=U(row);
% end
% 
% end


