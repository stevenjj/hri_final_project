function StateVector = GetStateVector(xpos,ypos,MaxVector,ObsSet,HumanSet,GoalVector,POINTS)
% Input  : position (x,y)
% Output : State vector s = 9 X 1 = ( s1,s2,...,s8, NearestGoalDirection, NearestHumandirection, MDPsolution)
% xminmax : xminmax(1) = minimum x
%           xminmax(2) = maximum x
% POINTS, Obsset

StateDim=11;


StateVector=zeros(StateDim,1);
nFreeSpace=0;
MDPVotingVector=[];

ActionCC=zeros(8,2);
ActionCC(1,:)=[1,0];
ActionCC(2,:)=[1,1];
ActionCC(3,:)=[0,1];
ActionCC(4,:)=[-1,1];
ActionCC(5,:)=[-1,0];
ActionCC(6,:)=[-1,-1];
ActionCC(7,:)=[0,-1];
ActionCC(8,:)=[1,-1];

%% Current MDP sol
k=find(POINTS(:,1)==xpos&POINTS(:,2)==ypos);
cur_mdpsol=POINTS(k,7);

%% Obstacle/Grid map enviornment staevector
for i=1:8
    StateVector(i)=CheckObs(xpos,ypos,ObsSet,HumanSet,MaxVector,ActionCC(i,:));

    %% Save MDP solution in state feature vector
    if StateVector(i)==0
       nFreeSpace= nFreeSpace+1;
       xpos_next=xpos+ActionCC(i,1);
       ypos_next=ypos+ActionCC(i,2);
       m=find(POINTS(:,1)==xpos_next &POINTS(:,2)==ypos_next);
       mdpsol=POINTS(m,7);
       MDPVotingVector=[MDPVotingVector;mdpsol];
    end
end
StateVector(9)=GestGoalStateIdx(xpos,ypos,GoalVector(1),GoalVector(2));

%% Get Nearest Human Index
NHIdx=GetNNHIdx(xpos,ypos,HumanSet);
StateVector(10)=NHIdx;

%% Get Majority Voting of MDP solution 
VotingResult = mode(MDPVotingVector);

MoveVotingResult=GetMoveVector(VotingResult);
if (CheckObs(xpos,ypos,ObsSet,HumanSet,MaxVector,MoveVotingResult))
    VotingResult=cur_mdpsol;
end

if VotingResult
    StateVector(11)=VotingResult;
else
    StateVector(11)=0;
end

end