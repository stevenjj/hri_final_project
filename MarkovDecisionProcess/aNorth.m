function uValue = aNorth(xval,yval,U,POINTS,k,ObsSet,MaxVector)

moveVector=[0, 1];
yy=CheckObsall(xval,yval,ObsSet,MaxVector,moveVector);

% can't go south if at Y=1 or if in cell above sink 'because it will go to sink cell'
% if ((yval==1) || (xval==xSink && yval==ySink+1))
if yy==1
    uValue=U(k);
else
    for h=1:size(POINTS,1)
        if ((POINTS(h,1)==xval+moveVector(1)) && POINTS(h,2)==(yval+moveVector(2)))
            row=h;
        end
    end
    
    uValue=U(row);
end

end


% yy=CheckObsall(xval,yval,ObsSet,MaxVector,[0,1]);
% 
% % can't go north if at YMax or if in cell below sink 'because it will go to sink cell'
% % if ((yval==Ymax) || (xval==xSink && yval==ySink-1))
%  if (yy==1)
%     uValue=U(k);
% else
%     for h=1:size(POINTS,1)
%         if (POINTS(h,1)==xval && POINTS(h,2)==yval+1)
%             row=h;
%         end
%     end
%     
%      uValue=U(row);
%     
% end
% 
% 
% end
% 
