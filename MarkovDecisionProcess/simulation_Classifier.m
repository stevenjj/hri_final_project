
%%Simulation for navigation
MAX_X=8;
MAX_Y=8;
MaxVector=[MAX_X;MAX_Y];

StateVector=zeros(9,1);
%This array stores the coordinates of the R and the 
%Objects in each coordinate

% Obtain Obstacle, Target and Robot Position
% Initialize the R
% Obstacle=-1,Target = 0,Robot=1,Space=2
figure();
axis([0 MAX_X 0 MAX_Y])   
%set(gca,'color', [1 1 0]);
%set(gca,'color','b');

set(gca,'XTick',[1:MAX_X])  
set(gca,'YTick',[1:MAX_Y]) 

grid on;
hold on;

% Determine Terminals, Obstacles, Start Locations

%Terminals
%Winning point
xWin=8;     %X Coordinate of the Winning point
yWin=8;     %Y Coordinate of the Winning point
GoalVector=[xWin;yWin];
R(xWin,yWin)=100;%Reward = 100
Pi(xWin,yWin)='+';%Policy
plot(xWin-.5,yWin-.5,'gd','MarkerSize',10);
text(xWin-.9,yWin-.3,'Winning +100','Color','g')

%Loss point
xLos=8;%X Coordinate of the Loss point
yLos=7;%Y Coordinate of the Loss point
R(xLos,yLos)=-100;%Reward = -100
Pi(xLos,yLos)='-';%Policy
plot(xLos-.5,yLos-.5,'rd','MarkerSize',10);
text(xLos-.8,yLos-.4,'Loss -100','Color','r')

%% Setting Obstacles
Num_Obs=5;
ObstacleSet=cell(Num_Obs,1);
ObstacleSet{1}=[3;2];
ObstacleSet{2}=[3;3];
ObstacleSet{3}=[3;4];
ObstacleSet{4}=[5;5];
ObstacleSet{5}=[6;5];

xStart=1;%X Coordinate of the Start
yStart=1;%Y Coordinate of the Start
plot(xStart-.5,yStart-.5,'bo','MarkerSize',10);
text(xStart-.6,yStart-.4,'Start','Color','k')


curPos_x=xStart;
curPos_y=yStart;
iter=0;

path=[xStart,yStart];
 while iter<5;
    Cur_State =GestStateVector(curPos_x,curPos_y,MaxVector,ObstacleSet,GoalVector,1);
    Actions=getPolicy(Cur_State(1:9));
    
    curPos_x=curPos_x+Actions(1);
    curPos_y=curPos_y+Actions(2);
    
    path=[path;[curPos_x,curPos_y]];
    iter=iter+1;
 end

% for i=1:MAX_X
%     for j=1:MAX_Y
%         m=m+1;
%         k=find(POINTS(:,1)==i & POINTS(:,2)==j);
% %         text(i-.7,j-.2,num2str(U(k)),'color','k')
% %         text(i-.5,j-.8,charPi(k),'color','b')
%     end
% end

