function varargout = Navigation_GUI(varargin)
% NAVIGATION_GUI MATLAB code for Navigation_GUI.fig
%      NAVIGATION_GUI, by itself, creates a new NAVIGATION_GUI or raises the existing
%      singleton*.
%
%      H = NAVIGATION_GUI returns the handle to a new NAVIGATION_GUI or the handle to
%      the existing singleton*.
%
%      NAVIGATION_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NAVIGATION_GUI.M with the given input arguments.
%
%      NAVIGATION_GUI('Property','Value',...) creates a new NAVIGATION_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Navigation_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Navigation_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Navigatio_GUI

% Last Modified by GUIDE v2.5 11-Dec-2016 16:44:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Navigation_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @Navigation_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Navigation_GUI is made visible.
function Navigation_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Navigation_GUI (see VARARGIN)

% Choose default command line output for Navigation_GUI
handles.output = hObject;

global Main_Param

Main_Param.Threshold =0.003;

Main_Param.MAX_X=50;
Main_Param.MAX_Y=20;
Main_Param.MaxVector=[Main_Param.MAX_X;Main_Param.MAX_Y];
Main_Param.Ra = -1; 
Main_Param.StateVector=zeros(9,1);
Main_Param.TrainingDataSet=[];
Main_Param.TrainingDataSetState=[];
Main_Param.saveNum=0;
Main_Param.saveiter=0;
Main_Param.boolLearned=0;
Main_Param.Num_Class=4;
Main_Param.Num_good=0;
Main_Param.Num_bad=0;
Main_Param.Num_trj=0;
Main_Param.RH=0;
Main_Param.VMD=0;
Main_Param.PBH=0;
Main_Param.MDP_RH=0;
Main_Param.MDP_VMD=0;
Main_Param.MDP_PBH=0;
Main_Param.x=0;
Main_Param.occupacymap=[];
%This array stores the coordinates of the R and the 
%Objects in each coordinate
% Main_Param.R=Main_Param.Ra*ones(Main_Param.MAX_X,Main_Param.MAX_Y);
% Main_Param.Pi=ones(Main_Param.MAX_X,Main_Param.MAX_Y);
% Main_Param.PiNums=zeros(Main_Param.MAX_X,Main_Param.MAX_Y);
% Main_Param.boolplot=0;
% 
% %% Map setting
% axes(handles.Mainfig);
% axis([0 Main_Param.MAX_X 0 Main_Param.MAX_Y])   
% set(gca,'XTick',[1:Main_Param.MAX_X])  
% set(gca,'YTick',[1:Main_Param.MAX_Y]) 
% grid on;
% hold on;
% 
% %% Setting Startpoint 
% Main_Param.xStart=1;%X Coordinate of the Start
% Main_Param.yStart=9;%Y Coordinate of the Start
% % plot(Main_Param.xStart-.5,Main_Param.yStart-.5,'bo','MarkerSize',10); 
% text(Main_Param.xStart-.65,Main_Param.yStart-.4,'Start','Color','k')
% Main_Param.CurPos_X=Main_Param.xStart;
% Main_Param.CurPos_Y=Main_Param.yStart;
% % Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'g*','MarkerSize',12,'LineWidth',2);
% 
% 
% %% Determine Terminals, Obstacles, Start Locations
% %Winning point
% Main_Param.xWin=12;     %X Coordinate of the Winning point
% Main_Param.yWin=1;     %Y Coordinate of the Winning point
% Main_Param.GoalVector=[Main_Param.xWin;Main_Param.yWin];
% Main_Param.R(Main_Param.xWin,Main_Param.yWin)=100;%Reward = 100
% Main_Param.Pi(Main_Param.xWin,Main_Param.yWin)='+';%Policy
% % plot(Main_Param.xWin-.5,Main_Param.yWin-.5,'gd','MarkerSize',10);
% text(Main_Param.xWin-.9,Main_Param.yWin-.3,'Goal','Color','k')

% 
% 
% %Loss point
% % Main_Param.xLos=6;      %X Coordinate of the Loss point
% % Main_Param.yLos=1;      %Y Coordinate of the Loss point
% % Main_Param.R(Main_Param.xLos,Main_Param.yLos)=-100;%Reward = -100
% % Main_Param.Pi(Main_Param.xLos,Main_Param.yLos)='-';%Policy
% % plot(Main_Param.xLos-.5,Main_Param.yLos-.5,'rd','MarkerSize',10);
% % text(Main_Param.xLos-.8,Main_Param.yLos-.4,'Loss -100','Color','r')
% 
% %% Setting Obstacles
% Main_Param.Num_Obs=32;
% Main_Param.ObstacleSet=cell(Main_Param.Num_Obs,1);
% cons=8;
% for j=1:cons
% Main_Param.ObstacleSet{j}=[j+2;10];
% Main_Param.ObstacleSet{j+cons}=[j+2;9];
% Main_Param.ObstacleSet{j+2*cons}=[j+2;1];
% Main_Param.ObstacleSet{j+3*cons}=[j+2;2];
% end
% 
% % cons=5;
% % for n=1:5
% %     Main_Param.ObstacleSet{24+n}=[1;n];
% %     Main_Param.ObstacleSet{24+cons+n}=[2;n];
% %     Main_Param.ObstacleSet{24+2*cons+n}=[3;n];
% %     Main_Param.ObstacleSet{24+3*cons+n}=[4;n];
% %     Main_Param.ObstacleSet{24+4*cons+n}=[5;n];
% %     Main_Param.ObstacleSet{24+5*cons+n}=[6;n];
% %     Main_Param.ObstacleSet{24+6*cons+n}=[7;n];
% %     Main_Param.ObstacleSet{24+7*cons+n}=[8;n];
% % end
% % 
% % cons=10;
% % for n=1:cons
% %     Main_Param.ObstacleSet{64+n}=[11;n];
% %     Main_Param.ObstacleSet{64+cons+n}=[12;n];
% % end
% 
% 
% 
% %% Setting Humans
% Main_Param.Num_Human=4;
% Main_Param.HumanSet=cell(Main_Param.Num_Human,1);
% % Main_Param.HumanSet{1}=[4;4];
% % Main_Param.HumanSet{1}=[3;7];
% Main_Param.HumanSet{1}=[4;7];
% Main_Param.HumanSet{2}=[5;7];
% Main_Param.HumanSet{3}=[5;5];
% Main_Param.HumanSet{4}=[6;5];
% %  Main_Param.HumanSet{6}=[3;2];
% %  Main_Param.HumanSet{7}=[4;2];
% %  Main_Param.HumanSet{8}=[2;2];
% % Main_Param.HumanSet{6}=[7;2];
% % Main_Param.HumanSet{4}=[2;4];
% % Main_Param.HumanSet{5}=[2;3];
% % Main_Param.HumanSet{5}=[7;2];
% % Main_Param.HumanSet{6}=[7;4];
% % Main_Param.HumanSet{7}=[4;1];
% 
% 
% %% Setting for Obstacle reward and Drawing
% for k=1:Main_Param.Num_Obs
%     Main_Param.R(Main_Param.ObstacleSet{k}(1),Main_Param.ObstacleSet{k}(2))=0;                             %Reward = 0
%     Main_Param.Pi(Main_Param.ObstacleSet{k}(1),Main_Param.ObstacleSet{k}(2))='#';                          %Policy
% %     plot(Main_Param.ObstacleSet{k}(1)-.5,Main_Param.ObstacleSet{k}(2)-.5,'ro','MarkerSize',11);
%     text(Main_Param.ObstacleSet{k}(1)-.8,Main_Param.ObstacleSet{k}(2)-.4,'O','Color','r')
% end
% 
% Color=hsv(6);
% 
% %% Setting Humans reward and Drawing 
% for k=1:Main_Param.Num_Human
%    
%       Main_Param.R(Main_Param.HumanSet{k}(1),Main_Param.HumanSet{k}(2))=0;                             %Reward = 0
%       Main_Param.Pi(Main_Param.HumanSet{k}(1),Main_Param.HumanSet{k}(2))='#';
% %       plot(Main_Param.HumanSet{k}(1)-.5,Main_Param.HumanSet{k}(2)-.5,'Color',Color(5,:),'Marker','^','MarkerSize',11);
%       text(Main_Param.HumanSet{k}(1)-.8,Main_Param.HumanSet{k}(2)-.4,'H','Color',Color(5,:))
% end




% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Navigation_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Navigation_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton_Bad.
function pushbutton_Bad_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Bad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param

Main_Param.Num_bad=Main_Param.Num_bad+1;
set(handles.Num_bad,'String',Main_Param.Num_bad);

Main_Param.CurPos_X=Main_Param.CurPos_X-Main_Param.CurActionPolicy(1);
Main_Param.CurPos_Y=Main_Param.CurPos_Y-Main_Param.CurActionPolicy(2);

set(handles.cur_X,'String',Main_Param.CurPos_X);
set(handles.cur_Y,'String',Main_Param.CurPos_Y);


Main_Param.CurActionPolicy=[0 0];
 set(handles.Act_x,'String',0);
 set(handles.Act_y,'String',0);
 delete(Main_Param.figh);
 Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'g*','MarkerSize',12,'LineWidth',2);



% Main_Param.CurStateVector=TestStateVector;
%%User has to give Action


% Main_Param.CurActionPolicy=ActionPolicy;
% Main_Param.fighR=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'r*','MarkerSize',15,'LineWidth',2);


% --- Executes on button press in pushbutton_Good.
function pushbutton_Good_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Good (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param

Main_Param.Num_good=Main_Param.Num_good+1;
set(handles.Num_good,'String',Main_Param.Num_good);


delete(Main_Param.figh);
Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'g*','MarkerSize',15,'LineWidth',2);
set(handles.cur_X,'String',Main_Param.CurPos_X);
set(handles.cur_Y,'String',Main_Param.CurPos_Y);
SaveDataSet_Callback(hObject, eventdata, handles);


% --- Executes on button press in SaveDataSet.
function SaveDataSet_Callback(hObject, eventdata, handles)
% hObject    handle to SaveDataSet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param 
Main_Param.TrainingDataSet=[Main_Param.TrainingDataSet; Main_Param.CurStateVector];
Main_Param.TrainingDataSetState=[Main_Param.TrainingDataSetState;Main_Param.CurActionPolicyidx];
% Main_Param.saveNum=Main_Param.saveNum+1;
 set(handles.SaveNum,'String',Main_Param.saveNum);
%  LearningData (hObject, eventdata, handles);

 
function LearningData (hObject, eventdata, handles)
global Main_Param 
NeuronNum=10;
Sigmas=1.5;
% if Main_Param.saveNum>30
    [Beta, MU, SIGMA] = ELM_RBF_Kernels(Main_Param.TrainingDataSet,Main_Param.TrainingDataSetState, NeuronNum,Sigmas);
    Main_Param.ELM_param.Beta=Beta;
    Main_Param.ELM_param.MU=MU;
    Main_Param.ELM_param.SIGMA=SIGMA;
    Main_Param.boolLearned=1;
    
    Main_Param.saveNum=size(Main_Param.TrainingDataSet,1);
    set(handles.SaveNum,'String',Main_Param.saveNum);
%     
%     Main_Param.SVM_Param.models=SVM_Learning(Main_Param.TrainingDataSet,Main_Param.TrainingDataSetState,Sigmas );
%     Main_Param.boolLearned=2;
%     
%     
%     
%    u=unique(Main_Param.TrainingDataSetState);
%     numClasses=length(u);
%     
%      Main_Param.SVM_param.models(k) = svmtrain(TrainingSet,G1vAll,'kernel_function','rbf','rbf_sigma',sigma);
    
    
    
% end

% --- Executes on button press in Next.
function Next_Callback(hObject, eventdata, handles)
% hObject    handle to Next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param

% if Main_Param.fighR
if Main_Param.boolplot
%   delete(Main_Param.figh);
 end

k=find(Main_Param.POINTS(:,1)==Main_Param.CurPos_X & Main_Param.POINTS(:,2)==Main_Param.CurPos_Y);
TestStateVector = GetStateVector(Main_Param.CurPos_X, Main_Param.CurPos_Y,Main_Param.MaxVector,Main_Param.ObstacleSet,Main_Param.HumanSet,Main_Param.GoalVector,Main_Param.POINTS);
[ActionPolicy confidence] = getPolicy(TestStateVector,Main_Param.TrainingDataSet,Main_Param.TrainingDataSetState,Main_Param.boolLearned);

 set(handles.Act_x,'String',ActionPolicy(1));
 set(handles.Act_y,'String',ActionPolicy(2));
 set(handles.Confidence,'String',confidence);
 
Main_Param.CurPos_X=Main_Param.CurPos_X+ActionPolicy(1);
Main_Param.CurPos_Y=Main_Param.CurPos_Y+ActionPolicy(2);
 
%% Plot possible action policy candidate
Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'b*','MarkerSize',12,'LineWidth',2);
Main_Param.boolplot=1;
Main_Param.CurStateVector=TestStateVector';
Main_Param.CurActionPolicy=ActionPolicy;
Main_Param.CurActionPolicyidx=getPolicyidx(ActionPolicy);
% Main_Param.fighR=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'g*','MarkerSize',15,'LineWidth',2);
 
%%Display 
set(handles.cur_X,'String',Main_Param.CurPos_X);
set(handles.cur_Y,'String',Main_Param.CurPos_Y);
  
 guidata(hObject, handles);

% --- Executes on button press in Up_push.
function Up_push_Callback(hObject, eventdata, handles)
% hObject    handle to Up_push (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.CurActionPolicy=[0 1];
Main_Param.CurActionPolicyidx=2;

Main_Param.CurPos_X=Main_Param.CurPos_X+Main_Param.CurActionPolicy(1);
Main_Param.CurPos_Y=Main_Param.CurPos_Y+Main_Param.CurActionPolicy(2);
Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'r*','MarkerSize',12,'LineWidth',2);
 set(handles.Act_x,'String',0);
 set(handles.Act_y,'String',1);

% --- Executes on button press in Left_push.
function Left_push_Callback(hObject, eventdata, handles)
% hObject    handle to Left_push (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.CurActionPolicy=[-1 0];
Main_Param.CurActionPolicyidx=3;

Main_Param.CurPos_X=Main_Param.CurPos_X+Main_Param.CurActionPolicy(1);
Main_Param.CurPos_Y=Main_Param.CurPos_Y+Main_Param.CurActionPolicy(2);
Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'r*','MarkerSize',15,'LineWidth',2);
set(handles.Act_x,'String',-1);
 set(handles.Act_y,'String',0);


% --- Executes on button press in Right_push.
function Right_push_Callback(hObject, eventdata, handles)
% hObject    handle to Right_push (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.CurActionPolicy=[1 0];
Main_Param.CurActionPolicyidx=1;

Main_Param.CurPos_X=Main_Param.CurPos_X+Main_Param.CurActionPolicy(1);
Main_Param.CurPos_Y=Main_Param.CurPos_Y+Main_Param.CurActionPolicy(2);
Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'r*','MarkerSize',15,'LineWidth',2);
set(handles.Act_x,'String',1);
 set(handles.Act_y,'String',0);

% --- Executes on button press in Down_push.
function Down_push_Callback(hObject, eventdata, handles)
% hObject    handle to Down_push (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.CurActionPolicy=[0 -1];
Main_Param.CurActionPolicyidx=4;

Main_Param.CurPos_X=Main_Param.CurPos_X+Main_Param.CurActionPolicy(1);
Main_Param.CurPos_Y=Main_Param.CurPos_Y+Main_Param.CurActionPolicy(2);
Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'r*','MarkerSize',15,'LineWidth',2);


set(handles.Act_x,'String',0);
 set(handles.Act_y,'String',-1);



% --- Executes on button press in MDP_Solve.
function MDP_Solve_Callback(hObject, eventdata, handles)
% hObject    handle to MDP_Solve (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
N = 100; %max number of iterations of Value Iteration

global Main_Param

deltaMin = 1e-8; %convergence criterion for iteration
delta = 0;

POINTS_COUNT= Main_Param.MAX_X * Main_Param.MAX_Y;
Main_Param.POINTS=zeros(POINTS_COUNT,7);

%Put all the points in list with their rewards and initial UP and U
%LIST |X val |Y val |Reward |Uprime |Utility |Policy

k=1;%Dummy counter
for i=1:Main_Param.MAX_X
    for j=1:Main_Param.MAX_Y
          Main_Param.POINTS(k,1)=i;
          Main_Param.POINTS(k,2)=j;
          Main_Param.POINTS(k,3)=Main_Param.R(i,j);
          Main_Param.POINTS(k,4)=0;
          Main_Param.POINTS(k,5)=0;
          Main_Param.POINTS(k,6)=Main_Param.Pi(i,j);
          Main_Param.POINTS(k,7)=Main_Param.PiNums(i,j);  
          k=k+1;
    end
end

Main_Param.R=Main_Param.POINTS(:,3);%instantaneous reward
Main_Param.Up=Main_Param.POINTS(:,4);%UPrime, used in updates
Main_Param.U=Main_Param.POINTS(:,5);%long-term utility
Main_Param.Pi=Main_Param.POINTS(:,6);%policy (char)
Main_Param.PiNum=Main_Param.POINTS(:,7); %policy(int)

n=0;

%while((delta < deltaMin) && (n < N))
while 1
    
    Main_Param.POINTS(:,5)=Main_Param.POINTS(:,4);%U=Up
    Main_Param.U=Main_Param.Up;
  
    n=n+1;

    delta = 0;
    
    for i=1:Main_Param.MAX_X
        for j=1:Main_Param.MAX_Y
            %obtain utilityprime function : [upPi]=[Up, Pi, PiNum];
            upPi=updateUPrimePi(i,j,Main_Param.POINTS,Main_Param.Ra,Main_Param.MaxVector);  
            Main_Param.Up=upPi(:,1);
            Main_Param.Pi=upPi(:,2);
            Main_Param.PiNum=upPi(:,3);
            Main_Param.POINTS(:,4)=Main_Param.Up;
            Main_Param.POINTS(:,6)=Main_Param.Pi;
            Main_Param.POINTS(:,7)=Main_Param.PiNum;
            k=find(Main_Param.POINTS(:,1)==i & Main_Param.POINTS(:,2)==j);
            %k=k(1);

            diff=abs(Main_Param.Up(k)-Main_Param.U(k));

            if diff > delta
                delta = diff;
            end

        end
    end
    
    if (delta < deltaMin || n > N)
        break;
    end
    
end

Main_Param.charPi=char(Main_Param.Pi);
% StateVectorcell=cell(MAX_X,MAX_Y);
% StateVectorSet=zeros(MAX_X*MAX_Y,9);
% StateVectorSetState=zeros(MAX_X*MAX_Y,1);
m=0;
for i=1:Main_Param.MAX_X
    for j=1:Main_Param.MAX_Y
        m=m+1;
        k=find(Main_Param.POINTS(:,1)==i & Main_Param.POINTS(:,2)==j);
%          text(i-.7,j-.2,num2str(Main_Param.U(k)),'color','k')
%          text(i-.5,j-.8,Main_Param.charPi(k),'color','b')
    end
end


path=[];

i=1;
path(1,1)=Main_Param.xStart;
path(1,2)=Main_Param.yStart;

newX=Main_Param.xStart;
newY=Main_Param.yStart;

while 1
k=find(Main_Param.POINTS(:,1)==newX & Main_Param.POINTS(:,2)==newY);
if (Main_Param.charPi(k)~='+' && Main_Param.charPi(k)~='-' && Main_Param.charPi(k)~='#')
    i=i+1;
    
         switch Main_Param.charPi(k)
           case 'E'
                   path(i,1)=newX+1;
                   path(i,2)=newY;        
                   newX=newX+1;
                   newY=newY;
            case 'R'
                   path(i,1)=newX+1;
                   path(i,2)=newY+1;        
                   newX=newX+1;
                   newY=newY+1;
            case 'N'
               path(i,1)=newX;
                   path(i,2)=newY+1;        
                   newX=newX;
                   newY=newY+1;
             
           case 'Q'
               path(i,1)=newX-1;
                   path(i,2)=newY+1;      
                   newX=newX-1;
                   newY=newY+1;
               
      
           case 'W'
               path(i,1)=newX-1;
                   path(i,2)=newY;        
                   newX=newX-1;
                   newY=newY;
         
           case 'Z'
               path(i,1)=newX-1;
                   path(i,2)=newY-1;        
                   newX=newX-1;
                   newY=newY-1;
           
           case 'S'
               path(i,1)=newX;
                   path(i,2)=newY-1;        
                   newX=newX;
                   newY=newY-1;
          
           case 'C'
               path(i,1)=newX+1;
                   path(i,2)=newY-1;        
                   newX=newX+1;
                   newY=newY-1;
             
       end
    
    
%     if Main_Param.charPi(k)=='N'        
%         path(i,1)=newX;
%         path(i,2)=newY+1;        
%         newX=newX;
%         newY=newY+1;
%     else if Main_Param.charPi(k)=='S'
%             path(i,1)=newX;
%             path(i,2)=newY-1;
%             newX=newX;
%             newY=newY-1;
%         else if Main_Param.charPi(k)=='W'
%                 path(i,1)=newX-1;
%                 path(i,2)=newY;    
%                 newX=newX-1;
%                 newY=newY;
%             else %'E'
%                 path(i,1)=newX+1;
%                 path(i,2)=newY;
%                 newX=newX+1;
%                 newY=newY;
%             end
%             
%         end
%     end
else
    break;
end

end

i=size(path,1);
Main_Param.Num_trj=i;
set(handles.Num_trj,'String',Main_Param.Num_trj);

 %Plot the Path!
 p=plot(path(i,1)-.5,path(i,2)-.5,'ro','LineWidth',2);

%% Draw simulation path
for i=1:size(path,1)
%   pause(.25);
  set(p,'XData',path(i,1)-.5,'YData',path(i,2)-.5);
%  drawnow ;
 end;
plot(path(:,1)-.5,path(:,2)-.5,'LineWidth',2);
% end


% end



function cur_X_Callback(hObject, eventdata, handles)
% hObject    handle to cur_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cur_X as text
%        str2double(get(hObject,'String')) returns contents of cur_X as a double


% --- Executes during object creation, after setting all properties.
function cur_X_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cur_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cur_Y_Callback(hObject, eventdata, handles)
% hObject    handle to cur_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cur_Y as text
%        str2double(get(hObject,'String')) returns contents of cur_Y as a double


% --- Executes during object creation, after setting all properties.
function cur_Y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cur_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Confidence_Callback(hObject, eventdata, handles)
% hObject    handle to Confidence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Confidence as text
%        str2double(get(hObject,'String')) returns contents of Confidence as a double


% --- Executes during object creation, after setting all properties.
function Confidence_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Confidence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Act_x_Callback(hObject, eventdata, handles)
% hObject    handle to Act_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Act_x as text
%        str2double(get(hObject,'String')) returns contents of Act_x as a double


% --- Executes during object creation, after setting all properties.
function Act_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Act_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Act_y_Callback(hObject, eventdata, handles)
% hObject    handle to Act_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Act_y as text
%        str2double(get(hObject,'String')) returns contents of Act_y as a double


% --- Executes during object creation, after setting all properties.
function Act_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Act_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SaveNum_Callback(hObject, eventdata, handles)
% hObject    handle to SaveNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SaveNum as text
%        str2double(get(hObject,'String')) returns contents of SaveNum as a double


% --- Executes during object creation, after setting all properties.
function SaveNum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SaveNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in SaveMatfile.
function SaveMatfile_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMatfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Main_Param
Main_Param.saveiter=Main_Param.saveiter+1;
DataSet=Main_Param.TrainingDataSet;
DataSetState=Main_Param.TrainingDataSetState;

[row col]= find( DataSetState >0);
DataSet=DataSet(row,:);
DataSetState=DataSetState(row,:);

Main_Param.saveNum=size(DataSet,1);

curtime=clock;
FileName=strcat('TrVectorSet_',num2str(Main_Param.saveiter),'.mat');
save(FileName, 'DataSet', 'DataSet');
FileName=strcat('TrVectorSet_',num2str(curtime(4)),'_',num2str(curtime(5)),'.mat');
save(FileName, 'DataSet', 'DataSet');
FileName=strcat('TrVectorSetState_',num2str(Main_Param.saveiter),'.mat');
save(FileName, 'DataSetState', 'DataSetState');
FileName=strcat('TrVectorSetState_',num2str(curtime(4)),'_',num2str(curtime(5)),'.mat');
save(FileName, 'DataSetState', 'DataSetState');



% --- Executes on button press in LoadMatfile.
function LoadMatfile_Callback(hObject, eventdata, handles)
% hObject    handle to LoadMatfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.loaditer=1;
FileName=strcat('TrVectorSet_',num2str(Main_Param.loaditer),'.mat');
load(FileName);

TempDataSet=[];
% load('TempDataSet.mat');

Main_Param.TrainingDataSet=[Main_Param.TrainingDataSet;[DataSet;TempDataSet]];
FileName=strcat('TrVectorSetState_',num2str(Main_Param.loaditer),'.mat');
load(FileName);

TempDataSetState=[];
% load('TempDataSetState.mat');


Main_Param.TrainingDataSetState=[Main_Param.TrainingDataSetState;[DataSetState;TempDataSetState]];
%  [Main_Param.TrainingDataSet Main_Param.TrainingDataSetState]=FilterTrainingData( Main_Param.TrainingDataSet, Main_Param.TrainingDataSetState);
LearningData(hObject, eventdata, handles);
 %  LearningData
 


% --- Executes on button press in Simulation_button.
function Simulation_button_Callback(hObject, eventdata, handles)
% hObject    handle to Simulation_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



global Main_Param
while Main_Param.CurPos_X ~= Main_Param.xWin || Main_Param.CurPos_Y~=Main_Param.yWin
    
    Next_Callback(hObject, eventdata, handles)

end



function Num_good_Callback(hObject, eventdata, handles)
% hObject    handle to Num_good (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Num_good as text
%        str2double(get(hObject,'String')) returns contents of Num_good as a double


% --- Executes during object creation, after setting all properties.
function Num_good_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Num_good (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Num_bad_Callback(hObject, eventdata, handles)
% hObject    handle to Num_bad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Num_bad as text
%        str2double(get(hObject,'String')) returns contents of Num_bad as a double


% --- Executes during object creation, after setting all properties.
function Num_bad_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Num_bad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Num_trj_Callback(hObject, eventdata, handles)
% hObject    handle to Num_trj (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Num_trj as text
%        str2double(get(hObject,'String')) returns contents of Num_trj as a double


% --- Executes during object creation, after setting all properties.
function Num_trj_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Num_trj (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MDP_S1_Callback(hObject, eventdata, handles)
% hObject    handle to MDP_S1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.MDP_RH=str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of MDP_S1 as text
%        str2double(get(hObject,'String')) returns contents of MDP_S1 as a double


% --- Executes during object creation, after setting all properties.
function MDP_S1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MDP_S1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function CBA_S1_Callback(hObject, eventdata, handles)
% hObject    handle to CBA_S1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.RH=str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of CBA_S1 as text
%        str2double(get(hObject,'String')) returns contents of CBA_S1 as a double


% --- Executes during object creation, after setting all properties.
function CBA_S1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CBA_S1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MDP_S2_Callback(hObject, eventdata, handles)
% hObject    handle to MDP_S2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.MDP_VMD=str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of MDP_S2 as text
%        str2double(get(hObject,'String')) returns contents of MDP_S2 as a double


% --- Executes during object creation, after setting all properties.
function MDP_S2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MDP_S2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function CBA_S2_Callback(hObject, eventdata, handles)
% hObject    handle to CBA_S2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.VMD=str2double(get(hObject,'String'));


% Hints: get(hObject,'String') returns contents of CBA_S2 as text
%        str2double(get(hObject,'String')) returns contents of CBA_S2 as a double


% --- Executes during object creation, after setting all properties.
function CBA_S2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CBA_S2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MDP_S3_Callback(hObject, eventdata, handles)
% hObject    handle to MDP_S3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.MDP_PBH=str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of MDP_S3 as text
%        str2double(get(hObject,'String')) returns contents of MDP_S3 as a double


% --- Executes during object creation, after setting all properties.
function MDP_S3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MDP_S3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function CBA_S3_Callback(hObject, eventdata, handles)
% hObject    handle to CBA_S3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.PBH=str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of CBA_S3 as text
%        str2double(get(hObject,'String')) returns contents of CBA_S3 as a double


% --- Executes during object creation, after setting all properties.
function CBA_S3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CBA_S3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in SaveMetric_Push.
function SaveMetric_Push_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMetric_Push (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param

textFileName='mytextfile';
curtime=clock;
textFileName=strcat(textFileName,'_',num2str(curtime(4)),'_',num2str(curtime(5)),'.csv');

fID=fopen(textFileName,'w');
myformat='%s \t %g\n';             % my format

%% Learning Evaluation
D1='# of Good : ';                                % string data
fprintf(fID,myformat,D1,Main_Param.Num_good);     % write in file
D1='# of bad : ';                                  % string data
fprintf(fID,myformat,D1,Main_Param.Num_bad);     % write in file
D1='# of trajectory : ';                         % string data
fprintf(fID,myformat,D1,Main_Param.Num_trj);     % write in file
D1='# of trainingData : ';        % string data
fprintf(fID,myformat,D1,Main_Param.saveNum);     % write in file

%% Social navigation measurement
D1='# of navigation Right Hallway : ';        % string data
fprintf(fID,myformat,D1,Main_Param.RH);     % write in file
D1='# of viloating minimum Distance : ';        % string data
fprintf(fID,myformat,D1,Main_Param.VMD);     % write in file
D1='# of passing between Humans: ';        % string data
fprintf(fID,myformat,D1,Main_Param.PBH);     % write in file

%% MDP reference
D1='# of navigation Right Hallway (MDP) : ';        % string data
fprintf(fID,myformat,D1,Main_Param.MDP_RH);     % write in file
D1='# of viloating minimum Distance (MDP) : ';        % string data
 fprintf(fID,myformat,D1,Main_Param.MDP_VMD);     % write in file
D1='# of passing between Humans (MDP) : ';        % string data
fprintf(fID,myformat,D1,Main_Param.MDP_PBH);     % write in file

 
fclose(fID);                     % close file  		


function ROS_cmd_receiveCallback2(~, ~, handles)
global Main_Param
% handles = guidata(hObject);
 disp('Message received!');
% set(handles.ros_cmd,'String',3);


function exampleMKTimer2(~,~ , handles)
global  Main_Param
% MatlabPub = robotics.ros.Publisher( Main_Param.node_3,'/Matlab_cmd','matlab_msg/m_gridInfo');

A=round(10*rand(50,1));
localmsgs=rosmessage('matlab_msg/m_gridInfo');
localmsgs.ActionPolicyType=A;
send(handles.Pub,localmsgs);
% Main_ParMatlabPubam.msgsub.LatestMessage;
% Thandles=handles.handles;
%  Thandles = guidata(handles.hObject);
% set(Thandles.ros_cmd,'String',Main_Param.x);




%   send(handles.MatPub,handles.msgs);


% --- Executes on button press in GUI_ROS_CONNECT.
function GUI_ROS_CONNECT_Callback(hObject, eventdata, handles)
% hObject    handle to GUI_ROS_CONNECT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param

rosinit;
 masterHost = 'localhost';
% Main_Param.node_1 = robotics.ros.Node('node_1');
 Main_Param.node_2 = robotics.ros.Node('node_18');
 Main_Param.node_3 = robotics.ros.Node('node_119');

%  Main_Param.node_1 = robotics.ros.Node('node_1', masterHost);
%  Main_Param.node_2 = robotics.ros.Node('node_2', masterHost);
%  Main_Param.node_3 = robotics.ros.Node('node_3', masterHost);
%  Main_Param.node_4 = robotics.ros.Node('node_4', masterHost);

%Publisher
% MatlabPub = robotics.ros.Publisher( Main_Param.node_2,'/Matlab_cmd','matlab_msg/m_gridInfo');

 Main_Param.MatlabPublisher = robotics.ros.Publisher( Main_Param.node_2,'/Matlab_cmd1','matlab_msg/m_gridInfo');
 Main_Param.PosPublisher = robotics.ros.Publisher( Main_Param.node_3,'/Matlab_goal','geometry_msgs/Pose2D');
% msgs=rosmessage('matlab_msg/m_gridInfo');
% msgs.ActionPolicyType=Main_Param.POINTS(:,7);
% msgs.CellOccupancyType=Main_Param.POINTS(:,1);
% msgs.Width=10;
% msgs.Height=10;
% send(MatlabPublisher,msgs);


% Main_Param.msgsub = robotics.ros.Subscriber( Main_Param.node_1, '/CBA_cmd_int');
% Main_Param.msgsub2 = robotics.ros.Subscriber(Main_Param.node_4,'/CBA_cmd_int', @ROS_cmd_receiveCallback2);
 

% msgsub = robotics.ros.Subscriber('/CBA_cmd_int',ROS_cmd_receiveCallback2);
% timerHandles.twistPub = msgsub;
% timerHandles.MatPub=MatlabPub;
% timerHandles.msgs=rosmessage('matlab_msg/m_gridInfo');

% timerHandles.msgs.StringProperty='lol';
% A=round(10*rand(50,1));
% timerHandles.msgs.Mapgrid=A;
% timerHandles.twistPubmsg = twistPubmsg;

timerHandles.handles=handles;
 timerHandles.Pub=Main_Param.MatlabPublisher;

%  Main_Param.MKTimer = ExampleHelperROSTimer(1, {@exampleMKTimer2,timerHandles});





% --- Executes on button press in ROS_disconnect.
function ROS_disconnect_Callback(hObject, eventdata, handles)
% hObject    handle to ROS_disconnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
rosshutdown;


% --- Executes on button press in Send_MDP_push.
function Send_MDP_push_Callback(hObject, eventdata, handles)
% hObject    handle to Send_MDP_push (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global  Main_Param
% MatlabPub = robotics.ros.Publisher( Main_Param.node_3,'/Matlab_cmd','matlab_msg/m_gridInfo');

% master=robotics.ros.Core;
A=round(10*rand(50,1));
localmsgs=rosmessage('matlab_msg/m_gridInfo');
localmsgs.actionpolicy_Type=Main_Param.POINTS(:,7);
localmsgs.CellOccupancyType=Main_Param.occupacymap;
localmsgs.Width=Main_Param.MAX_X;
localmsgs.Height=Main_Param.MAX_Y;
%csvwrite('MD',Actionpolicy);


if isvalid(Main_Param.MatlabPublisher)
    send(Main_Param.MatlabPublisher,localmsgs);
end


function ros_cmd_Callback(hObject, eventdata, handles)
% hObject    handle to ros_cmd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ros_cmd as text
%        str2double(get(hObject,'String')) returns contents of ros_cmd as a double


% --- Executes during object creation, after setting all properties.
function ros_cmd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ros_cmd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ReceiveData_push.
function ReceiveData_push_Callback(hObject, eventdata, handles)
% hObject    handle to ReceiveData_push (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
laser = rossubscriber('/CBA2Matlab');
scandata = receive(laser,10);
disp(scandata);

global Main_Param
Main_Param.MAX_X=scandata.Width;
Main_Param.MAX_Y=scandata.Height;
Main_Param.occupacymap=scandata.CellOccupancyType;
Main_Param.Robot=scandata.RobotLocalCellIndices;

Main_Param.occupacymap=getConvertedMap(Main_Param.occupacymap,Main_Param.MAX_X);
Main_Param.MAX_X=150;
Main_Param.MAX_Y=60;

 scaledfactor=3;
% 
 Main_Param.occupacymap=getScaledoccuapncy(Main_Param.occupacymap2,Main_Param.MAX_X,Main_Param.MAX_Y,scaledfactor);
 Main_Param.MAX_X=150/scaledfactor;
 Main_Param.MAX_Y=60/scaledfactor;






%%
% * Use |<docid:robotics_ref.buqbyro receive>| to wait for a new message
% (the second argument is a time-out in seconds). The output |scandata|
% contains the received message data.
%%

%%
% --- Executes on button press in drawmap.
function drawmap_Callback(hObject, eventdata, handles)
% hObject    handle to drawmap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param

Main_Param.R=Main_Param.Ra*ones(Main_Param.MAX_X,Main_Param.MAX_Y);
Main_Param.Pi=ones(Main_Param.MAX_X,Main_Param.MAX_Y);
Main_Param.PiNums=zeros(Main_Param.MAX_X,Main_Param.MAX_Y);
Main_Param.boolplot=0;


Main_Param.ObstacleSet=getObstacles(Main_Param.occupacymap,Main_Param.MAX_X);
Main_Param.Num_Obs=length(Main_Param.ObstacleSet);
Main_Param.RobotSet=getRObbot(Main_Param.occupacymap,Main_Param.MAX_X);
Main_Param.Num_RobotSet=length(Main_Param.RobotSet);

%% Map setting
axes(handles.Mainfig);
axis([0 Main_Param.MAX_X 0 Main_Param.MAX_Y])   
% set(gca,'XTick',[1:Main_Param.MAX_X])  
% set(gca,'YTick',[1:Main_Param.MAX_Y]) 
grid on;
hold on;
%% Setting Startpoint 
Main_Param.xStart=Main_Param.RobotSet{1}(1);%X Coordinate of the Start
Main_Param.yStart=Main_Param.RobotSet{1}(2);%Y Coordinate of the Start
set(handles.start_X,'string',Main_Param.xStart);
set(handles.start_Y,'string',Main_Param.yStart);

% plot(Main_Param.xStart-.5,Main_Param.yStart-.5,'bo','MarkerSize',10); 
 text(Main_Param.xStart-.65,Main_Param.yStart-.4,'Start','Color','k')
Main_Param.CurPos_X=Main_Param.xStart;
Main_Param.CurPos_Y=Main_Param.yStart;
% Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'g*','MarkerSize',12,'LineWidth',2);


%% Determine Terminals, Obstacles, Start Locations
%Winning point
% Main_Param.xWin=35;                   %X Coordinate of the Winning point
% Main_Param.yWin=10;                    %Y Coordinate of the Winning point

% set(handles.Goal_X,'string',Main_Param.xWin);
% set(handles.Goal_Y,'string',Main_Param.ySWin);
% Main_Param.GoalVector=[Main_Param.xWin;Main_Param.yWin];
% Main_Param.R(Main_Param.xWin,Main_Param.yWin)=100;      %Reward = 100
% Main_Param.Pi(Main_Param.xWin,Main_Param.yWin)='+'      ;%Policy
% % plot(Main_Param.xWin-.5,Main_Param.yWin-.5,'gd','MarkerSize',10);
% text(Main_Param.xWin-.9,Main_Param.yWin-.3,'Goal','Color','k')




%% Setting for Obstacle reward and Drawing
for k=1:Main_Param.Num_Obs
    Main_Param.R(Main_Param.ObstacleSet{k}(1),Main_Param.ObstacleSet{k}(2))=0;                             %Reward = 0
    Main_Param.Pi(Main_Param.ObstacleSet{k}(1),Main_Param.ObstacleSet{k}(2))='#';                          %Policy
     plot(Main_Param.ObstacleSet{k}(1)-.5,Main_Param.ObstacleSet{k}(2)-.5,'ko','MarkerSize',2);
%    text(zz-0.2,nn-0.4,'O','Color','r');
end



% for k=1:Main_Param.Num_RobotSet
% %     Main_Param.R(Main_Param.RobotSet{k}(1),Main_Param.RobotSet{k}(2))=0;                             %Reward = 0
% %     Main_Param.Pi(Main_Param.RobotSet{k}(1),Main_Param.RobotSet{k}(2))='#';                          %Policy
%      plot(Main_Param.RobotSet{k}(1)-.5,Main_Param.RobotSet{k}(2)-.5,'bo','MarkerSize',3);
% %    text(zz-0.2,nn-0.4,'O','Color','r');
% end


Color=hsv(6);

%% Setting Humans reward and Drawing 
% for k=1:Main_Param.Num_Human
%    
%       Main_Param.R(Main_Param.HumanSet{k}(1),Main_Param.HumanSet{k}(2))=0;                             %Reward = 0
%       Main_Param.Pi(Main_Param.HumanSet{k}(1),Main_Param.HumanSet{k}(2))='#';
% %       plot(Main_Param.HumanSet{k}(1)-.5,Main_Param.HumanSet{k}(2)-.5,'Color',Color(5,:),'Marker','^','MarkerSize',11);
%       text(Main_Param.HumanSet{k}(1)-.8,Main_Param.HumanSet{k}(2)-.4,'H','Color',Color(5,:))
% end



function start_X_Callback(hObject, eventdata, handles)
% hObject    handle to start_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.xStart=str2double(get(hObject,'String'));
Main_Param.CurPos_X=Main_Param.xStart;

% Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'g*','MarkerSize',12,'LineWidth',2);
% Hints: get(hObject,'String') returns contents of start_X as text
%        str2double(get(hObject,'String')) returns contents of start_X as a double


% --- Executes during object creation, after setting all properties.
function start_X_CreateFcn(hObject, eventdata, handles)
% hObject    handle to start_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function start_Y_Callback(hObject, eventdata, handles)
% hObject    handle to start_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.yStart=str2double(get(hObject,'String'));
Main_Param.CurPos_Y=Main_Param.yStart;
% Hints: get(hObject,'String') returns contents of start_Y as text
%        str2double(get(hObject,'String')) returns contents of start_Y as a double


% --- Executes during object creation, after setting all properties.
function start_Y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to start_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Goal_X_Callback(hObject, eventdata, handles)
% hObject    handle to Goal_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.xWin=str2double(get(hObject,'String'));
% Hints: get(hObject,'String') returns contents of Goal_X as text
%        str2double(get(hObject,'String')) returns contents of Goal_X as a double


% --- Executes during object creation, after setting all properties.
function Goal_X_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Goal_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Goal_Y_Callback(hObject, eventdata, handles)
% hObject    handle to Goal_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
Main_Param.yWin=str2double(get(hObject,'String'));

% Hints: get(hObject,'String') returns contents of Goal_Y as text
%        str2double(get(hObject,'String')) returns contents of Goal_Y as a double


% --- Executes during object creation, after setting all properties.
function Goal_Y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Goal_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in MapUpdate.
function MapUpdate_Callback(hObject, eventdata, handles)
% hObject    handle to MapUpdate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Main_Param
% Main_Param.R=Main_Param.Ra*ones(Main_Param.MAX_X,Main_Param.MAX_Y);
% Main_Param.Pi=ones(Main_Param.MAX_X,Main_Param.MAX_Y);
% Main_Param.PiNums=zeros(Main_Param.MAX_X,Main_Param.MAX_Y);
% Main_Param.boolplot=0;
% 
% 
% text(Main_Param.xStart-.65,Main_Param.yStart-.4,'Start','Color','k')
% Main_Param.CurPos_X=Main_Param.xStart;
% Main_Param.CurPos_Y=Main_Param.yStart;
% % Main_Param.figh=plot(Main_Param.CurPos_X-.5,Main_Param.CurPos_Y-.5,'g*','MarkerSize',12,'LineWidth',2);
% 
% 
% %% Determine Terminals, Obstacles, Start Locations
% %Winning point

Main_Param.GoalVector=[Main_Param.xWin;Main_Param.yWin];
Main_Param.R(Main_Param.xWin,Main_Param.yWin)=100;      %Reward = 100
Main_Param.Pi(Main_Param.xWin,Main_Param.yWin)='+'      ;%Policy
% plot(Main_Param.xWin-.5,Main_Param.yWin-.5,'gd','MarkerSize',10);
text(Main_Param.xWin-.9,Main_Param.yWin-.3,'Goal','Color','k')


% --- Executes on button press in SendGoalPos.
function SendGoalPos_Callback(hObject, eventdata, handles)
% hObject    handle to SendGoalPos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global  Main_Param
% MatlabPub = robotics.ros.Publisher( Main_Param.node_3,'/Matlab_cmd','matlab_msg/m_gridInfo');

localmsgs=rosmessage('geometry_msgs/Pose2D');
localmsgs.X=Main_Param.xWin;
localmsgs.Y=Main_Param.yWin;
localmsgs.Theta=0;

if isvalid(Main_Param.PosPublisher)
    send(Main_Param.PosPublisher,localmsgs);
    disp('send Goal');
end
