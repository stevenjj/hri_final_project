function [Ratio, Result, Previous_Result] = SLFNs_RBF_Kernels(X_feature, Trans_Matrix, Beta, MU, SIGMA, Previous_Result)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% First Code was written by HanJin LEE at 2012. 7. 31
% with MATLAB 7.6.0 (R2008a) Version
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if size(X_feature,1) > size(X_feature,2),
    X_feature = X_feature';                 % Change dimension [#Channel #Datum] to [#Datum #Channel]
end

X_feature = Trans_Matrix'*X_feature';

if sum(X_feature) ~= 0,
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Make H %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SIGMA_Length = length(SIGMA);
H = zeros(size(X_feature,1),SIGMA_Length);
for K = 1:SIGMA_Length,
%     Norm_Coeff = norm(X_feature-ones(size(X_feature,1),1)*MU(K,:));
    Norm_Coeff = sqrt( sum((X_feature'-MU(K,:)).^2,2) );
%       Norm_Coeff = getTheta(X_feature',MU(K,:));
    H(:,K) = exp(-1*(1/SIGMA(K)).*Norm_Coeff);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%% Fine Max Value %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T = H*Beta;
[C, Result] = max(T(1,:));
[D, IndexSet]=sort(T(1,:),'descend');
Ratio = C/sum(abs(T(1,:)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%% Check Classification Ration %%%%%%%%%%%%%%%%%%%%%%
% if Ratio < 0.4,
%     Result = Previous_Result;
% else
%     Previous_Result = Result;
% end
if Ratio < 0.2,
    Result = Previous_Result;
elseif Ratio < 0.1
      Result=0.0;
end

    Previous_Result = Result;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

else
    Result = 0;
    Ratio = -1;
end