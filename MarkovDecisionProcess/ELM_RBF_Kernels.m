function [Beta, MU, SIGMA] = ELM_RBF_Kernels(X_feature, Class_Information, EachClassNeuronNum,Sigmas)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% First Code was written by HanJin LEE at 2012. 7. 31
% with MATLAB 7.6.0 (R2008a) Version
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if size(X_feature,2) > size(X_feature,1),
%     X_feature = X_feature';                 % Change dimension [#Channel #Datum] to [#Datum #Channel]
% end
MaxClassNumber = max(Class_Information);
FeatureDimension = size(X_feature,2);

%%%%%%%%%%%%%%%%%% Find Each Class Mean and Variation %%%%%%%%%%%%%%%%%
Class_Mean = zeros(MaxClassNumber,FeatureDimension);
Class_Variation = zeros(MaxClassNumber,FeatureDimension);
Class_Variations = zeros(MaxClassNumber,FeatureDimension);
for K = 1:MaxClassNumber,
    Class_Mean(K,:) = mean(X_feature(Class_Information == K,:));
      Class_Variations(K,:) = Sigmas*std(X_feature(Class_Information == K,:));
      Class_Variation(K,:) = 5*(max(X_feature(Class_Information == K,:))-min(X_feature(Class_Information == K,:)));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%% Make MU and SIGMA %%%%%%%%%%%%%%%%%%%%%%%%%
MU = ones(EachClassNeuronNum,1)*Class_Mean(1,:)+5*(rand(EachClassNeuronNum,FeatureDimension)-0.5)*diag(Class_Variation(1,:));
SIGMA =Sigmas*ones(EachClassNeuronNum,1)*max(Class_Variation(1,:));
for K = 2:MaxClassNumber,
    MU = [MU; ones(EachClassNeuronNum,1)*Class_Mean(K,:)+5*(rand(EachClassNeuronNum,FeatureDimension)-0.5)*diag(Class_Variation(1,:))];
    SIGMA = [SIGMA; 1*ones(EachClassNeuronNum,1)*max(Class_Variation(K,:))];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% X_feature_Max = max(X_feature);
% X_feature_Min = min(X_feature);
% Step_Size = (X_feature_Max-X_feature_Min)/(EachClassNeuronNum*MaxClassNumber-1);
% MU = zeros(EachClassNeuronNum*MaxClassNumber,FeatureDimension);
% for K = 1:FeatureDimension,
%     MU(:,K) = [X_feature_Min(K):Step_Size(K):X_feature_Max(K)];
% end
% SIGMA = 100*max(var(X_feature))*ones(EachClassNeuronNum*MaxClassNumber,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Make H %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SIGMA_Length = length(SIGMA);
H = zeros(size(X_feature,1),SIGMA_Length);
for K = 1:SIGMA_Length,
    Norm_Coeff = sqrt( sum((X_feature-ones(size(X_feature,1),1)*MU(K,:)).^2,2) );
%      Norm_Coeff = getThetaMat(X_feature,MU(K,:));
    H(:,K) = exp(-1*(1/SIGMA(K)).*Norm_Coeff);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Make T %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T = zeros(size(X_feature,1),MaxClassNumber);
for K = 1:MaxClassNumber,
    [Row, Col]=find(Class_Information == K);
    T(Row, K) = 1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Find Beta %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Beta = pinv(H)*T;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%