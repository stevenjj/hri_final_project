function BoolCollision = CheckCollisionIdx(StateVector,ActionPolicy)


switch ActionPolicy
    
    case 1
        checkIdx=1;
        if StateVector(checkIdx)>0
             BoolCollision=1;
        else
             BoolCollision=0;
        end
    case 2
        checkIdx=3;
         if StateVector(checkIdx)>0
             BoolCollision=1;
        else
             BoolCollision=0;
        end
    case 3
        checkIdx=5;
         if StateVector(checkIdx)>0
             BoolCollision=1;
        else
             BoolCollision=0;
        end
    case 4
        checkIdx=7;  
         if StateVector(checkIdx)>0
             BoolCollision=1;
        else
             BoolCollision=0;
        end
        
    otherwise
          BoolCollision=0;
end
  
              
end
    