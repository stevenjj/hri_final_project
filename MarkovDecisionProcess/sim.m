
global ELM_param;
tempDataSet=[];
tempDataSetState=[];
for k=3:14
num_sim=1;
FileName=strcat('StateVectorSet_',num2str(k),'.mat');
load(FileName);
tempDataSet=[tempDataSet;StateVectorSet];
FileName=strcat('StateVectorSetState_',num2str(k),'.mat');
tempDataSetState=[tempDataSetState;StateVectorSetState];
load(FileName);

end

[row col]=find(tempDataSetState>0);
FTempDataSet=tempDataSet(row,:);
FTempDataSetState=tempDataSetState(row,:);


[result acc]=ELMClassifier(FTempDataSet,FTempDataSetState,FTempDataSet,FTempDataSetState,2);



%% Learning
% NeuronNum=10;
% Sigmas=1.5;
% [Beta, MU, SIGMA] = ELM_RBF_Kernels(FTempDataSet,FTempDataSetState, NeuronNum,Sigmas);
% ELM_param.Beta=Beta;
% ELM_param.MU=MU;
% ELM_param.SIGMA=SIGMA;
% 
% 
% [result2 acc2]=multisvm(FTempDataSet,FTempDataSetState,FTempDataSet,FTempDataSetState,2);


