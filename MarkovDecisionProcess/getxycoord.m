function xycoord=getxycoord(cellnumber,xstep)

xycoord.x=rem(cellnumber,xstep);
xycoord.y=floor(cellnumber/xstep);
   
end