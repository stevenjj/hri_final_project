function [result accuracy] = ELMClassifier(TrainingSet,TrainingState,TestSet,TestState,Sigmas)

NeuronNum=15;

u=unique(TrainingState);
numClasses=length(u);
result = zeros(length(TestSet(:,1)),1);
count=0;
pre_result=0;

%build models
[Beta, MU, SIGMA] = ELM_RBF_Kernels(TrainingSet,TrainingState, NeuronNum,Sigmas);

%classify test cases
for j=1:size(TestSet,1)
   
   TestData=TestSet(j,:);
   W_pca=eye(size(TestData,2));
   [Ratio, results, pre_result] = SLFNs_RBF_Kernels(TestData,W_pca, Beta, MU, SIGMA,pre_result); 
    result(j) = results;
    
    if results==TestState(j)
        count=count+1;
%     elseif results==0
%         count=count+1;
    else
        count=count;
    end
    
end
  
accuracy=count/size(TestSet,1);
