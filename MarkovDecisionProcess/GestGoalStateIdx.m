function getGoalDirection= GestGoalStateIdx(xpos,ypos,Goal_x,Goal_y)

     % Get the direction of goal direction refer to State
      % 4     3    2
      % 5   Robot  1
      % 6     7    8
      
ActionCC=cell(8,1);
ActionCC{1}=[1,0];
ActionCC{2}=[1,1];
ActionCC{3}=[0,1];
ActionCC{4}=[-1,1];
ActionCC{5}=[-1,0];
ActionCC{6}=[-1,-1];
ActionCC{7}=[0,-1];
ActionCC{8}=[1,-1];
      
getGoalDirection=0;

x_diff =Goal_x-xpos;
y_diff =Goal_y-ypos;

if x_diff>0
    tempx=1;   
elseif x_diff<0 
    tempx=-1;   
else
    tempx=0;
end

if y_diff>0
    tempy=1;   
elseif y_diff<0 
    tempy=-1;   
else
    tempy=0;
end

%%Check the direction
for i=1:8
    if ActionCC{i}==[tempx,tempy]
        getGoalDirection=i;
    end
end

end