function BoolCollision = CheckCollision(StateVector,ActionVector)

curPosX=0;
curPosY=0;

BoolCollision=0;
CheckIdx=0;

New_X=curPosX+ActionVector(1);
New_Y=curPosY+ActionVector(2);

if New_X==1 && New_Y==0
    CheckIdx=1;
elseif New_X==0 && New_Y==1
      CheckIdx=3;        
elseif New_X==-1 && New_Y==0
      CheckIdx=5;      
elseif New_X==0 && New_Y==-1
      CheckIdx=7;                
else
     CheckIdx=0;  
    BoolCollision=0;
    return
end

    if StateVector(CheckIdx)>0
        BoolCollision=1;
    else
        BoolCollision=0;
    end
end
    