function NHIdex= GetNNHIdx(xpos,ypos,HumanSet)


NumHumans = length(HumanSet);
DistanceSet=zeros(NumHumans ,1);

for i=1:NumHumans

    x_diff=HumanSet{i}(1)-xpos;
    y_diff=HumanSet{i}(2)-ypos;
    
    DistanceSet(i)=sqrt(x_diff*x_diff+y_diff*y_diff);
    
end

 [Result, Index]=sort(DistanceSet,'ascend');

NHIdex= GestGoalStateIdx(xpos,ypos,HumanSet{Index(1)}(1),HumanSet{Index(1)}(2));


end