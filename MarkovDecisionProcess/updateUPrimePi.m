function new_up_pi = updateUPrimePi(xval,yval,POINTS,Ra,MaxVector)

actions=[];

R=POINTS(:,3);
Up=POINTS(:,4);
U=POINTS(:,5);
Pi=POINTS(:,6);
PiNums=POINTS(:,7);

gamma = 1; %discount factor
pGood = 0.9; %probability of taking intended action
pBad = (1-pGood)/2; % 2 bad actions, split prob between them

sink=find(POINTS(:,3)==0);%row no. of sink point in POINTS

ObsSet=cell(length(sink),1);
xSinkset=zeros(length(sink),1);
ySinkset=zeros(length(sink),1);
xSink=POINTS(sink(2),1);
ySink=POINTS(sink(2),1);
for i=1:length(sink)
ObsSet{i}=[POINTS(sink(i),1);POINTS(sink(i),2)];
xSinkset(i)=POINTS(sink(i),1);
ySinkset(i)=POINTS(sink(i),2);
end

Xmax=max(POINTS(:,1));
Ymax=max(POINTS(:,2));

for k=1:size(POINTS,1)
    if (POINTS(k,1)==xval && POINTS(k,2)==yval)
        if R(k)~=Ra %If at a sink state or unreachable state 'terminals' , use that value
            Up(k)=R(k);
        else %use Bellman equation  "computed using U(s), not U'(s))"
            aN= aNorth(xval,yval,U,POINTS,k,ObsSet,MaxVector);
            aS= aSouth(xval,yval,U,POINTS,k,ObsSet,MaxVector);
            aW= aWest(xval,yval,U,POINTS,k,ObsSet,MaxVector);
            aE= aEast(xval,yval,U,POINTS,k,ObsSet,MaxVector);
            
            aEN= aEastNorth(xval,yval,U,POINTS,k,ObsSet,MaxVector);
            aWN= aWestNorth(xval,yval,U,POINTS,k,ObsSet,MaxVector);
            aES= aEastSouth(xval,yval,U,POINTS,k,ObsSet,MaxVector);
            aWS= aWestSouth(xval,yval,U,POINTS,k,ObsSet,MaxVector);
                       
            actions(1)= aE*pGood + aES*pBad + aEN*pBad;          
            actions(2)= aEN*pGood + aE*pBad + aN*pBad;
            actions(3)= aN*pGood + aEN*pBad + aWN*pBad;
            actions(4)= aWN*pGood + aW*pBad + aN*pBad;
            actions(5)= aW*pGood + aWS*pBad + aWN*pBad;
            actions(6)= aWS*pGood + aW*pBad + aS*pBad;
            actions(7)= aS*pGood + aWS*pBad + aES*pBad;
            actions(8)= aES*pGood + aE*pBad + aS*pBad;
               
            best= maxindex(actions);
            PiNums(k)=best;
   
            Up(k)= R(k) + gamma*actions(best);
                       
            %-------Update Pi 'Policy'
            switch best
                case 1
                        Pi(k)='E';
                case 2
                        Pi(k)='R';
                case 3
                        Pi(k)='N';
                case 4
                        Pi(k)='Q';
                case 5
                        Pi(k)='W';
                case 6
                        Pi(k)='Z';
                case 7
                        Pi(k)='S';
                case 8
                    Pi(k)='C';
                otherwise
                    Pi(k)='X';
            end
           
            
            
%             if best==1
%                Pi(k)='E';
%             else if best==2
%                     Pi(k)='N'; 
%                 else if best==3
%                         Pi(k)='W';
%                     else
%                         Pi(k)='S';
%                     end
%                 end
%             end
            %-------
            
        end
        
    end
end

new_up_pi=[Up,Pi,PiNums];

end

