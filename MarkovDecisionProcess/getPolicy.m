function [ActionPolicy confidence] = getPolicy(TestStateVector,TrainingData,TrainingDataState,Classifier_type)
pre_result=0;
boolcollision=1;
frow=0;
global Main_Param
% global ELM_param
 switch Classifier_type
     case 1 %ELM Classifier

        Beta=Main_Param.ELM_param.Beta;
        MU=Main_Param.ELM_param.MU;
        SIGMA=Main_Param.ELM_param.SIGMA;
        W_pca=eye(size(TestStateVector,2));
               
    [confidence Actionresult] =SLFNs_RBF_Kernels(TestStateVector,W_pca, Beta, MU, SIGMA,pre_result); 
    boolcollision=CheckCollisionIdx(TestStateVector,Actionresult);
    
    while boolcollision > 0 || Actionresult==0 || confidence < 0.35
%          Actionresult = randi([1 4],1,1);
          Actionresult =TestStateVector(end); 
          boolcollision=CheckCollisionIdx(TestStateVector,Actionresult);
          confidence=0.35;
    end
    
     case 2 %%SVM
      for k=1:Main_Param.Num_Class
        if(svmclassify(Main_Param.SVM_Param.models(k),TestStateVector')) 
            break;
        end
      end
     Actionresult=k;
    confidence=0.6;
     
     case 0 %%RandomNumber
         
    %% Find the past data 
    for k=1:size(TrainingData,1)
        if TestStateVector'==TrainingData(k,:)
           frow=k; 
           break;
        end
    end
    if frow
        Actionresult=TrainingDataState(frow);
        confidence=1;
    else
    
        Actionresult =TestStateVector(end); 
%      while boolcollision > 0
%          Actionresult = randi([1 4],1,1);
%          boolcollision=CheckCollisionIdx(TestStateVector,Actionresult);
%      end
%      
     confidence=0;
    end
     
    
 end
    
    ActionPolicy=GetMoveVector(Actionresult);
    
%         
%     case 2 %SVM Classifier
%     
        
     
% end



end