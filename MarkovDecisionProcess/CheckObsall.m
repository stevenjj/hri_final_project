function result = CheckObsall(xval,yval,ObsSet,MaxVec,Action)

% xval   = current state xposition
% yval   = current state yposition
% ObsSet = Obstacle Set
% MaxSet = [X_max; Y_max] : to check boundary of map 
% Action = Posible Action 2D vector ex) right : [1 ; 0], up [0 ; 1];
% result = 1   -if collision with obstacle 
%          0   -no collision with obstacle 

Num_obs=length(ObsSet);

%%Calculate next position after action policy
xafter=xval+Action(1);
yafter=yval+Action(2);
result=0;

for i=1:Num_obs

   if(xafter>MaxVec(1) ||  yafter>MaxVec(2))  %check boundary of map
       result=1;
   elseif (xafter<1 ||  yafter<1)
       result=1;
       return;
   else
        if(xafter==ObsSet{i}(1) && yafter==ObsSet{i}(2))  %check set of obstacles of map
            result=1;
        end
   end
end