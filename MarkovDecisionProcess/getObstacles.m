function ObstacleSet=getObstacles(occupacymap,xstep)

[row col]=find(occupacymap==1 |occupacymap==3);

ObstacleSet=cell(length(row),1);
for i=1:length(row)
    obsxycoord=getxycoord(row(i),xstep);
    ObstacleSet{i}=[obsxycoord.x+1;obsxycoord.y+1];
end


