function ROS_cmd_Callback(~, ~, handles)
    %exampleHelperROSSimTimer - Timer update function called in startExamples to publish
    %   messages at well-defined intervals
    %   exampleHelperROSSimTimer(~,~,HANDLES) publishes /pose and /scan messages at
    %   regular intervals. The /scan message value remains constant while
    %   the /pose message value changes continually.
    %   
    %   See also exampleHelperROSCreateSampleNetwork
    
    %   Copyright 2014-2015 The MathWorks, Inc.
%     global 
%     % Update the pose message values
%     if isvalid(handles.twistPub)
%         scan=receive(handles.twistPub);
%        % handles.twistPubmsg
%        % Publish the scan and pose messages
% %         handles.msgs.Data='Wow';
%         send(handles.MatPub,handles.msgs);
%     end
     
       
     global Main_Param
     
     Main_Param.Num_good=Main_Param.Num_good+1;
     
     set(handles.ros_cmd,'String',Main_Param.Num_good);
    
end