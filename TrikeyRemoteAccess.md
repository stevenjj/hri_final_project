Kwan-Suk's Notes about accessing Trikey

Here is the ssh access

hri:hri@192.168.1.107

You need to set bash configuration as follow
source ~/.bashrc

To check Trikey is on, give the following command
lsec

This message should be shown
0  0:0  PREOP  +  EK1100 EtherCAT-Koppler (2A E-Bus)
1  0:1  PREOP  +  EK1122 2-Port EtherCAT-Abzweig
2  0:2  PREOP  +  M3 ECHUB EtherCat Hub [Name: EH020 ; SN: 1015]
3  0:3  PREOP  +  M3EC UTA .PWR Controller [Name: PWR020 ; SN: 718]
4  0:4  PREOP  +  M3EC ActX1.PDOV2 Controller [Name: MB2J0 ; SN: 667]
5  0:5  PREOP  +  M3EC ActX1.PDOV2 Controller [Name: MB2J1 ; SN: 720]
6  0:6  PREOP  +  M3EC ActX1.PDOV2 Controller [Name: MB2J2 ; SN: 721]

To run the M3 server give the following command
m3rt_server_run -m

To kill M3 server,
m3rt_server_kill

I made a compact code for the programming in ~/base_ctrl folder.

All you need to change is tasks/task02.cpp file

To build the program, just run 'make'

./base_ctrl Task02