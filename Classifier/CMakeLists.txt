cmake_minimum_required(VERSION 2.8.3)
project(classifier)	

SET(SOURCES src/classifier.cpp src/StringTokenizer.cpp src/manager.cpp)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  libsocket
  roscpp
  geometry_msgs
  sensor_msgs
  message_generation
  std_msgs
)

find_package(Eigen3 REQUIRED)
include_directories(${EIGEN3_INCLUDE_DIR})
include_directories(include)
include_directories(src)


add_message_files(FILES CBA_NavInfo.msg)
#add_service_files(FILES CBA_Navinoservice.srv)


generate_messages(
   DEPENDENCIES
   std_msgs
)



catkin_package(
  INCLUDE_DIRS include
#  LIBRARIES classifier
#  CATKIN_DEPENDS libsocket roscpp geometry_msgs
  DEPENDS
  	roscpp
  	geometry_msgs
  	sensor_msgs
  	message_runtime
 )

include_directories(
  ${catkin_INCLUDE_DIRS}
)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_executable(classifier_test src/classifier_test.cpp ${SOURCES})
target_link_libraries(classifier_test ${catkin_LIBRARIES} socket++)
